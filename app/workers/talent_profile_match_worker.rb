class TalentProfileMatchWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily }

  def perform
    TalentProfile.all.each do |talent|
      MatchFinder.call(talent)
    end
  end
end
