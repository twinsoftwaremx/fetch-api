require 'linkedin'
class LinkedinWorker
  include Sidekiq::Worker

  def perform(oauth_token, oauth_secret, user_id)
    linkedin = ::LinkedIn::Client.new
    linkedin.authorize_from_access(oauth_token, oauth_secret)
    linkedin_asset = ::LinkedinAsset.first_or_create(user_id: user_id)
    linkedin_asset.profile = linkedin.profile
    linkedin_asset.save
  end
end
