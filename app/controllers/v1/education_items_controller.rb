class V1::EducationItemsController < V1::BaseController

  def index
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :view_object_list?
    education_items = EducationItem.find_by(talent_profile: params[:talent_profile_id])
    render json: education_items, status: 200
  end

  def show
    education_item = EducationItem.find(params[:id])

    authorize education_item
    render json: education_item, status: 200
  end

  def create
    talent_profile = TalentProfile.find(params[:talent_profile_id])

    authorize talent_profile, :add_object?
    education_item = EducationItem.new(education_item_params)
    education_item.talent_profile = talent_profile
    if education_item.save
      render json: education_item, status: 201
    else
      render json: { errors: education_item.errors }, status: 422
    end
  end

  def update
    education_item = EducationItem.find(params[:id])

    authorize education_item
    if education_item.update(education_item_params)
      render json: education_item, status: 204
    else
      render json: { errors: education_item.errors }, status: 422
    end
  end

  def destroy
    education_item = EducationItem.find(params[:id])

    authorize education_item
    education_item.destroy
    head 204
  end

  private
  def education_item_params
    params.require(:education_item).permit(:college, :year, :degree)
  end
end