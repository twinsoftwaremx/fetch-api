class V1::EmployerContactsController < V1::BaseController

  def index

    skip_authorization # Need to define authorization for this action

    employer_contacts = EmployerContact.all
    render json: employer_contacts, status: 200
  end

  def show
    employer_contact = EmployerContact.find(params[:id])

    authorize employer_contact

    render json: employer_contact, include: 'account', status: 200
  end

  def create
    employer_contact = EmployerContact.create(employer_contact_params)
    employer_contact.user = current_user

    authorize employer_contact

    if employer_contact.save
      render json: employer_contact, status: 201
    else
      render json: { errors: employer_contact.errors }, status: 422
    end
  end

  def update
    employer_contact = EmployerContact.find(params[:id])

    authorize employer_contact

    if employer_contact.update(employer_contact_params)
      render json: employer_contact, status: 204
    else
      render json: { errors: @employer_contact.errors }
    end
  end

  def destroy
    employer_contact = EmployerContact.find(params[:id])

    authorize employer_contact

    employer_contact.destroy
    head 204
  end

  def invite_coworkers
    skip_authorization

    invitation_link = request.base_url + '/invite_employer_contact.html?id=' + SecureRandom.uuid

    if params[:emails].present?
      params[:emails].each do |email|
        EmployerContactMailer.invite_coworker(email, invitation_link).deliver_now
      end
    end
    
    render json: 'Invites sent', status: 200
  end

  def get_recent_conversations
    skip_authorization
    
    employer_contact_id = params[:employer_contact_id]
    count = params[:count].present? ? params[:count].to_i : 0
    result = EmployerContact.get_recent_conversations(employer_contact_id, count)

    render json: result.to_json, status: 200
  end

  private
  def employer_contact_params
    params.require(:employer_contact).permit(:name, :photo_id, :title, :linked_in_url, :summary, :phone,
                                             :time_zone, :account_id)
  end
end