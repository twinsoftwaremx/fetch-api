class V1::AgentsController < V1::BaseController
  after_action :verify_authorized, except: [:index, :show]

  def show
    @agent = Agent.find(params[:id])

    render json: @agent
  end
end
