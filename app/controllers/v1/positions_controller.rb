class V1::PositionsController < V1::BaseController
  after_action :verify_authorized, except: [:index]

  def index
    positions = policy_scope(Position)

    if params[:status].present?
      status = params[:status] == 'active' ? Offer.statuses[:unanswered] : Offer.statuses[:accepted]

      if status == Offer.statuses[:accepted]
        positions = positions.joins(:offers).where('offers.status=?', Offer.statuses[:accepted])
      else
        offers_joins = 'LEFT JOIN Offers on Offers.employer_contact_id = Employer_Contacts.id AND Offers.status = ' + status.to_s
        positions = positions.joins('LEFT JOIN Employer_Contacts on Employer_Contacts.id = Positions.employer_contact_id').joins(offers_joins)
      end
    end

    if params[:employer_contact_id].present?
      positions = positions.includes({city: [:state]}, :salary_range, :employment_type, :employer_contact).where(employer_contact_id: params[:employer_contact_id])
    end

    if params[:filter_skill].present?
      positions = positions.joins(:required_skill_associations).where('required_skill_associations.skill_id = ?', params[:filter_skill])
    end
    if params[:filter_compensation].present?
      positions = positions.where(salary_range_id: params[:filter_compensation])
    end
    if params[:filter_location].present?
      positions = positions.where(city_id: params[:filter_location])
    end

    render json: positions, include: ['city', 'salary_range', 'employment_type', 'employer_contact'], status: 200
  end

  def show
    position = Position.find(params[:id])

    authorize position

    render json: position, status: 200
  end

  def create
    employer_contact = EmployerContact.find(params[:employer_contact_id])

    authorize employer_contact, :add_object?

    position = Position.create(position_params)
    position.employer_contact = employer_contact

    if position.save
      render json: position, status: 201
    else
      render json: { errors: position.errors }, status: 422
    end
  end

  def update
    position = Position.find(params[:id])

    authorize position

    if position.update(position_params)
      render json: position, status: 204
    else
      render json: { errors: position.errors }, status: 422
    end
  end

  def destroy
    position = Position.find(params[:id])

    authorize position

    position.destroy
    head 204
  end

  private
  def position_params
    params.require(:position).permit(:job_title, :description, :hours, :salary_range_id, :desired_years_experience,
                                     :team_size, :employment_type_id, :employer_contact_id, :city_id, :hiring_manager,
                                     :required_skill_list)
  end
end
