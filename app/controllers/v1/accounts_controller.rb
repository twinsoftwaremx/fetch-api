class V1::AccountsController < V1::BaseController
  after_action :verify_authorized, except: [:index, :show]

  def index
    accounts = Account.all
    render json: accounts, status: 200
  end

  def show
    @account = Account.find(params[:id])

    render json: @account
  end

  def create
    account = Account.new(account_params)
    account.agent = Agent.first

    authorize account

    if location_params.present?
      account.build_location(location_params)
    end

    if params[:cultures].present?
      params[:cultures].each do |culture|
        account_culture_association = AccountCultureAssociation.new(culture_id:culture[:id])
        account.account_culture_associations << account_culture_association
      end
    end

    if params[:logo_file].present?
      account.create_logo(params[:logo_file])
    end

    if account.save
      render json: account, status: 201
    else
      render json: { errors: account.errors }
    end
  end

  def update
    @account = Account.find(params[:id])

    authorize @account

    if @account.update(account_params)
      render json: @account, status: 204
    else
      render json: { errors: @account.errors }
    end
  end

  def destroy
    account = Account.find(params[:id])

    authorize account

    account.destroy
    head 204
  end

  private
  def account_params
    params.require(:account).permit(:company_name, :company_size_id, :phone, :summary, :benefits, :perks, :culture,
                                    :github_url, :company_site_url, :address_id, :revenue, :year_founded,
                                    :what_we_do, :who_we_are, :logo, :video_url)
  end

  def location_params
    params.permit(:location).permit(:line1, :line2, :city, :state, :zip)
  end
end
