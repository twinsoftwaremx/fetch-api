class V1::CitiesController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    cities = policy_scope(City)

    render json: cities, status: 200
  end

  def show
    city = City.find(params[:id])

    render json: city, status: 200
  end

  def create
    city = City.create(city_params)

    authorize city

    if city.save
      render json: city, status: 201
    else
      render json: { errors: city.errors }, status: 422
    end
  end

  def update
    city = City.find(params[:id])

    authorize city

    if city.update(city_params)
      render json: city, status: 204
    else
      render json: { errors: city.errors }, status: 422
    end
  end

  def destroy
    city = City.find(params[:id])

    authorize city

    city.destroy
    head 204
  end

  private
  def city_params
    params.require(:city).permit(:name, :state_id)
  end
end