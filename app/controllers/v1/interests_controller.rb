class V1::InterestsController < V1::BaseController
  after_action :verify_authorized, except: [:index]

  def index
    interests = policy_scope(Interest)
    render json: interests, status: 200
  end

  def show
    interest = Interest.find(params[:id])

    authorize interest

    render json: interest, status: 200
  end

  def create
    interest = Interest.create(interest_params)

    authorize interest

    if interest.save
      render json: interest, status: 201
    else
      render json: { errors: interest.errors }, status: 422
    end
  end

  def update
    interest = Interest.find(params[:id])

    authorize interest

    if interest.update(interest_params)
      render json: interest, status: 204
    else
      render json: { errors: interest.errors }, status: 422
    end
  end

  def destroy
    interest = Interest.find(params[:id])

    authorize interest

    interest.destroy
    head 204
  end

  private
  def interest_params
    params.require(:interest).permit(:name)
  end

end