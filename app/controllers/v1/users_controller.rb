class V1::UsersController < V1::BaseController
  before_action :authenticate, except: [:create]
  after_action :verify_authorized, only: [:show]

  def show
    user = User.find(params[:id])

    authorize user

    render json: user
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def from_token
    user = current_user
    puts user.inspect
    render json: user, status: 200
  end

  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role)
  end

end
