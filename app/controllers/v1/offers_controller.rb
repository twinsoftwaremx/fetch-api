class V1::OffersController < V1::BaseController
  after_action :verify_authorized, except: :index
  before_action :set_offer, only: [:show, :update, :destroy, :accept, :reject]

  def index
    offers = policy_scope(Offer)
    render json: offers, status: 200
  end

  def show
    authorize @offer
    render json: @offer, status: 200
  end

  def create
    offer = Offer.new(offer_params)

    authorize offer

    OfferMailer.email_make_offer(offer).deliver_now

    if offer.save
      render json: offer, status: 201
    else
      puts offer.errors.inspect
      render json: { errors: offer.errors }
    end
  end

  def destroy
    authorize @offer
    @offer.destroy
    render json: @offer, status: 204
  end

  def accept
    authorize @offer
    status = @offer.accepted? ? 'unanswered' : 'accepted'
    @offer.update(status: status)
    OfferMailer.email_accept_offer(@offer).deliver_now
    render json: @offer, status: 201
  end

  def reject
    authorize @offer
    status = @offer.rejected? ? 'unanswered' : 'rejected'
    @offer.update(status: status)
    OfferMailer.email_reject_offer(@offer).deliver_now
    render json: @offer, status: 201
  end

  private
  def set_offer
    @offer = Offer.find(params[:id])
  end

  def offer_params
    params.require(:offer).permit(:employer_contact_id, :talent_profile_match_id, :company_name,
      :position_title, :manager_name, :salary_pay_rate, :start_date, :benefits, :legal_verbiage,
      :hiring_managers_name, :details, :job_type)
  end
end
