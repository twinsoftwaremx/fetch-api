class V1::InterestedLocationsController < V1::BaseController
  after_action :verify_authorized, except: [:index]

  def index
    interested_locations = policy_scope(InterestedLocation)
    render json: interested_locations, status: 200
  end

  def show
    interested_location = InterestedLocation.find(params[:id])

    authorize interested_location

    render json: interested_location, status: 200
  end

  def create
    interested_location = InterestedLocation.create(interested_location_params)

    authorize interested_location

    if interested_location.save
      render json: interested_location, status: 201
    else
      render json: { errors: interested_location.errors }, status: 422
    end
  end

  def update
    interested_location = InterestedLocation.find(params[:id])

    authorize interested_location

    if interested_location.update(interested_location_params)
      render json: interested_location, status: 204
    else
      render json: { errors: interested_location.errors }, status: 422
    end
  end

  def destroy
    interested_location = InterestedLocation.find(params[:id])

    authorize interested_location

    interested_location.destroy
    head 204
  end

  private
  def interested_location_params
    params.require(:interested_location).permit(:name)
  end

end