class V1::MessagesController < V1::BaseController
  after_action :verify_authorized, except: [:index, :sent, :received]
  
  def index
    messages = policy_scope(Message)

    if params[:position_id].present?
      messages = messages.where(position_id: params[:position_id])
    end

    render json: messages, include: ['to_user', 'from_user'], status: 200
  end

  def show
    message = Message.find(params[:id])

    authorize message

    render json: message, status: 200
  end

  def create
    message = Message.new(new_message_params)

    message.from_user = current_user

    authorize message

    if message.to_user.nil?
      position = Position.includes(:employer_contact).find(new_message_params[:position_id])
      message.to_user_id = position.employer_contact.user_id
    end

    if message.save
      render json: message, status: 201
    else
      render json: { errors: message.errors }, status: 422
    end
  end

  def update
    message = Message.find(params[:id])

    authorize message

    if message.update(edit_message_params)
      render json: message, status: 204
    else
      render json: { errors: message.errors }, status: 422
    end
  end

  def destroy
    message = Message.find(params[:id])

    authorize message

    message.destroy
    head 204
  end

  def sent
    messages = policy_scope(Message)
      .where(position_id: params[:position_id])
    if params[:to_user_id].present?
      messages = messages.where(to_user_id: params[:to_user_id])
    end

    render json: messages, status: 200
  end

  def received
    messages = policy_scope(Message)
      .where(position_id: params[:position_id])
    if params[:from_user_id].present?
      messages = messages.where(from_user_id: params[:from_user_id])
    end

    render json: messages, status: 200
  end

  def with_user
    skip_authorization

    other_user_id = params[:with_user_id]
    messages = Message.where(position_id: params[:position_id])
    messages = messages.where('(from_user_id=? and to_user_id=?) or (from_user_id=? and to_user_id=?)',
                              current_user.id, other_user_id, other_user_id, current_user.id)

    render json: messages, status: 200
  end

  private
  def new_message_params
    params.require(:message).permit(:body, :to_user_id, :position_id)
  end

  def edit_message_params
    params.require(:message).permit(:body)
  end
end