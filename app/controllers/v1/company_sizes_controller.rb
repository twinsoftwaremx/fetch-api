class V1::CompanySizesController < V1::BaseController
  before_action :authenticate, except: [:index, :show]
  after_action :verify_authorized, except: [:index, :show]

  def index
    company_sizes = policy_scope(CompanySize)
    render json: company_sizes, status: 200
  end

  def show
    company_size = CompanySize.find(params[:id])

    render json: company_size, status: 200
  end

  def create
    company_size = CompanySize.create(company_size_params)

    authorize company_size

    if company_size.save
      render json: company_size, status: 201
    else
      render json: { errors: company_size.errors }, status: 422
    end
  end

  def update
    company_size = CompanySize.find(params[:id])

    authorize company_size

    if company_size.update(company_size_params)
      render json: company_size, status: 204
    else
      render json: { errors: company_size.errors }, status: 422
    end
  end

  def destroy
    company_size = CompanySize.find(params[:id])

    authorize company_size

    company_size.destroy
    head 204
  end

  private
  def company_size_params
    params.require(:company_size).permit(:name)
  end

end