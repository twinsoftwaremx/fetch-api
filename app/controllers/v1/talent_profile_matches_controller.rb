  class V1::TalentProfileMatchesController < V1::BaseController
  after_action :verify_authorized, except: :index
  before_action :set_talent, except: [:index]
  before_action :set_match, only: [:like, :reject]

  def index
    if params[:talent_profile_id].present?
      @talent_profile = TalentProfile.find(params[:talent_profile_id])
      matches = @talent_profile.talent_profile_matches.ranked
    elsif params[:position_id].present?
      @position = Position.find(params[:position_id])
      accepted_statuses = [TalentProfileMatch.statuses[:new_match], TalentProfileMatch.statuses[:liked]]
      matches = @position.talent_profile_matches.where(status: accepted_statuses).ranked
    end
    render json: matches, include: ['position', 'talent_profile'], status: 200
  end

  def like
    authorize @match
    status = @match.liked? ? 'new_match' : 'liked'
    @match.update(status: status)
    TalentMatchMailer.email_talent_interest(@talent_profile, @match).deliver_now
    render json: @match, include: 'position', status: 201
  end

  def reject
    authorize @match
    status = @match.liked? ? 'new_match' : 'rejected'
    @match.update(status: status)
    render json: @match, include: 'position', status: 201
  end

  private
  def set_talent
    @talent_profile = TalentProfile.find(params[:talent_profile_id])
  end

  def set_match
    @match = @talent_profile.talent_profile_matches.find(params[:id])
  end
end
