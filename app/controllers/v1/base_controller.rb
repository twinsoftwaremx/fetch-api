class V1::BaseController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include Pundit
  before_action :authenticate
  after_action :verify_authorized

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # Overwrite Devise method
  def current_user
    authenticate_with_http_token do |token, options|
      @current_user ||= User.find_by(auth_token: token)
    end
  end

  protected
  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      User.find_by(auth_token: token)
    end
  end

  def render_unauthorized
    render json: 'Bad credentials', status: 401
  end

  def user_not_authorized
    render json: 'User not authorized for this action', status: 403
  end
end
