class V1::TalentProfilesController < V1::BaseController

  def show
    talent_profile = TalentProfile.find(params[:id])

    authorize talent_profile

    render json: talent_profile, status: 200
  end

  def create
    talent_profile = TalentProfile.new(talent_profile_params)
    talent_profile.user = current_user
    talent_profile.agent = Agent.first

    authorize talent_profile

    if location_params.present?
      talent_profile.build_location(location_params)
    end

    if params[:skills].present?
      params[:skills].each do |skill|
        skill_association = SkillAssociation.new(skill_id:skill['id'], rank:skill['rank'].to_i)
        talent_profile.skill_associations << skill_association
      end
    end

    if params[:cities].present?
      params[:cities].each do |city|
        city_of_interest_association = CityOfInterestAssociation.new(city_id:city[:id])
        talent_profile.city_of_interest_associations << city_of_interest_association
      end
    end

    if params[:cultures].present?
      params[:cultures].each do |culture|
        talent_culture_association = TalentCultureAssociation.new(culture_id:culture[:id])
        talent_profile.talent_culture_associations << talent_culture_association
      end
    end

    if talent_profile.save
      render json: talent_profile, status: 201
    else
      render json: { errors: talent_profile.errors }, status: 422
    end
  end

  def update
    talent_profile = TalentProfile.find(params[:id])

    authorize talent_profile
    if talent_profile.update(talent_profile_params)
      render json: talent_profile, status: 204
    else
      render json: { errors: talent_profile.errors }, status: 422
    end
  end

  def destroy
    talent_profile = TalentProfile.find(params[:id])

    authorize talent_profile
    talent_profile.destroy
    head 204
  end

  def get_recent_conversations
    skip_authorization
    
    talent_profile_id = params[:talent_profile_id]
    count = params[:count].present? ? params[:count].to_i : 0
    result = TalentProfile.get_recent_conversations(talent_profile_id, count)

    render json: result.to_json, status: 200
  end

  private
  def talent_profile_params
    params.require(:talent_profile).permit(:name, :photo_id, :resume_id, :time_zone, :phone, :summary,
                                           :area_of_interest, :years_of_experience, :usdod_clearance,
                                           :reason_for_interest_in_new_job_opportunities, :job_search_status,
                                           :salary_range_id, :usa_work_auth_type, :linked_in_url,
                                           :personal_website, :dribbble_url, :stack_overflow_url,
                                           :blog_url, :skill_list, :title, :company)
  end

  def location_params
    params.permit(:location).permit(:line1, :line2, :city, :state, :zip)
  end
end
