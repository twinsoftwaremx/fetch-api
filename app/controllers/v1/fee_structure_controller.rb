class V1::FeeStructureController < V1::BaseController
  before_action :authenticate
  after_action :verify_authorized

  def get_fees
    salary = params[:salary]

    fee_structure = FeeStructure.new

    authorize fee_structure, :get_fees?

    fee_structure.calculate_fees(salary)

    render json: fee_structure.to_json, status: 200
  end

end