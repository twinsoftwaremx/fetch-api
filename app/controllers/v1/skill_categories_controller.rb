class V1::SkillCategoriesController < V1::BaseController
  before_action :authenticate, except: [:index]
  after_action :verify_authorized, except: [:index, :show]

  def index
    skill_categories = policy_scope(SkillCategory)
    render json: skill_categories, status: 200
  end

  def show
    skill_category = SkillCategory.find(params[:id])
    render json: skill_category, status: 200
  end

  def create
    skill_category = SkillCategory.create(skill_category_params)

    authorize skill_category

    if skill_category.save
      render json: skill_category, status: 201
    else
      render json: { errors: skill_category.errors }, status: 422
    end
  end

  def update
    skill_category = SkillCategory.find(params[:id])

    authorize skill_category

    if skill_category.update(skill_category_params)
      render json: skill_category, status: 204
    else
      render json: { errors: skill_category.errors }, status: 422
    end
  end

  def destroy
    skill_category = SkillCategory.find(params[:id])

    authorize skill_category

    skill_category.destroy
    head 204
  end

  private
  def skill_category_params
    params.require(:skill_category).permit(:name)
  end
end