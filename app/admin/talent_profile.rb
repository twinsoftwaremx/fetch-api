ActiveAdmin.register TalentProfile do
  remove_filter :talent_profile_matches
  remove_filter :matches

  action_item :match, only: :show do
    link_to "Get Matches", get_matches_admin_talent_profile_path(params[:id])
  end

  member_action :get_matches, method: :get do
    MatchFinder.call(resource)
    redirect_to admin_talent_profiles_path
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted  = [:name, :photo_id, :agent_id, :resume_id, :time_zone, :phone, :summary]
    permitted << [:area_of_interest, :years_of_experience, :usdod_clearance]
    permitted << [:reason_for_interest_in_new_job_opportunities, :job_search_status]
    permitted << [:preferred_base_salary, :usa_work_auth_type, :linked_in_url]
    permitted << [:personal_website, :dribbble_url, :stack_overflow_url]
    permitted << [:stack_overflow_url, :blog_url]

    permitted
  end


end
