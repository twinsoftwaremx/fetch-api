ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      if current_admin_user.agent_admin?
        column do
          panel "Assigned Talent " do
            ul do
              current_admin_user.agent.talent_profiles.limit(10).map do |profile|
                li link_to(profile.name, admin_talent_profile_path(profile))
              end
            end
          end
        end

        column do
          panel "Assigned Accounts" do
            ul do
              current_admin_user.agent.accounts.limit(10).map do |account|
                li link_to(account.company_name, admin_account_path(account))
              end
            end
          end
        end
      end
    end
  end
end
