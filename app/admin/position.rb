ActiveAdmin.register Position do
  remove_filter :talent_profile_matches
  remove_filter :talent_profiles

  collection_action :search_skills, method: :get do
    skills = Skill.all

    if params[:query].present?
      name = params[:query]
      skills = skills.where("LOWER(name) LIKE ?", "#{name.downcase}%")
    end

    render json: skills
  end

  index do
    id_column
    column :job_title
    column :employment_type
    column :city
    column :salary_range
    column :required_skill_list
    column :created_at
  end

  form do |f|
    f.semantic_errors # shows errors on :base
    f.inputs 'Details' do
      f.input :job_title
      f.input :description
      f.input :employment_type
      f.input :employer_contact
      f.input :city
      f.input :salary_range, label: 'Compensation'
      f.input :hours
      f.input :desired_years_experience, label: 'Desired years of experience'
      f.input :team_size
    end
    f.inputs 'Skills' do
      f.input :required_skill_list
      f.input :bonus_skill_list
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
    render partial: 'scripts'
  end

  show do
    tabs do
      tab 'Overview' do
        attributes_table do
          row(:job_title)
          row(:description)
          row(:employment_type)
          row(:employer_contact)
          row(:city)
          row(:salary_range)
          row(:hours)
          row(:desired_years_experience)
          row(:team_size)
          row(:created_at)
          row(:updated_at)
        end
      end
      tab 'Skills' do
        attributes_table do
          row(:required_skill_list)
          row(:bonus_skill_list)
        end
      end
    end
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted =  [:employment_type_id, :employer_contact_id, :job_title]
    permitted << [:description, :hours, :salary_range, :desired_years_experience]
    permitted << [:team_size, :required_skill_list, :bonus_skill_list]
    permitted
  end
end
