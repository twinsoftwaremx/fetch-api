ActiveAdmin.register Account do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted  = [:company_name, :size, :phone, :summary, :benefits]
    permitted << [:perks, :culture, :github_url, :company_site_url, :agent_id]
    permitted
  end


end
