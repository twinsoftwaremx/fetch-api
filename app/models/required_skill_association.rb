# == Schema Information
#
# Table name: required_skill_associations
#
#  id          :integer          not null, primary key
#  position_id :integer
#  skill_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_required_skill_associations_on_position_id  (position_id)
#  index_required_skill_associations_on_skill_id     (skill_id)
#

class RequiredSkillAssociation < ActiveRecord::Base

  belongs_to :skill
  belongs_to :position

end
