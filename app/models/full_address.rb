# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  line1      :string
#  line2      :string
#  city       :string
#  state      :string
#  zip        :string
#  type       :string
#  latitude   :float
#  longitude  :float
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_addresses_on_type  (type)
#

class FullAddress < Address
  has_many :talent_profile, foreign_key: 'location_id'
  geocoded_by :geocode_address
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      obj.city = geo.city
      obj.state = geo.state
      obj.zip = geo.postal_code
      obj.line1 = geo.address
    end
  end

  after_validation :geocode
  after_validation :reverse_geocode

  def geocode_address
    "#{line1}, #{city} #{state} #{zip}"
  end  # So Admin will display address data easily
  alias_method :name, :geocode_address
end
