# == Schema Information
#
# Table name: talents
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  talent_profile_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_talents_on_talent_profile_id  (talent_profile_id)
#  index_talents_on_user_id            (user_id)
#

class Talent < ActiveRecord::Base
  belongs_to :talent_profile
  belongs_to :user
end
