# == Schema Information
#
# Table name: talent_profiles
#
#  id                                           :integer          not null, primary key
#  name                                         :string           not null
#  time_zone                                    :string           not null
#  phone                                        :string
#  summary                                      :text
#  area_of_interest                             :string
#  years_of_experience                          :integer          default(0)
#  usdod_clearance                              :boolean          default(FALSE)
#  reason_for_interest_in_new_job_opportunities :text
#  job_search_status                            :integer          default(0)
#  usa_work_auth_type                           :integer          default(0)
#  linked_in_url                                :string
#  personal_website                             :string
#  dribbble_url                                 :string
#  stack_overflow_url                           :string
#  blog_url                                     :string
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  location_id                                  :integer
#  agent_id                                     :integer
#  salary_range_id                              :integer
#  github_url                                   :string
#  avatar_url                                   :string
#  title                                        :string
#  company                                      :string
#

class TalentProfile < ActiveRecord::Base
  include Searchable
  include TalentProfileSearchable

  DEFAULT_RECENT_CONVERSATIONS = 4

  enum years_of_experience: [:zero_one, :one_two, :two_four, :four_six, :six_plus]
  enum job_search_status: [:ready, :interviewing, :curious, :not_looking, :existing]
  enum usa_work_auth_type: [:us_citizen, :other]

  validates :name, :time_zone, presence: true

  has_many :interest_associations
  has_many :interests, through: :interest_associations
  has_many :skill_associations
  has_many :skills, through: :skill_associations
  belongs_to :location, class_name: "FullAddress", touch: true
  has_many :preferred_company_size_associations
  has_many :preferred_company_sizes, through: :preferred_company_size_associations, source: :company_size
  has_many :previous_employments
  has_many :education_items
  has_many :location_of_interest_associations
  has_many :locations_of_interest, through: :location_of_interest_associations, source: :location
  has_many :city_of_interest_associations
  has_many :cities_of_interest, through: :city_of_interest_associations, source: :city
  has_one :talent
  has_one :user, through: :talent
  belongs_to :agent
  has_many :talent_culture_associations
  has_many :cultures, through: :talent_culture_associations
  has_one :photo, as: :assetable, class_name: "Photo"
  has_one :resume, as: :assetable, class_name: "Resume"
  has_many :talent_profile_matches
  has_many :matches, through: :talent_profile_matches, source: :position
  has_many :offers, through: :talent_profile_matches, source: :offer
  belongs_to :salary_range

  def preferred_base_salary
    salary_range.present? ? salary_range.name : ""
  end

  def city_list
    cities_of_interest.map(&:name).join(", ")
  end

  def culture_list
    cultures.map(&:name).join(", ")
  end

  def culture_list=(list)
    self.cultures = list.split(",").map do |m|
      Culture.where(name: m.strip).first_or_create!
    end
  end

  def skill_list
    skills.map(&:name).join(", ")
  end

  def skill_list=(names)
    self.skills = names.split(",").map do |m|
      Skill.where(name: m.strip).first_or_create!
    end
  end

  def photo_id=(id)
    if @asset = Photo.find(id)
      self.photo = @asset
    else
      self.errors.add(:photo, "Asset not found")
    end
  end

  def resume_id=(id)
    if @asset = Resume.find(id)
      self.resume = @asset
    else
      self.errors.add(:resume, "Asset not found")
    end
  end

  def self.get_recent_conversations(talent_profile_id, count)
    count = (count == 0) ? DEFAULT_RECENT_CONVERSATIONS : count

    sql = %{select max(from_user_id) as from_user_id, position_id, ec.id as employer_contact_id, ec.name as employer_contact_name, max(m.created_at) as date from positions p inner join messages m on m.position_id=p.id
            inner join employer_contacts ec on ec.id=p.employer_contact_id
            inner join users u on u.id=ec.user_id
            inner join talents t on (t.user_id=from_user_id or t.user_id=to_user_id)
            inner join talent_profiles tp on tp.id=t.talent_profile_id
            where tp.id=#{talent_profile_id}
            group by position_id, ec.id
            order by date desc
            limit #{count}}
    array = ActiveRecord::Base.connection.execute(sql)
    results = array.map do |row|
      employer_contact = EmployerContact.find(row['employer_contact_id'])
      {
        job_id: row['position_id'],
        employer_contact_name: employer_contact.name,
        photo_url: employer_contact.photo != nil ? employer_contact.photo.url : '',
        last_message_date: row['date'],
        user_id: employer_contact.user.id
      }
    end
    results
  end
end
