# == Schema Information
#
# Table name: assets
#
#  id             :integer          not null, primary key
#  file_id        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  assetable_id   :integer
#  assetable_type :string
#  type           :string
#

class Asset < ActiveRecord::Base
  ASSET_TYPES = ["Resume", "Photo", "Video"]
  attachment :file
  belongs_to :assetable, polymorphic: true

  validates :type, presence: true
  validates :type, inclusion: { in: ASSET_TYPES }

  def url
    # never meant to be used in testing
    "https://s3.amazonaws.com/#{ENV['AWS_S3_BUCKET']}/store/#{file_id}"
  end
end
