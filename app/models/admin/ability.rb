class Admin::Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin? && !user.agent_admin?
      can :manage, :all
    else
      can :read, ActiveAdmin::Page, name: "Dashboard"
      can :read, TalentProfile
      can :read, Account
    end
  end
end
