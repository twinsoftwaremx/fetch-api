# == Schema Information
#
# Table name: skill_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  skill_id          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  rank              :integer
#
# Indexes
#
#  index_skill_associations_on_skill_id           (skill_id)
#  index_skill_associations_on_talent_profile_id  (talent_profile_id)
#

class SkillAssociation < ActiveRecord::Base
  belongs_to :talent_profile, required: true
  belongs_to :skill, required: true
  
  # FIXME: Uncomment this when front end logic reflects skill ranks
  # validates_inclusion_of :rank, in: (1..10).to_a
end
