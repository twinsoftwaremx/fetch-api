# == Schema Information
#
# Table name: account_culture_associations
#
#  id         :integer          not null, primary key
#  culture_id :integer
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_account_culture_associations_on_account_id  (account_id)
#  index_account_culture_associations_on_culture_id  (culture_id)
#

class AccountCultureAssociation < ActiveRecord::Base
  belongs_to :account, required: true
  belongs_to :culture, required: true
end
