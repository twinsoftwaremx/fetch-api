# == Schema Information
#
# Table name: skill_categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SkillCategory < ActiveRecord::Base
  has_many :skills

  validates :name, presence: true
end
