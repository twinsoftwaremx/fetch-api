# == Schema Information
#
# Table name: salary_ranges
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SalaryRange < ActiveRecord::Base
  has_many :talent_profiles
  has_many :positions

  validates :name, presence: true
end
