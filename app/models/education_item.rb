# == Schema Information
#
# Table name: education_items
#
#  id                :integer          not null, primary key
#  college           :string
#  year              :integer
#  degree            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

class EducationItem < ActiveRecord::Base
  belongs_to :talent_profile, required: true

  validates :college, :year, :degree, presence: true
end
