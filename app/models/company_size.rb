# == Schema Information
#
# Table name: company_sizes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CompanySize < ActiveRecord::Base
  has_many :preferred_company_size_associations
  has_many :talent_profiles, through: :preferred_company_size_associations

  validates :name, presence: true
end
