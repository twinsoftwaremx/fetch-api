# == Schema Information
#
# Table name: interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  interest_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_interest_associations_on_interest_id        (interest_id)
#  index_interest_associations_on_talent_profile_id  (talent_profile_id)
#

class InterestAssociation < ActiveRecord::Base
  belongs_to :interest, required: true
  belongs_to :talent_profile, required: true
end
