class FeeStructure

  FEE_PERCENTAGE = 0.17
  MAX_SALARY_RANGE = 76000
  UPFRONT_FEE_LOW = 1000
  UPFRONT_FEE_HIGH = 2000
  UPFRONT_FEE_PERCENTAGE = 0.5
  MONTHLY_SUBSCRIPTION_COUNT = 36

  attr_reader :id, :standard_fee, :monthly_subscription, :upfront_fee

  def calculate_fees(salary)
    salary = salary.to_f
    @upfront_fee = salary < MAX_SALARY_RANGE ? UPFRONT_FEE_LOW : UPFRONT_FEE_HIGH
    @standard_fee = (salary * FEE_PERCENTAGE).round(2) - (upfront_fee-(upfront_fee*UPFRONT_FEE_PERCENTAGE))
    if @standard_fee < 0
      @standard_fee = 0
    end
    # round to the tenth
    @monthly_subscription = ((standard_fee/MONTHLY_SUBSCRIPTION_COUNT)+4).round(-1)
    if @monthly_subscription < 0
      @monthly_subscription = 0
    end
  end
end