module TalentProfileSearchable
  extend ActiveSupport::Concern

  included do
    # Setup our Elastic Search indexes
    settings index: { number_of_shards: 1, number_of_replicas: 0 } do
      mapping dynamic: 'false' do
        indexes :id, type: 'integer', index: :not_analyzed
        indexes :name, type: 'multi_field' do
          indexes :name,     analyzer: 'snowball'
          indexes :tokenized, analyzer: 'simple'
        end

        indexes :summary, type: 'multi_field' do
          indexes :summary,   analyzer: 'snowball'
          indexes :tokenized, analyzer: 'simple'
        end
        indexes :skills, analyzer: 'keyword'
        indexes :interests, analyzer: 'keyword'
        indexes :cities_of_interest, analyzer: 'keyword'
        indexes :salary_range, analyzer: 'keyword'
        indexes :required_skills, analyzer: 'keyword'
        indexes :bonus_skills, analyzer: 'keyword'
      end
    end

    # Customize the JSON serialization for Elasticsearch
    def as_indexed_json(options={})
      result = self.as_json
      skills = self.skills.collect(&:name)
      result[:skills] = skills
      result['required_skills'] = skills
      result['bonus_skills'] = skills
      result[:cities_of_interest] = self.cities_of_interest.collect(&:name)
      if self.salary_range.present?
        result[:salary_range] = self.salary_range.name
      end
      result['interests'] = self.interests.map(&:name)
      result
    end

    def self.match(options={})
      @search_definition = Elasticsearch::DSL::Search.search do
        query do
          bool do
            must do
              terms skills: options[:required_skills], boost: 2.0, minimum_should_match: 1
            end
            should do
              terms skills: options[:bonus_skills]
            end
            must do
              term cities_of_interest: options[:city]
            end
            should do
              match salary_range: options[:salary_range]
            end
          end
        end
      end
      self.__elasticsearch__.search(@search_definition)
    end

    # Our customized per model search query
    def self.search(q, options={})
      @search_definition = Elasticsearch::DSL::Search.search do
        query do
          # if user query is blank match_all
          unless q.blank?
            bool do
              should do
                multi_match do
                  query q
                  fields ['name', 'summary']
                  operator 'or'
                end
              end

              if q.present? && options[:skills]
                should do
                  nested do
                    path :skills
                    query do
                      multi_match do
                        query q
                        fields 'name'
                        operator 'or'
                      end
                    end
                  end
                end
              end
            end
          else
            if options[:skills].present? && options[:for_position]
              bool do
                must do
                  term required_skills: options[:skills], boost: 2.0
                end
                must do
                  term bonus_skills: options[:skills]
                end
              end
            end
            match_all
          end
          # Location Geosearch
          if options[:location].present?
            filtered do
              filter do
                if options[:for_position]
                  nested do
                    path 'locations_of_interest'
                    filter do
                      geo_distance 'locations_of_interest.coordinates' => options[:location], distance: '10m'
                    end
                  end
                else
                  nested do
                    path 'location'
                    filter do
                      geo_distance 'location.coordinates' => options[:location], distance: '10m'
                    end
                  end
                end
              end
            end
          end
        end

      end
      self.__elasticsearch__.search(@search_definition)
    end
  end
end
