require 'elasticsearch/model'
require 'elasticsearch/dsl'

module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::DSL
    # Customize the index name
    #
    index_name [Rails.application.engine_name, self.model_name.name.downcase, Rails.env].join('_')

    # Set up callbacks for updating the index on model changes
    #
    after_commit -> { IndexerJob.perform_later('index',  self.class.to_s, self.id) }, on: :create
    after_commit -> { IndexerJob.perform_later('update', self.class.to_s, self.id) }, on: :update
    after_commit -> { IndexerJob.perform_later('delete', self.class.to_s, self.id) }, on: :destroy
    after_touch  -> { IndexerJob.perform_later('update', self.class.to_s, self.id) }
  end
end
