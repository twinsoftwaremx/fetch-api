# == Schema Information
#
# Table name: talent_profile_matches
#
#  id                :integer          not null, primary key
#  position_id       :integer
#  talent_profile_id :integer
#  match_score       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  status            :integer          default(0)
#
# Indexes
#
#  index_talent_profile_matches_on_position_id        (position_id)
#  index_talent_profile_matches_on_talent_profile_id  (talent_profile_id)
#

class TalentProfileMatch < ActiveRecord::Base
  belongs_to :position, required: true
  belongs_to :talent_profile, required: true
  has_one :offer

  enum status: [:new_match, :liked, :rejected]

  scope :ranked, -> { not_rejected.order('match_score ASC') }
  scope :not_rejected, -> { where(status: [0, 1]) }
end
