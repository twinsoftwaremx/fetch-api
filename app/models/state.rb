# == Schema Information
#
# Table name: states
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  abbreviation :string           not null
#

class State < ActiveRecord::Base
  has_many :cities

  validates :name, presence: true
  validates :abbreviation, presence: true
end
