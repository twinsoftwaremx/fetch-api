class MatchFinder
  def self.call(talent)
    new.call(talent)
  end

  def call(talent)
    matches = MatchFinder.for_talent(talent)
    existing_matches = talent.matches

    x = existing_matches.collect(&:id)
    y = matches.collect {|x| x[:id] }
    # This logic checks for differences on new matches vs existing. If new matches
    # returns empty (no matches) we should ignore them, otherwise find the difference
    # in id's.
    new_matches = y.empty? ? [] : (x-y)+(y-x)

    new_matches.each do |match|
      TalentProfileMatch.create talent_profile_id: talent.id, position_id: match, match_score: matches.find{|x| x[:id]==match}[:score]*1000
    end

    unless new_matches.empty?
      TalentMatchMailer.email_talent_match(talent, new_matches).deliver_later
    end
  end

  def self.for_talent(talent)
    options = {}
    options[:skills] = talent.skills.collect(&:name)
    options[:locations] = talent.cities_of_interest.collect(&:name)
    options[:cultures] = talent.cultures.collect(&:name)
    options[:salary_range] =
      talent.salary_range.present? ?
      talent.salary_range.name :
      ""
    [:skills, :salary_range, :locations, :cultures].map {|x| return [] if !options[x].present? }

    results = Position.match(options)
    results.map do |r|
      {
        id: r.id.to_i,
        score: r._score
      }
    end
  end
end
