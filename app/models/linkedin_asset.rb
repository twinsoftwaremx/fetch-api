# == Schema Information
#
# Table name: linkedin_assets
#
#  id              :integer          not null, primary key
#  recommendations :json
#  skills          :json
#  positions       :json
#  user_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  profile         :json
#
# Indexes
#
#  index_linkedin_assets_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_704acb6cfd  (user_id => users.id)
#

class LinkedinAsset < ActiveRecord::Base
  belongs_to :user
end
