# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  body         :text             not null
#  to_user_id   :integer          not null
#  from_user_id :integer          not null
#  position_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

class Message < ActiveRecord::Base
  belongs_to :to_user, required: true, class_name: 'User'
  belongs_to :from_user, required: true, class_name: 'User'
  belongs_to :position

  validates :body, presence: true

  after_create :notify_users

  def notify_users
    ::MessageMailer.notify_users(self.from_user, self.to_user, self.body).deliver_now!
  end
end
