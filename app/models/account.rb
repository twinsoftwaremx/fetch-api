# == Schema Information
#
# Table name: accounts
#
#  id               :integer          not null, primary key
#  company_name     :string
#  phone            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  summary          :text
#  benefits         :text
#  perks            :text
#  github_url       :string
#  company_site_url :string
#  address_id       :integer
#  culture          :integer          default(0)
#  agent_id         :integer
#  company_size_id  :integer
#  year_founded     :integer
#  revenue          :string
#  who_we_are       :text
#  what_we_do       :text
#  video_url        :string
#
# Indexes
#
#  index_accounts_on_company_size_id  (company_size_id)
#

class Account < ActiveRecord::Base
  belongs_to :address, class_name: 'FullAddress'
  has_many :employer_contacts
  has_many :positions, through: :employer_contacts
  belongs_to :agent
  has_many :account_culture_associations
  has_many :cultures, through: :account_culture_associations
  has_one :logo, as: :assetable, class_name: "Photo"
  belongs_to :company_size

  validates :company_name, presence: true

  def culture_list
    cultures.map(&:name).join(",")
  end

  def culture_list=(list)
    self.cultures = list.split(",").map do |m|
      Culture.where(name: m.strip).first_or_create!
    end
  end

  def logo_id=(id)
    if @asset = Photo.find(id)
      self.logo = @asset
    else
      self.errors.add(:logo, "Asset not found")
    end
  end

  def create_logo(base64_string)
    self.logo = Asset.new(type:'Photo')
    header, data = base64_string.split(',')
    file = Tempfile.new('logo')
    file.binmode
    tmp_file_name = file.path
    file.write(Base64.decode64(data))
    file.close
    file = File.open(tmp_file_name)
    self.logo.file = file
    file.close
  end
end
