# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cities_on_state_id  (state_id)
#

class City < ActiveRecord::Base
  belongs_to :state

  validates :name, presence: true

  def full_name
    "#{name}, #{state.abbreviation}"
  end
end
