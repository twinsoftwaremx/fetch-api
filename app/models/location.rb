# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  line1      :string
#  line2      :string
#  city       :string
#  state      :string
#  zip        :string
#  type       :string
#  latitude   :float
#  longitude  :float
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_addresses_on_type  (type)
#

class Location < Address
  validates :city, :state, :zip, presence: true

  has_many :positions
  has_many :talents, foreign_key: 'location_id'
  has_many :location_of_interest_association
  has_many :talent_profiles, through: :location_of_interest_association

  geocoded_by :geocode_address
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      obj.city = geo.city
      obj.state = geo.state
      obj.zip = geo.postal_code
    end
  end

  after_validation :geocode
  after_validation :reverse_geocode

  def geocode_address
    "#{city} #{state} #{zip}"
  end
end
