# == Schema Information
#
# Table name: cultures
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Culture < ActiveRecord::Base
  has_many :account_culture_associations
  has_many :accounts, through: :account_culture_associations
  has_many :talent_culture_associations
  has_many :talent_profiles, through: :talent_culture_associations, source: :talent_profile
  has_one :photo, as: :assetable, class_name: "Photo"

  validates :name, presence: true

  def photo_id=(id)
    if @asset = Photo.find(id)
      self.photo = @asset
    else
      self.errors.add(:photo, "Asset not found")
    end
  end
end
