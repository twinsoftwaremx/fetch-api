# == Schema Information
#
# Table name: agents
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Agent < ActiveRecord::Base
  has_one :admin_user
  has_many :talent_profiles
  has_many :accounts
  has_one :photo, as: :assetable, class_name: 'Photo'

  validates :name, presence: true

  def admin?
    true
  end

  def photo_id=(id)
    if @asset = Photo.find(id)
      self.photo = @asset
    else
      self.errors.add(:photo, "Asset not found")
    end
  end
end
