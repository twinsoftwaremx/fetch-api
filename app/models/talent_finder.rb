class TalentFinder

  def self.call(position)
    matches = TalentFinder.for_position(position)

    matches.each do |match|
      record = TalentProfileMatch.where(talent_profile_id: match[:id], position_id: position.id)
      if record.blank?
        TalentProfileMatch.create!(talent_profile_id: match[:id], position_id: position.id, match_score: match[:score] * 1000)
        talent = TalentProfile.find(match[:id])
        TalentMatchMailer.email_talent_match(talent, [position.id]).deliver_later
      end
    end
  end

  def self.for_position(position)
    return [] if position.nil?
    options = {}
    options[:for_position] = true
    options[:required_skills] = position.required_skills.collect(&:name)
    options[:bonus_skills] = position.bonus_skills.collect(&:name)
    options[:salary_range] = position.salary_range.name
    options[:city] = position.city.name

    results = TalentProfile.match(options)
    res = results.map do |r|
      {
        id: r.id.to_i,
        score: r._score
      }
    end
  end
end
