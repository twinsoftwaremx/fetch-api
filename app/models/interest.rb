# == Schema Information
#
# Table name: interests
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Interest < ActiveRecord::Base
  has_many :interest_associations
  has_many :talent_profiles, through: :interest_associations

  validates :name, presence: true
end
