class Auth::LinkedinAuthenticator < Auth::BaseAuthenticator
  def name
    "linkedin"
  end

  def authenticate(auth_hash, auth_params={}, current_user=nil)
    user = process(auth_hash, current_user)
    user = after_process(auth_hash, auth_params, user)
    user
  end

  private
  def create_profile_from_hash(hash)
    profile = TalentProfile.new
    name = hash.info.name
    profile.name = name if name.present?
    profile.time_zone = "EST"
    url = hash.info.urls.public_profile
    profile.linked_in_url = url if url.present?
    avatar = hash.info.image
    profile.avatar_url = avatar if avatar.present?
    profile.save
    profile
  end
end
