class Auth::GithubAuthenticator < Auth::BaseAuthenticator
  def name
    "github"
  end

  def authenticate(auth_hash, auth_params={}, current_user = nil)
    user = process(auth_hash, current_user)
    user = after_process(auth_hash, auth_params, user)
    user
  end

  def create_profile_from_hash(hash)
    profile = TalentProfile.new
    name = hash.info.name
    profile.name = name if name.present?
    profile.time_zone = "EST"
    url = hash.info.urls["Github"]
    profile.github_url = url if url.present?
    avatar = hash.info.image
    profile.avatar_url = avatar if avatar.present?
    profile.save
    profile
  end

end
