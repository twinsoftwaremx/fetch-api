class ApplicationMailer < ActionMailer::Base
  default from: "noreply@api.fetch.com"
end
