class TalentMatchMailer < ApplicationMailer
  def send_test(email)
    @email = email
    mail(to: @email, subject: 'Welcome to My Awesome Site')
  end

  def email_talent_match(talent, matches)
    @talent = talent
    @new_matches = Position.find(matches)
    mail(to: @talent.user.email, subject: "You have been matched on Fetch!")
  end

  def email_talent_interest(talent, match)
    @talent = talent
    @position = match.position
    @employer = @position.employer_contact
    mail(to: @employer.user.email, subject: "Talent interested on position!")
  end
end
