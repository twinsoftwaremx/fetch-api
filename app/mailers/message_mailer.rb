class MessageMailer < ApplicationMailer
  def send_test(email)
    @email = email
    mail(to: @email, subject: 'Welcome to My Awesome Site')
  end

  def notify_users(from_user, to_user, message_body)
    @from_user = from_user
    @to_user = to_user
    @message_body = message_body
    mail(to: to_user.email, from: from_user.email, subject: "You've recieved a message on Fetch!")
  end
end
