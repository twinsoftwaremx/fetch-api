class EmployerContactMailer < ApplicationMailer
  def invite_coworker(email, link)
    @link = link
    mail(to: email, subject: "Invitation to register on Fetch!")
  end
end
