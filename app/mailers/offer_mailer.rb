class OfferMailer < ApplicationMailer
  def email_make_offer(offer)
    @talent = offer.talent_profile_match.talent_profile
    @offer = offer
    mail(to: @talent.user.email, subject: "You have received a job offer!")
  end

  def email_accept_offer(offer)
    @talent = offer.talent_profile_match.talent_profile
    @position = offer.talent_profile_match.position
    @employer = offer.employer_contact
    mail(to: @employer.user.email, subject: "Talent accepted the offer!")
  end

  def email_reject_offer(offer)
    @talent = offer.talent_profile_match.talent_profile
    @position = offer.talent_profile_match.position
    @employer = offer.employer_contact
    mail(to: @employer.user.email, subject: "Talent rejected the offer!")
  end
end
