class UserMailer < ApplicationMailer
  def send_test(email)
    @email = email
    mail(to: @email, subject: 'Welcome to My Awesome Site')
  end
end
