class MessagePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.where('from_user_id = ? OR to_user_id = ?', @user.id, @user.id)
    end
  end

  def show?
    is_owner?
  end

  def create?
    true # Any authenticated user can send a message
  end

  def update?
    is_owner?
  end

  def destroy?
    is_owner?
  end

  private
  def is_owner?
    @user == @record.from_user
  end
end
