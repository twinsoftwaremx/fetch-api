class SearchPolicy < ApplicationPolicy
  def talent_profile?
    true
  end

  def talent_by_position?
    true
  end
 end
