class AccountPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    @user.has_role? :account_admin
  end

  def update?
    user_is_contact? and @user.has_role? :account_admin
  end

  def destroy?
    user_is_contact? and @user.has_role? :account_admin
  end

  private
  def user_is_contact?
    result = false
    @record.employer_contacts.each do |contact|
      if @user == contact.user
        result = true
        break
      end
    end
    result
  end
end
