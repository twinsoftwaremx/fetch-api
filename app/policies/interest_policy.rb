class InterestPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if @user.has_role? :admin or @user.has_role? :account_admin or @user.has_role? :employer_contact
        scope.all
      else
        []
      end
    end
  end

  def show?
    user_has_allowed_role?
  end

  def create?
    @user.has_role? :admin
  end

  def update?
    @user.has_role? :admin
  end

  def destroy?
    @user.has_role? :admin
  end

  def user_has_allowed_role?
    result = false
    [:admin, :account_admin, :employer_contact].each do |role|
      if @user.has_role? role
        result = true
        break
      end
    end
    result
  end
end
