class FeeStructurePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def get_fees?
    user_roles = @user.roles.collect(&:name)
    allowed_roles = ['account_admin', 'admin', 'employer_contact']
    (user_roles & allowed_roles).any?
  end
end
