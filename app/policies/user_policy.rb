class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    is_user?
  end

  def sign_out?
    is_user?
  end

  private 
  def is_user?
    @user == @record
  end
end
