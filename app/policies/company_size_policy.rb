class CompanySizePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def show?
    true
  end

  def create?
    @user.has_role? :admin
  end

  def update?
    @user.has_role? :admin
  end

  def destroy?
    @user.has_role? :admin
  end

  def user_has_allowed_role?
    result = false
    [:admin, :account_admin, :employer_contact].each do |role|
      if @user.has_role? role
        result = true
        break
      end
    end
    result
  end
end
