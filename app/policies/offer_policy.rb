class OfferPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      case user.role
      when 'talent'
        user.talent_profile.offers
      when 'admin', 'account_admin', 'employer_contact'
        user.employer_contact.offers
      else
        []
      end
    end
  end

  def show?
    case user.role
    when 'talent'
      user.talent_profile.offer_ids.include?(record.id)
    when 'admin', 'account_admin', 'employer_contact'
      user.employer_contact.offer_ids.include?(record.id)
    else
      false
    end
  end

  def create?
    has_allowed_role?
  end

  def destroy?
    if has_allowed_role? &&
        user.employer_contact.offer_ids.include?(record.id)
      true
    else
      false
    end
  end

  def accept?
    show?
  end

  def reject?
    show?
  end

  private
  def has_allowed_role?
    user_roles = user.roles.collect(&:name)
    allowed_roles = ['admin', 'account_admin', 'employer_contact']

    !((allowed_roles & user_roles).empty?)
  end
end
