class TalentProfilePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    is_owner?
  end

  def create?
    # Need to define authorization rules for this action
    true
  end

  def update?
    is_owner?
  end

  def destroy?
    is_owner?
  end

  def view_object_list?
    is_owner?
  end

  def add_object?
    is_owner?
  end

  private
  def is_owner?
    @user == @record.user
  end
end
