# == Schema Information
#
# Table name: talent_profiles
#
#  id                                           :integer          not null, primary key
#  name                                         :string           not null
#  time_zone                                    :string           not null
#  phone                                        :string
#  summary                                      :text
#  area_of_interest                             :string
#  years_of_experience                          :integer          default(0)
#  usdod_clearance                              :boolean          default(FALSE)
#  reason_for_interest_in_new_job_opportunities :text
#  job_search_status                            :integer          default(0)
#  usa_work_auth_type                           :integer          default(0)
#  linked_in_url                                :string
#  personal_website                             :string
#  dribbble_url                                 :string
#  stack_overflow_url                           :string
#  blog_url                                     :string
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  location_id                                  :integer
#  agent_id                                     :integer
#  salary_range_id                              :integer
#  github_url                                   :string
#  avatar_url                                   :string
#  title                                        :string
#  company                                      :string
#

class TalentProfileSerializer < ActiveModel::Serializer
  attributes :id, :name, :time_zone, :phone, :summary, :area_of_interest,
    :years_of_experience, :usdod_clearance, :reason_for_interest_in_new_job_opportunities,
    :job_search_status, :preferred_base_salary, :usa_work_auth_type, :linked_in_url,
    :personal_website, :dribbble_url, :stack_overflow_url, :blog_url, :skill_list, :culture_list,
    :photo_url, :resume_url, :agent_id, :locations, :city_list, :salary_range_name, :location_name,
    :title, :company, :user_id
  belongs_to :salary_range

  def locations

    object.locations_of_interest.map(&:city).join(", ") unless object.locations_of_interest.nil?
  end

  def salary_range_name
    if object.salary_range.present?
      object.salary_range.name
    end
  end

  def location_name
    if object.location.present?
      object.location.name
    end
  end

  def photo_url
    if object.photo.present?
      object.photo.url
    elsif object.avatar_url.present?
      object.avatar_url
    end
  end

  def resume_url
    if object.resume.present?
      object.resume.url
    end
  end

  def user_id
    if object.user.present?
      object.user.id
    end
  end
end
