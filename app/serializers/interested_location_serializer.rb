# == Schema Information
#
# Table name: interested_locations
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class InterestedLocationSerializer < ActiveModel::Serializer
  attributes :id, :name
end
