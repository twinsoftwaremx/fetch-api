# == Schema Information
#
# Table name: company_sizes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CompanySizeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
