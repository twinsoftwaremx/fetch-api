# == Schema Information
#
# Table name: positions
#
#  id                       :integer          not null, primary key
#  job_title                :string           not null
#  description              :text
#  hours                    :integer          default(0)
#  desired_years_experience :integer          default(0)
#  team_size                :string
#  employment_type_id       :integer          not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  employer_contact_id      :integer
#  remote                   :boolean          default(FALSE)
#  location_id              :integer
#  salary_range_id          :integer
#  city_id                  :integer
#  hiring_manager           :string
#  is_deleted               :boolean          default(FALSE)
#
# Indexes
#
#  index_positions_on_city_id              (city_id)
#  index_positions_on_employer_contact_id  (employer_contact_id)
#  index_positions_on_employment_type_id   (employment_type_id)
#

class PositionSerializer < ActiveModel::Serializer
  attributes :id, :job_title, :description, :hours, :desired_years_experience,
             :team_size, :cultures, :required_skill_list, :logo_url, :salary_range_name,
             :city_name, :employment_type_name, :employer_contact_name, :hiring_manager,
             :employer_user_id
             # , :employment_type,
             # :city, :salary_range, :employer_contact
  belongs_to :employment_type
  belongs_to :employer_contact
  belongs_to :salary_range
  belongs_to :city

  def cultures
    object.account.culture_list
  end

  def logo_url
    if object.account.logo.present?
      object.account.logo.url
    end
  end

  def salary_range_name
    if object.salary_range.present?
      object.salary_range.name
    end
  end

  def city_name
    if object.city.present?
      object.city.full_name
    end
  end

  def employment_type_name
    if object.employment_type.present?
      object.employment_type.name
    end
  end

  def employer_contact_name
    if object.employer_contact.present?
      object.employer_contact.name
    end
  end

  def employer_user_id
    if object.employer_contact.present?
      object.employer_contact.user_id
    end
  end
end
