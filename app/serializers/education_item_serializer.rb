# == Schema Information
#
# Table name: education_items
#
#  id                :integer          not null, primary key
#  college           :string
#  year              :integer
#  degree            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

class EducationItemSerializer < ActiveModel::Serializer
  attributes :id, :college, :year, :degree
end
