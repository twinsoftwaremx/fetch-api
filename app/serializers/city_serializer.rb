# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cities_on_state_id  (state_id)
#

class CitySerializer < ActiveModel::Serializer
  attributes :id, :name, :full_name

  belongs_to :state

  def full_name
    "#{name}, #{state.abbreviation}"
  end
end
