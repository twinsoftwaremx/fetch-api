# == Schema Information
#
# Table name: talent_profile_matches
#
#  id                :integer          not null, primary key
#  position_id       :integer
#  talent_profile_id :integer
#  match_score       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  status            :integer          default(0)
#
# Indexes
#
#  index_talent_profile_matches_on_position_id        (position_id)
#  index_talent_profile_matches_on_talent_profile_id  (talent_profile_id)
#

class TalentProfileMatchSerializer < ActiveModel::Serializer
  attributes :id, :status, :match_score, :position_id, :talent_profile_id

  belongs_to :position
  belongs_to :talent_profile
end
