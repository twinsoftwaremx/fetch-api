# == Schema Information
#
# Table name: previous_employments
#
#  id                :integer          not null, primary key
#  company           :string
#  position          :string
#  start_date        :date
#  end_date          :date
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

class PreviousEmploymentSerializer < ActiveModel::Serializer
  attributes :id, :company, :position, :start_date, :end_date, :description
end
