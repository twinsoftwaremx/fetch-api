# == Schema Information
#
# Table name: employer_contacts
#
#  id            :integer          not null, primary key
#  name          :string           not null
#  title         :string
#  linked_in_url :string
#  summary       :text
#  phone         :string
#  time_zone     :string
#  account_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#
# Indexes
#
#  index_employer_contacts_on_account_id  (account_id)
#  index_employer_contacts_on_user_id     (user_id)
#

class EmployerContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :title, :linked_in_url, :summary, :phone,
             :time_zone, :photo_url
  belongs_to :account
  belongs_to :user

  def photo_url
    if object.photo.present?
      object.photo.url
    end
  end
end
