# == Schema Information
#
# Table name: agents
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AgentSerializer < ActiveModel::Serializer
  attributes :id, :name, :photo_url, :status

  def status
    "ONLINE NOW"
  end

  def photo_url
    if object.photo.present?
      object.photo.url
    end
  end
end
