# == Schema Information
#
# Table name: salary_ranges
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SalaryRangeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
