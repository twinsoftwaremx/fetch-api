# == Schema Information
#
# Table name: skills
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  skill_category_id :integer
#

class SkillSerializer < ActiveModel::Serializer
  attributes :id, :name

  belongs_to :skill_category
end
