# == Schema Information
#
# Table name: employment_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EmploymentTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
