# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  line1      :string
#  line2      :string
#  city       :string
#  state      :string
#  zip        :string
#  type       :string
#  latitude   :float
#  longitude  :float
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_addresses_on_type  (type)
#

#  id               :integer          not null, primary key
#  line1            :string
#  line2            :string
#  city             :string
#  state            :string
#  zip              :string
#  type             :string
#  latitude         :float
#  longitude        :float
#  addressable_id   :integer
#  addressable_type :string
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_addresses_on_addressable_id    (addressable_id)
#  index_addresses_on_addressable_type  (addressable_type)
#  index_addresses_on_type              (type)
#

class LocationSerializer < ActiveModel::Serializer
  attributes :id, :line1, :line2, :city, :state, :zip, :type
end
