# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  body         :text             not null
#  to_user_id   :integer          not null
#  from_user_id :integer          not null
#  position_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

class MessageSerializer < ActiveModel::Serializer
  attributes :id, :body, :created_at, :to_user, :from_user

  belongs_to :to_user
  belongs_to :from_user
  belongs_to :position

  def to_user
    {
      name: get_attribute_from_association('name', 'to_user'),
      avatar: get_attribute_from_association('photo', 'to_user'),
      type: get_user_type('to_user')
    }
  end

  def from_user
    {
      name: get_attribute_from_association('name', 'from_user'),
      avatar: get_attribute_from_association('photo', 'from_user'),
      type: get_user_type('from_user')
    }
  end

  private
  def get_attribute_from_association(attribute, association)
    if object.send(association).present?
      if object.send(association).talent_profile.present?
        object.send(association).talent_profile.send(attribute)
      elsif object.send(association).employer_contact.present?
        object.send(association).employer_contact.send(attribute)
      else
        ''
      end
    end
  end

  def get_user_type(association)
    if object.send(association).present?
      if object.send(association).talent_profile.present?
        'talent_profile'
      elsif object.send(association).employer_contact.present?
        'employer_contact'
      else
        ''
      end
    end
  end
end
