# == Schema Information
#
# Table name: preferred_company_size_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  company_size_id   :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_preferred_company_size_associations_on_company_size_id    (company_size_id)
#  index_preferred_company_size_associations_on_talent_profile_id  (talent_profile_id)
#

class PreferredCompanySizeAssociationSerializer < ActiveModel::Serializer
  attributes :id
  belongs_to :company_size
end
