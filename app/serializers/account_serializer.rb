# == Schema Information
#
# Table name: accounts
#
#  id               :integer          not null, primary key
#  company_name     :string
#  phone            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  summary          :text
#  benefits         :text
#  perks            :text
#  github_url       :string
#  company_site_url :string
#  address_id       :integer
#  culture          :integer          default(0)
#  agent_id         :integer
#  company_size_id  :integer
#  year_founded     :integer
#  revenue          :string
#  who_we_are       :text
#  what_we_do       :text
#  video_url        :string
#
# Indexes
#
#  index_accounts_on_company_size_id  (company_size_id)
#

class AccountSerializer < ActiveModel::Serializer

  attributes :id, :company_name, :phone, :summary, :benefits, :perks, :culture,
             :github_url, :company_site_url, :logo_url, :year_founded, :revenue,
             :who_we_are, :what_we_do, :video_url, :company_size_name, :address_name

  belongs_to :address
  belongs_to :company_size
  belongs_to :agent

  def logo_url
    if object.logo.present?
      object.logo.url
    end
  end

  def company_size_name
    if object.company_size.present?
      object.company_size.name
    end
  end

  def address_name
    if object.address.present?
      object.address.geocode_address
    end
  end
end
