# == Schema Information
#
# Table name: location_of_interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  location_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  idx_loc_int_on_intlocid                                       (location_id)
#  index_location_of_interest_associations_on_talent_profile_id  (talent_profile_id)
#

class LocationOfInterestAssociationSerializer < ActiveModel::Serializer
  attributes :id
  belongs_to :location
end
