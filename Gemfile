source 'https://rubygems.org'


gem 'rails', '4.2.3'

gem 'rails-api'

gem 'pg'

gem 'active_model_serializers', '~> 0.10.0.rc1'

gem 'refile', require: 'refile/rails'
gem 'refile-s3'
gem 'aws-sdk'
gem 'devise'
gem 'geocoder'

gem 'rollbar', '~> 1.5.3'
gem 'skylight'
gem 'mailgun'

# Active Admin
gem 'activeadmin', github: 'activeadmin'
gem 'cancan' # or cancancan
gem 'draper'
gem 'pundit'
gem 'rolify'

gem 'apitome'
gem 'omniauth'
gem 'omniauth-github'
gem 'omniauth-linkedin'
gem 'remote_syslog_logger'
# Background jobs
gem 'slim'
gem 'sinatra', require: nil
gem 'sidekiq'
gem 'sidetiq'

gem 'elasticsearch-dsl'
gem 'elasticsearch-model'
gem 'elasticsearch-rails'

gem 'github_api'
gem 'linkedin'
gem 'rack-cors', :require => 'rack/cors'

gem 'dotenv-rails'
gem 'uglifier'
gem 'unicorn'
gem 'gibberish'

group :development, :test do
  gem 'database_cleaner'
  gem 'pry'
  gem 'faker'
  gem 'shoulda-matchers', '~> 2.8.0', require: false
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'rspec_api_documentation'
end

group :development do
  gem 'annotate', '~> 2.6.6'
end

group :test do
  gem 'elasticsearch-extensions'
  gem 'simplecov', :require => false
  gem 'timecop'
end
# To use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano', :group => :development

# To use debugger
# gem 'ruby-debug19', :require => 'ruby-debug'
