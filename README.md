# api.fetch.com

### ENV Variables

In `.env`, please refer to the Isotope11 Clients' datavault for the relevant keys.

    SECRET_KEY_BASE="393jfkdhf83hszgej347Hk383hK38fhkl2l3"
    AWS_ACCESS_KEY="xxxx"
    AWS_SECRET_KEY="xxxxx"
    AWS_S3_REGION="us-east-1"
    AWS_S3_BUCKET="fetch-api-production"
    ROLLBAR_ACCESS_TOKEN="xxx"
    ROLLBAR_ACCESS_TOKEN="xxx"
    SKYLIGHT_AUTHENTICATION="xxx"
    MAILGUN_USERNAME="xxx"
    MAILGUN_PASSWORD="xxx"
    MAILGUN_DOMAIN="xxx"
    HOSTNAME="http://localhost:3000/"
    LINKEDIN_KEY="<YOUR CONSUMER KEY>"
    LINKEDIN_SECRET="<YOUR CONSUMER SECRET KEY>"

### Documentation

To generate docs:

	$ rake docs:generate

Docs are reachable at:
`/docs/api`

### TODO

#### Papertrail

Once we have servers, these tasks should be done to enable papertrail logging


**Find Out Which Logger System is Running**

```
ls -d /etc/*syslog*
```

If it's Ubuntu it'll probably be `rsyslog`

edit `/etc/rsyslog.conf` as root and add the following line to the bottom:

```
*.*          @<SUBDOMAIN>.papertrailapp.com:<PORT>
```

then `sudo service rsyslog restart`

### Elastic Search in Development

    brew install elasticsearch
    elasticsearch --config=/usr/local/opt/elasticsearch/config/elasticsearch.yml

ES should be running on port 9200

Run `rake environment elasticsearch:import:all DIR=app/models FORCE=y`
to force reindexing.

Tests use a mock elastic-search cluster but still requires the ES libraries. If tests are failing
sporadically for you and spitting out "Address in use errors", check the default 9200 port if in use
with `sudo lsof -i :9200` and kill it.


