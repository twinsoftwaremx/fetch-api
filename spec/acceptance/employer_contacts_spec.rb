require 'acceptance_helper'

resource "EmployerContacts" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/employer_contacts" do
    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"
    response_field :title, "Title"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :summary, "Summary"
    response_field :phone, "Phone"
    response_field :time_zone, "Time zone"
    response_field :account, "Account"
    response_field :user, "User"

    before do
      create_list(:employer_contact, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of employer contacts" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 3
    end
  end

  get "/employer_contacts/:id" do
    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"
    response_field :title, "Title"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :summary, "Summary"
    response_field :phone, "Phone"
    response_field :time_zone, "Time zone"
    response_field :account, "Account"
    response_field :user, "User"

    parameter :id, "Id"

    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:id) { employer_contact.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an employer contact by Id" do
      serialized_object = (ActiveModel::Serializer.adapter.new(EmployerContactSerializer.new(employer_contact)).to_json)[0..-2]

      expect(status).to eq(200)
      expect(response_body).to include(serialized_object)
    end
  end

  post "/employer_contacts" do
    parameter :name, "Name", scope: :employer_contact
    parameter :photo_id, "Photo Id", scope: :employer_contact
    parameter :title, "Title", scope: :employer_contact
    parameter :linked_in_url, "LinkedIn URL", scope: :employer_contact
    parameter :summary, "Summary", scope: :employer_contact
    parameter :phone, "Phone", scope: :employer_contact
    parameter :time_zone, "Time zone", scope: :employer_contact
    parameter :account_id, "Account Id", scope: :employer_contact

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"
    response_field :title, "Title"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :summary, "Summary"
    response_field :phone, "Phone"
    response_field :time_zone, "Time zone"
    response_field :account, "Account"
    response_field :user, "User"

    let(:account) { create(:account) }

    let(:name) { Faker::Name.name }
    let(:photo_id) { create(:photo).id }
    let(:title) { Faker::Name.title }
    let(:linked_in_url) { Faker::Internet.url }
    let(:summary) { 'Test Summary' }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:time_zone) { 'time zone' }
    let(:account_id) { account.id }

    let(:raw_post) { params.to_json }

    example_request "Creating an employer contact" do
      expect(status).to eq(201)
    end
  end

  put "/employer_contacts/:id" do
    parameter :name, "Name", scope: :employer_contact
    parameter :photo_id, "Photo Id", scope: :employer_contact
    parameter :title, "Title", scope: :employer_contact
    parameter :linked_in_url, "LinkedIn URL", scope: :employer_contact
    parameter :summary, "Summary", scope: :employer_contact
    parameter :phone, "Phone", scope: :employer_contact
    parameter :time_zone, "Time zone", scope: :employer_contact
    parameter :account_id, "Account Id", scope: :employer_contact

    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:id) { employer_contact.id }
    let(:name) { "New name" }
    let(:title) { "New title" }
    let(:summary) { 'Updated Summary' }

    let(:raw_post) { params.to_json }

    example_request "Updating an employer contact" do
      expect(status).to eq(204)
    end
  end

  put "/employer_contacts/:id" do
    parameter :name, "Name", scope: :employer_contact
    parameter :photo_id, "Photo Id", scope: :employer_contact
    parameter :title, "Title", scope: :employer_contact
    parameter :linked_in_url, "LinkedIn URL", scope: :employer_contact
    parameter :summary, "Summary", scope: :employer_contact
    parameter :phone, "Phone", scope: :employer_contact
    parameter :time_zone, "Time zone", scope: :employer_contact
    parameter :account_id, "Account Id", scope: :employer_contact

    let(:employer_contact) { create(:employer_contact) }
    let(:id) { employer_contact.id }
    let(:name) { "New name" }
    let(:title) { "New title" }
    let(:summary) { 'Updated Summary' }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when updating an employer contact from another user" do
      expect(status).to eq(403)
    end
  end

  delete "/employer_contacts/:id" do
    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:id) { employer_contact.id }

    example_request "Deleting an employer contact by Id" do
      expect(status).to eq(204)
    end
  end

  delete "/employer_contacts/:id" do
    let(:employer_contact) { create(:employer_contact) }
    let(:id) { employer_contact.id }

    example_request "User gets an error when deleting an employer contact by Id from another user" do
      expect(status).to eq(403)
    end
  end

  post "/employer_contacts/:id/invite_coworkers" do
    parameter :id, "Employer Contact Id"
    parameter :emails, "Array of emails to send the invite"

    before do
      user.add_role :account_admin
    end

    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:id) { employer_contact.id }

    let(:emails) { ['coworker1@test.com', 'coworker2@test.com'] }

    let(:raw_post) { params.to_json }

    example_request "User with admin_account role send email to invite coworkers" do
      expect(status).to eq 200
    end
  end

  get "/employer_contacts/:id/get_recent_conversations" do
    parameter :id, "Employer Contact Id"
    parameter :count, "Number of conversations"

    before do
      user.add_role :employer_contact

      @employer_contact = create(:employer_contact, :with_positions, number_of_positions: 3)
      talent_profile1 = create(:talent_profile)
      talent_profile2 = create(:talent_profile)
      talent_profile3 = create(:talent_profile)
      talent_profile4 = create(:talent_profile)
      talent_profile5 = create(:talent_profile)
      
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile1.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile2.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile3.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile4.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile5.user, 
              position: @employer_contact.positions.first)
    end

    let(:id) { @employer_contact.id }
    let(:count) { 3 }

    let(:raw_post) { params.to_json }

    example_request "User with employer_contact role can get the most recent conversations" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response.count).to eq 3
    end
  end
end
