require 'acceptance_helper'

resource "Talent Profiles" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/talent_profiles/:id" do
    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo, "Photo"
    response_field :resume, "Resume"
    response_field :time_zone, "Time zone"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :area_of_interest, "Area of Interest"
    response_field :years_of_experience, "Years of Experience"
    response_field :usdod_clearance, "US DoD Clearance"
    response_field :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities"
    response_field :job_search_status, "Job Search Status"
    response_field :preferred_base_salary, "Preferred Base Salary"
    response_field :usa_work_auth_type, "USA Work Auth Type"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :personal_website, "Personal Website"
    response_field :dribbble_url, "Dribbble URL"
    response_field :stack_overflow_url, "StackOverflow URL"
    response_field :blog_url, "Blog URL"

    parameter :id, "Id"

    let(:talent_profile) { create(:talent_profile, user: user) }

    let(:id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a talent request by Id" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        TalentProfileSerializer.new(talent_profile)).to_json
    end
  end

  get "/talent_profiles/:id" do
    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo, "Photo"
    response_field :resume, "Resume"
    response_field :time_zone, "Time zone"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :area_of_interest, "Area of Interest"
    response_field :years_of_experience, "Years of Experience"
    response_field :usdod_clearance, "US DoD Clearance"
    response_field :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities"
    response_field :job_search_status, "Job Search Status"
    response_field :preferred_base_salary, "Preferred Base Salary"
    response_field :usa_work_auth_type, "USA Work Auth Type"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :personal_website, "Personal Website"
    response_field :dribbble_url, "Dribbble URL"
    response_field :stack_overflow_url, "StackOverflow URL"
    response_field :blog_url, "Blog URL"

    parameter :id, "Id"

    let(:talent_profile) { create(:talent_profile) }
    let(:id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a talent profile by Id from another user" do
      expect(status).to eq(403)
    end
  end

  post "/talent_profiles" do

    parameter :name, "Name", scope: :talent_profile
    parameter :photo_id, "Photo", scope: :talent_profile
    parameter :resume_id, "Resume", scope: :talent_profile
    parameter :time_zone, "Time zone", scope: :talent_profile
    parameter :phone, "Phone", scope: :talent_profile
    parameter :summary, "Summary", scope: :talent_profile
    parameter :area_of_interest, "Area of Interest", scope: :talent_profile
    parameter :years_of_experience, "Years of Experience", scope: :talent_profile
    parameter :usdod_clearance, "US DoD Clearance", scope: :talent_profile
    parameter :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities", scope: :talent_profile
    parameter :job_search_status, "Job Search Status", scope: :talent_profile
    parameter :preferred_base_salary, "Preferred Base Salary", scope: :talent_profile
    parameter :usa_work_auth_type, "USA Work Auth Type", scope: :talent_profile
    parameter :linked_in_url, "LinkedIn URL", scope: :talent_profile
    parameter :personal_website, "Personal Website", scope: :talent_profile
    parameter :dribbble_url, "Dribbble URL", scope: :talent_profile
    parameter :stack_overflow_url, "StackOverflow URL", scope: :talent_profile
    parameter :blog_url, "Blog URL", scope: :talent_profile

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo, "Photo"
    response_field :resume, "Resume"
    response_field :time_zone, "Time zone"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :area_of_interest, "Area of Interest"
    response_field :years_of_experience, "Years of Experience"
    response_field :usdod_clearance, "US DoD Clearance"
    response_field :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities"
    response_field :job_search_status, "Job Search Status"
    response_field :preferred_base_salary, "Preferred Base Salary"
    response_field :usa_work_auth_type, "USA Work Auth Type"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :personal_website, "Personal Website"
    response_field :dribbble_url, "Dribbble URL"
    response_field :stack_overflow_url, "StackOverflow URL"
    response_field :blog_url, "Blog URL"

    let(:talent_profile) { create(:talent_profile, user: user) }

    let(:name) { Faker::Name.name }
    let(:photo_id) { create(:photo).id }
    let(:resume_id) { create(:resume).id }
    let(:time_zone) { "EST" }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:summary) { "Test summary" }
    let(:area_of_interest) { "area of interest" }
    let(:years_of_experience) { 3 }
    let(:usdod_clearance) { true }
    let(:reason_for_interest_in_new_job_opportunities) { "reason" }
    let(:job_search_status) { "ready" }
    let(:salary_range_id) { create(:salary_range).id }
    let(:usa_work_auth_type) { "us_citizen" }
    let(:linked_in_url) { Faker::Internet.url }
    let(:personal_website) { Faker::Internet.url }
    let(:dribbble_url) { Faker::Internet.url }
    let(:stack_overflow_url) { Faker::Internet.url }
    let(:blog_url) { Faker::Internet.url }

    let(:raw_post) { params.to_json }

    example_request "Creating a talent_profile" do
      expect(status).to eq(201)
    end
  end

  post "/talent_profiles" do
    parameter :name, "Name", scope: :talent_profile
    parameter :time_zone, "Time zone", scope: :talent_profile
    parameter :skills, "Skill Array"

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo, "Photo"
    response_field :resume, "Resume"
    response_field :time_zone, "Time zone"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :area_of_interest, "Area of Interest"
    response_field :years_of_experience, "Years of Experience"
    response_field :usdod_clearance, "US DoD Clearance"
    response_field :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities"
    response_field :job_search_status, "Job Search Status"
    response_field :preferred_base_salary, "Preferred Base Salary"
    response_field :usa_work_auth_type, "USA Work Auth Type"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :personal_website, "Personal Website"
    response_field :dribbble_url, "Dribbble URL"
    response_field :stack_overflow_url, "StackOverflow URL"
    response_field :blog_url, "Blog URL"

    let(:talent_profile) { create(:talent_profile, user: user) }

    before do
      ruby = create(:skill, name:'Ruby')
      js = create(:skill, name:'Javascript')
      @skill_list = [
        {id:ruby.id, rank:7},
        {id:js.id, rank:8}
      ]
    end

    let(:name) { Faker::Name.name }
    let(:time_zone) { "EST" }
    let(:skills) { @skill_list }

    let(:raw_post) { params.to_json }

    example_request "Creating a talent_profile with a skill list" do
      json_response = JSON.parse(response_body)
      expect(status).to eq(201)
      expect(json_response['data']['attributes']['skill_list']).to include('Ruby')
      expect(json_response['data']['attributes']['skill_list']).to include('Javascript')
    end
  end

  post "/talent_profiles" do
    parameter :name, "Name", scope: :talent_profile
    parameter :time_zone, "Time zone", scope: :talent_profile
    parameter :cities, "City Array"

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo, "Photo"
    response_field :resume, "Resume"
    response_field :time_zone, "Time zone"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :area_of_interest, "Area of Interest"
    response_field :years_of_experience, "Years of Experience"
    response_field :usdod_clearance, "US DoD Clearance"
    response_field :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities"
    response_field :job_search_status, "Job Search Status"
    response_field :preferred_base_salary, "Preferred Base Salary"
    response_field :usa_work_auth_type, "USA Work Auth Type"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :personal_website, "Personal Website"
    response_field :dribbble_url, "Dribbble URL"
    response_field :stack_overflow_url, "StackOverflow URL"
    response_field :blog_url, "Blog URL"

    let(:talent_profile) { create(:talent_profile, user: user) }

    before do
      atlanta = create(:city, name:'Atlanta')
      austin = create(:city, name:'Austin')
      @city_list = [
        {id:atlanta.id},
        {id:austin.id}
      ]
    end

    let(:name) { Faker::Name.name }
    let(:time_zone) { "EST" }
    let(:cities) { @city_list }

    let(:raw_post) { params.to_json }

    example_request "Creating a talent_profile with a city list" do
      json_response = JSON.parse(response_body)
      expect(status).to eq(201)
      expect(json_response['data']['attributes']['city_list']).to include('Atlanta')
      expect(json_response['data']['attributes']['city_list']).to include('Austin')
    end
  end

  post "/talent_profiles" do
    parameter :name, "Name", scope: :talent_profile
    parameter :time_zone, "Time zone", scope: :talent_profile
    parameter :cultures, "Culture Array"

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo, "Photo"
    response_field :resume, "Resume"
    response_field :time_zone, "Time zone"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :area_of_interest, "Area of Interest"
    response_field :years_of_experience, "Years of Experience"
    response_field :usdod_clearance, "US DoD Clearance"
    response_field :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities"
    response_field :job_search_status, "Job Search Status"
    response_field :preferred_base_salary, "Preferred Base Salary"
    response_field :usa_work_auth_type, "USA Work Auth Type"
    response_field :linked_in_url, "LinkedIn URL"
    response_field :personal_website, "Personal Website"
    response_field :dribbble_url, "Dribbble URL"
    response_field :stack_overflow_url, "StackOverflow URL"
    response_field :blog_url, "Blog URL"

    let(:talent_profile) { create(:talent_profile, user: user) }

    before do
      remote = create(:culture, name:'Remote')
      startup = create(:culture, name:'Start up')
      @culture_list = [
        {id:remote.id},
        {id:startup.id}
      ]
    end

    let(:name) { Faker::Name.name }
    let(:time_zone) { "EST" }
    let(:cultures) { @culture_list }

    let(:raw_post) { params.to_json }

    example_request "Creating a talent_profile with a culture list" do
      json_response = JSON.parse(response_body)
      expect(status).to eq(201)
      expect(json_response['data']['attributes']['culture_list']).to include('Remote')
      expect(json_response['data']['attributes']['culture_list']).to include('Start up')
    end
  end

  put "/talent_profiles/:id" do
    parameter :name, "Name", scope: :talent_profile
    parameter :photo_id, "Photo", scope: :talent_profile
    parameter :resume_id, "Resume", scope: :talent_profile
    parameter :time_zone, "Time zone", scope: :talent_profile
    parameter :phone, "Phone", scope: :talent_profile
    parameter :summary, "Summary", scope: :talent_profile
    parameter :area_of_interest, "Area of Interest", scope: :talent_profile
    parameter :years_of_experience, "Years of Experience", scope: :talent_profile
    parameter :usdod_clearance, "US DoD Clearance", scope: :talent_profile
    parameter :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities", scope: :talent_profile
    parameter :job_search_status, "Job Search Status", scope: :talent_profile
    parameter :preferred_base_salary, "Preferred Base Salary", scope: :talent_profile
    parameter :usa_work_auth_type, "USA Work Auth Type", scope: :talent_profile
    parameter :linked_in_url, "LinkedIn URL", scope: :talent_profile
    parameter :personal_website, "Personal Website", scope: :talent_profile
    parameter :dribbble_url, "Dribbble URL", scope: :talent_profile
    parameter :stack_overflow_url, "StackOverflow URL", scope: :talent_profile
    parameter :blog_url, "Blog URL", scope: :talent_profile

    let(:talent_profile) { create(:talent_profile, user: user) }

    let(:id) { talent_profile.id }
    let(:name) { Faker::Name.name }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:summary) { "Updated summary" }
    let(:area_of_interest) { "Updated area of interest" }

    let(:raw_post) { params.to_json }

    example_request "Updating a talent profile" do
      expect(status).to eq(204)
    end
  end

  put "/talent_profiles/:id" do
    parameter :name, "Name", scope: :talent_profile
    parameter :photo_id, "Photo", scope: :talent_profile
    parameter :resume_id, "Resume", scope: :talent_profile
    parameter :time_zone, "Time zone", scope: :talent_profile
    parameter :phone, "Phone", scope: :talent_profile
    parameter :summary, "Summary", scope: :talent_profile
    parameter :area_of_interest, "Area of Interest", scope: :talent_profile
    parameter :years_of_experience, "Years of Experience", scope: :talent_profile
    parameter :usdod_clearance, "US DoD Clearance", scope: :talent_profile
    parameter :reason_for_interest_in_new_job_opportunities, "Reason for Interest in new job Opportunities", scope: :talent_profile
    parameter :job_search_status, "Job Search Status", scope: :talent_profile
    parameter :preferred_base_salary, "Preferred Base Salary", scope: :talent_profile
    parameter :usa_work_auth_type, "USA Work Auth Type", scope: :talent_profile
    parameter :linked_in_url, "LinkedIn URL", scope: :talent_profile
    parameter :personal_website, "Personal Website", scope: :talent_profile
    parameter :dribbble_url, "Dribbble URL", scope: :talent_profile
    parameter :stack_overflow_url, "StackOverflow URL", scope: :talent_profile
    parameter :blog_url, "Blog URL", scope: :talent_profile

    let(:talent_profile) { create(:talent_profile) }

    let(:id) { talent_profile.id }
    let(:name) { Faker::Name.name }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:summary) { "Updated summary" }
    let(:area_of_interest) { "Updated area of interest" }

    let(:raw_post) { params.to_json }

    example_request "User gets error when updating a talent profile from another user" do
      expect(status).to eq(403)
    end
  end

  delete "/talent_profiles/:id" do
    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:id) { talent_profile.id }

    example_request "Deleting a talent profile by Id" do
      expect(status).to eq(204)
    end
  end

  delete "/talent_profiles/:id" do
    let(:talent_profile) { create(:talent_profile) }
    let(:id) { talent_profile.id }

    example_request "User gets an error when deleting a talent profile by Id from another user" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:id/get_recent_conversations" do
    parameter :id, "Talent Profile Id"
    parameter :count, "Number of conversations"

    before do
      user.add_role :talent

      @talent_profile = create(:talent_profile)
      employer_contact1 = create(:employer_contact, :with_positions, number_of_positions: 3)
      employer_contact2 = create(:employer_contact, :with_positions, number_of_positions: 3)
      employer_contact3 = create(:employer_contact, :with_positions, number_of_positions: 3)
      
      create(:message, to_user: employer_contact1.user, 
              from_user: @talent_profile.user, 
              position: employer_contact1.positions.first)
      create(:message, to_user: employer_contact2.user, 
              from_user: @talent_profile.user, 
              position: employer_contact2.positions.first)
      create(:message, to_user: employer_contact3.user, 
              from_user: @talent_profile.user, 
              position: employer_contact3.positions.first)
      create(:message, to_user: employer_contact3.user, 
              from_user: @talent_profile.user, 
              position: employer_contact3.positions.last)
    end

    let(:id) { @talent_profile.id }
    let(:count) { 3 }

    let(:raw_post) { params.to_json }

    example_request "User with talent role can get the most recent conversations" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response.count).to eq 3
    end
  end
end
