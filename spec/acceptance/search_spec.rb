require 'acceptance_helper'

resource "Search", elasticsearch: true do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
    TalentProfile.__elasticsearch__.create_index! index: TalentProfile.index_name
    @talent_profile = create(:talent_profile, name: "Adam")
    @position = create(:position, :with_bonus_skill_list, :with_required_skill_list)
    TalentProfile.import

    # Stupid I know but allows the ES test cluster to index the object we create
    sleep 1
  end

  after do
    TalentProfile.__elasticsearch__.client.indices.delete index: TalentProfile.index_name
  end

  get "/search/talent_profile", elasticsearch: true do
    parameter :q, "Search Term"

    let(:q) { "Adam" }
    let(:raw_post) { params.to_json }

    example_request "Searching for a Talent Profile" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 1
      expect(json_response['data'].first['id'].to_i).to eq @talent_profile.id
    end
  end

  get "/search/positions/:id/talent_profiles" do
    parameter :id, "Id of Position"

    let(:id) { @position.id}
    let(:raw_post) { params.to_json }

    example_request "Searching for a Talent Profile by a Position" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
    end
  end
end
