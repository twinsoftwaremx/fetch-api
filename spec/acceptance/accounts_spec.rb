require 'acceptance_helper'

resource "Accounts" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/accounts" do
    response_field :id, "Id"
    response_field :company_name, "Company Name"
    response_field :company_size_id, "Company Size Id"
    response_field :address_id, "Address Id"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :benefits, "Benefits"
    response_field :perks, "Perks"
    response_field :culture, "Culture"
    response_field :github_url, "Github URL"
    response_field :company_site_url, "Company Site URL"
    response_field :year_founded, "Year founded"
    response_field :revenue, "Revenue"
    response_field :who_we_are, "Who We Are"
    response_field :what_we_do, "What We Do"
    response_field :video_url, "Video URL"

    before do
      create_list(:account, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of accounts" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 3
    end
  end

  get '/accounts/:id' do
    response_field :id, "Id"
    response_field :company_name, "Company Name"
    response_field :company_size_id, "Company Size Id"
    response_field :address_id, "Address Id"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :benefits, "Benefits"
    response_field :perks, "Perks"
    response_field :culture, "Culture"
    response_field :github_url, "Github URL"
    response_field :company_site_url, "Company Site URL"
    response_field :year_founded, "Year founded"
    response_field :revenue, "Revenue"
    response_field :who_we_are, "Who We Are"
    response_field :what_we_do, "What We Do"
    response_field :video_url, "Video URL"

    let(:account) { create(:account) }
    before do
      3.times do
        account.employer_contacts << create(:employer_contact, user: user)
      end
    end
    let(:id) { account.id }

    let(:raw_post) { params.to_json }

    example_request "Getting an account" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        AccountSerializer.new(account)).to_json
    end
  end

  post '/accounts' do
    parameter :company_name, "Company Name", scope: :account
    parameter :company_size_id, "Company Size Id", scope: :account
    parameter :address_id, "Address Id", scope: :account
    parameter :phone, "Phone", scope: :account
    parameter :benefits, "Benefits", scope: :account
    parameter :revenue, "Revenue", scope: :account

    response_field :id, "Id"
    response_field :company_name, "Company Name"
    response_field :company_size_id, "Company Size Id"
    response_field :address_id, "Address Id"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :benefits, "Benefits"
    response_field :perks, "Perks"
    response_field :culture, "Culture"
    response_field :github_url, "Github URL"
    response_field :company_site_url, "Company Site URL"
    response_field :year_founded, "Year founded"
    response_field :revenue, "Revenue"
    response_field :who_we_are, "Who We Are"
    response_field :what_we_do, "What We Do"
    response_field :video_url, "Video URL"

    before do
      user.add_role :account_admin
    end

    let(:company_name) { Faker::Company.name }
    let(:size) { Faker::Number.number(5) }
    let(:address_id) { FactoryGirl.create(:full_address).id }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:benefits) { "Updated benefits" }
    let(:revenue) { "New revenue" }

    let(:raw_post) { params.to_json }

    example_request "Creating an account" do
      puts raw_post
      expect(status).to eq(201)
    end
  end

  post '/accounts' do
    parameter :company_name, "Company Name", scope: :account
    parameter :company_size_id, "Company Size Id", scope: :account
    parameter :address_id, "Address Id", scope: :account
    parameter :phone, "Phone", scope: :account
    parameter :benefits, "Benefits", scope: :account

    response_field :id, "Id"
    response_field :company_name, "Company Name"
    response_field :company_size_id, "Company Size Id"
    response_field :address_id, "Address Id"
    response_field :phone, "Phone"
    response_field :summary, "Summary"
    response_field :benefits, "Benefits"
    response_field :perks, "Perks"
    response_field :culture, "Culture"
    response_field :github_url, "Github URL"
    response_field :company_site_url, "Company Site URL"
    response_field :year_founded, "Year founded"
    response_field :revenue, "Revenue"
    response_field :who_we_are, "Who We Are"
    response_field :what_we_do, "What We Do"
    response_field :video_url, "Video URL"

    let(:company_name) { Faker::Company.name }
    let(:size) { Faker::Number.number(5) }
    let(:address_id) { FactoryGirl.create(:full_address).id }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:benefits) { "Updated benefits" }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when creating an account and he does not have the account_admin role" do
      expect(status).to eq(403)
    end
  end

  put '/accounts/:id' do
    parameter :company_name, "Company Name", scope: :account
    parameter :company_size_id, "Company Size Id", scope: :account
    parameter :address_id, "Address Id", scope: :account
    parameter :phone, "Phone", scope: :account

    let(:account) { create(:account) }
    before do
      user.add_role :account_admin

      account.employer_contacts << create(:employer_contact, user: user)
    end

    let(:id) { account.id }
    let(:company_name) { "Updated company name" }
    let(:phone) { "(999) 999-9999" }

    let(:raw_post) { params.to_json }

    example_request "Updating an account" do
      expect(status).to eq(204)
    end
  end

  put '/accounts/:id' do
    parameter :company_name, "Company Name", scope: :account
    parameter :company_size_id, "Company Size Id", scope: :account
    parameter :address_id, "Address Id", scope: :account
    parameter :phone, "Phone", scope: :account

    let(:account) { create(:account) }
    before do
      account.employer_contacts << create(:employer_contact, user: user)
    end

    let(:id) { account.id }
    let(:company_name) { "Updated company name" }
    let(:phone) { "(999) 999-9999" }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when updating an account and does not have account_admin role" do
      expect(status).to eq(403)
    end
  end

  put '/accounts/:id' do
    parameter :company_name, "Company Name", scope: :account
    parameter :company_size_id, "Company Size Id", scope: :account
    parameter :address_id, "Address Id", scope: :account
    parameter :phone, "Phone", scope: :account

    let(:account) { create(:account) }
    before do
      user.add_role :account_admin

      another_user = create(:user)

      account.employer_contacts << create(:employer_contact, user: another_user)
    end

    let(:id) { account.id }
    let(:company_name) { "Updated company name" }
    let(:phone) { "(999) 999-9999" }

    let(:raw_post) { params.to_json }

    example_request "User with role account_admin gets an error when updating an account he does not belong to" do
      expect(status).to eq(403)
    end
  end

  delete "/accounts/:id" do
    parameter :id, "Account Id"

    let(:account) { create(:account) }
    before do
      user.add_role :account_admin

      account.employer_contacts << create(:employer_contact, user: user)
    end

    let(:id) { account.id }

    example_request "Deleting an account" do
      expect(status).to eq(204)
    end
  end

  delete "/accounts/:id" do
    parameter :id, "Account Id"

    let(:account) { create(:account) }
    before do
      account.employer_contacts << create(:employer_contact, user: user)
    end

    let(:id) { account.id }

    example_request "User gets an error when deleting an account and does not have account_admin role" do
      expect(status).to eq(403)
    end
  end

  delete "/accounts/:id" do
    parameter :id, "Account Id"

    let(:account) { create(:account) }
    before do
      user.add_role :account_admin

      another_user = create(:user)

      account.employer_contacts << create(:employer_contact, user: another_user)
    end

    let(:id) { account.id }

    example_request "User gets an error when deleting an account he does not belong to" do
      expect(status).to eq(403)
    end
  end
end
