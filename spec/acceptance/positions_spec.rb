require 'acceptance_helper'

resource "Positions" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/positions" do
    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    before do
      create_list(:position, 5)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 5
    end
  end

  get "/positions/:id" do
    parameter :id, "Position Id"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:position) { create(:position) }
    let(:id) { position.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a position by Id" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        PositionSerializer.new(position)).to_json
    end
  end

  get "/employer_contacts/:employer_contact_id/positions" do
    parameter :employer_contact_id, "Employer Contact Id"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 3, user: user) }
    let(:employer_contact_id) { employer_contact.id }
    let(:salary_range_id) { create(:salary_range).id}

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions from the employer contact" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 3
    end
  end

  get "/employer_contacts/:employer_contact_id/positions" do
    parameter :employer_contact_id, "Employer Contact Id"
    parameter :filter_skill, "Skill used to filter the positions"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:position1) { create(:position, required_skill_list: 'ruby,javascript,css') }
    let(:position2) { create(:position, required_skill_list: 'python,ruby') }
    let(:position3) { create(:position) }
    let(:employer_contact) { create(:employer_contact, user: user, positions: [position1, position2, position3]) }
    let(:employer_contact_id) { employer_contact.id }

    let(:filter_skill) { employer_contact.positions.first.required_skill_associations.first.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions from the employer contact using skill filter" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 2
    end
  end

  get "/employer_contacts/:employer_contact_id/positions" do
    parameter :employer_contact_id, "Employer Contact Id"
    parameter :filter_compensation, "Compensation used to filter the positions"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:compensation1) { create(:salary_range) }
    let(:position1) { create(:position, salary_range: compensation1) }
    let(:position2) { create(:position) }
    let(:position3) { create(:position) }
    let(:employer_contact) { create(:employer_contact, user: user, positions: [position1, position2, position3]) }
    let(:employer_contact_id) { employer_contact.id }

    let(:filter_compensation) { employer_contact.positions.first.salary_range_id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions from the employer contact using compensation filter" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 1
    end
  end

  get "/employer_contacts/:employer_contact_id/positions" do
    parameter :employer_contact_id, "Employer Contact Id"
    parameter :filter_location, "Location used to filter the positions"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:location1) { create(:city) }
    let(:position1) { create(:position, city: location1) }
    let(:position2) { create(:position) }
    let(:position3) { create(:position) }
    let(:employer_contact) { create(:employer_contact, user: user, positions: [position1, position2, position3]) }
    let(:employer_contact_id) { employer_contact.id }

    let(:filter_location) { employer_contact.positions.first.city_id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions from the employer contact using location filter" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 1
    end
  end

  get "/employer_contacts/:employer_contact_id/positions" do
    parameter :employer_contact_id, "Employer Contact Id"
    parameter :filter_skill, "Skill used to filter the positions"
    parameter :filter_location, "Location used to filter the positions"
    parameter :filter_compensation, "Compensation used to filter the positions"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:city1) { create(:city, name: 'Birmingham') }
    let(:compensation1) { create(:salary_range, name: '$75 - $100K') }
    let(:position1) { create(:position, required_skill_list: 'ruby,javascript,css', city: city1, salary_range: compensation1) }
    let(:position2) { create(:position) }
    let(:position3) { create(:position) }
    let(:employer_contact) { create(:employer_contact, user: user, positions: [position1, position2, position3]) }
    let(:employer_contact_id) { employer_contact.id }

    let(:filter_skill) { employer_contact.positions.first.required_skill_associations.first.id }
    let(:filter_compensation) { employer_contact.positions.first.salary_range_id }
    let(:filter_location) { employer_contact.positions.first.city_id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions from the employer contact using filters" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 1
    end
  end
  get "/employer_contacts/:employer_contact_id/positions" do
    parameter :employer_contact_id, "Employer Contact Id"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 3) }
    let(:employer_contact_id) { employer_contact.id }
    let(:salary_range_id) { create(:salary_range).id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of positions from another user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 3
    end
  end

  get "/employer_contacts/:employer_contact_id/positions/:id" do
    parameter :employer_contact_id, "Employer Contact Id"

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 1, user: user) }
    let(:employer_contact_id) { employer_contact.id }
    let(:salary_range_id) { create(:salary_range).id }

    let(:position) { employer_contact.positions.first }

    parameter :id, "Id"

    let(:id) { position.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a position by Id" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        PositionSerializer.new(position)).to_json
    end
  end

  post '/employer_contacts/:employer_contact_id/positions' do
    parameter :job_title, "Job Title", scope: :position
    parameter :description, "Description", scope: :position
    parameter :hours, "Hours", scope: :position
    parameter :salary_range_id, "Salary Range Id", scope: :position
    parameter :desired_years_experience, "Desired Years of Experience", scope: :position
    parameter :team_size, "Team Size", scope: :position
    parameter :employment_type_id, "Employment Type Id", scope: :position
    parameter :employer_contact_id, "Employer Contact Id", scope: :position

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:employment_type) { create(:employment_type) }
    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:salary_range_id) { create(:salary_range).id }

    let(:job_title) { Faker::Name.title }
    let(:description) { 'Job description' }
    let(:hours) { 40 }
    let(:desired_years_experience) { 5 }
    let(:team_size) { 'size' }
    let(:employment_type_id) { employment_type.id }
    let(:employer_contact_id) { employer_contact.id }

    let(:raw_post) { params.to_json }

    example_request "Creating a position" do
      expect(status).to eq(201)
    end
  end

  post '/employer_contacts/:employer_contact_id/positions' do
    parameter :job_title, "Job Title", scope: :position
    parameter :description, "Description", scope: :position
    parameter :hours, "Hours", scope: :position
    parameter :salary_range_id, "Salary Range Id", scope: :position
    parameter :desired_years_experience, "Desired Years of Experience", scope: :position
    parameter :team_size, "Team Size", scope: :position
    parameter :employment_type_id, "Employment Type Id", scope: :position
    parameter :employer_contact_id, "Employer Contact Id", scope: :position
    parameter :required_skill_list, "Required Skills list", scope: :position

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    before do
      ruby = create(:skill, name:'Ruby')
      js = create(:skill, name:'Javascript')
      @skill_list = [ruby.name, js.name].join(',')
    end

    let(:employment_type) { create(:employment_type) }
    let(:employer_contact) { create(:employer_contact, user: user) }
    let(:salary_range_id) { create(:salary_range).id }

    let(:job_title) { Faker::Name.title }
    let(:description) { 'Job description' }
    let(:hours) { 40 }
    let(:desired_years_experience) { 5 }
    let(:team_size) { 'size' }
    let(:employment_type_id) { employment_type.id }
    let(:employer_contact_id) { employer_contact.id }

    let(:required_skill_list) { @skill_list }

    let(:raw_post) { params.to_json }

    example_request "Creating a position with a list of required skills" do
      expect(status).to eq(201)
      json_response = JSON.parse(response_body)
      expect(json_response['data']['attributes']['required_skill_list']).to include('Ruby')
      expect(json_response['data']['attributes']['required_skill_list']).to include('Javascript')
    end
  end

  post '/employer_contacts/:employer_contact_id/positions' do
    parameter :job_title, "Job Title", scope: :position
    parameter :description, "Description", scope: :position
    parameter :hours, "Hours", scope: :position
    parameter :salary_range, "Salary Range", scope: :position
    parameter :desired_years_experience, "Desired Years of Experience", scope: :position
    parameter :team_size, "Team Size", scope: :position
    parameter :employment_type_id, "Employment Type Id", scope: :position
    parameter :employer_contact_id, "Employer Contact Id", scope: :position

    response_field :id, "Id"
    response_field :job_title, "Job Title"
    response_field :description, "Description"
    response_field :hours, "Hours"
    response_field :salary_range, "Salary Range"
    response_field :desired_years_experience, "Desired Years of Experience"
    response_field :team_size, "Team Size"
    response_field :employment_type, "Employment Type"
    response_field :employer_contact, "Employer Contact"

    let(:employment_type) { create(:employment_type) }
    let(:employer_contact) { create(:employer_contact) }

    let(:job_title) { Faker::Name.title }
    let(:description) { 'Job description' }
    let(:hours) { 40 }
    let(:salary_range_id) { create(:salary_range).id }
    let(:desired_years_experience) { 5 }
    let(:team_size) { 'size' }
    let(:employment_type_id) { employment_type.id }
    let(:employer_contact_id) { employer_contact.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when creating a position for another user" do
      expect(status).to eq(403)
    end
  end

  put "/employer_contacts/:employer_contact_id/positions/:id" do
    parameter :job_title, "Job Title", scope: :position
    parameter :description, "Description", scope: :position
    parameter :hours, "Hours", scope: :position
    parameter :salary_range, "Salary Range", scope: :position
    parameter :desired_years_experience, "Desired Years of Experience", scope: :position
    parameter :team_size, "Team Size", scope: :position
    parameter :employment_type_id, "Employment Type Id", scope: :position
    parameter :employer_contact_id, "Employer Contact Id", scope: :position

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 1, user: user) }
    let(:employer_contact_id) { employer_contact.id }

    let(:position) { employer_contact.positions.first }

    let(:id) { position.id }
    let(:job_title) { "New job title" }
    let(:description) { "New description" }
    let(:salary_range_id) { create(:salary_range).id }

    let(:raw_post) { params.to_json }

    example_request "Updating a position" do
      expect(status).to eq(204)
    end
  end

  put "/employer_contacts/:employer_contact_id/positions/:id" do
    parameter :job_title, "Job Title", scope: :position
    parameter :description, "Description", scope: :position
    parameter :hours, "Hours", scope: :position
    parameter :salary_range, "Salary Range", scope: :position
    parameter :desired_years_experience, "Desired Years of Experience", scope: :position
    parameter :team_size, "Team Size", scope: :position
    parameter :employment_type_id, "Employment Type Id", scope: :position
    parameter :employer_contact_id, "Employer Contact Id", scope: :position

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 1) }
    let(:employer_contact_id) { employer_contact.id }

    let(:position) { employer_contact.positions.first }

    let(:id) { position.id }
    let(:job_title) { "New job title" }
    let(:description) { "New description" }
    let(:salary_range_id) { create(:salary_range).id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when updating a position from another user" do
      expect(status).to eq(403)
    end
  end

  delete "/employer_contacts/:employer_contact_id/positions/:id" do
    parameter :employer_contact_id, "Employer Contact Id"
    parameter :id, "Position Id"

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 1, user: user) }
    let(:employer_contact_id) { employer_contact.id }

    let(:position) { employer_contact.positions.first }
    let(:id) { position.id }

    example_request "Deleting a position" do
      expect(status).to eq(204)
    end
  end

  delete "/employer_contacts/:employer_contact_id/positions/:id" do
    parameter :employer_contact_id, "Employer Contact Id"
    parameter :id, "Position Id"

    let(:employer_contact) { create(:employer_contact, :with_positions, number_of_positions: 1) }
    let(:employer_contact_id) { employer_contact.id }

    let(:position) { employer_contact.positions.first }
    let(:id) { position.id }

    example_request "User gets an error when deleting a position from another user" do
      expect(status).to eq(403)
    end
  end
end
