require 'acceptance_helper'

resource "Salary Ranges" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/salary_ranges" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      header "Authorization", ""
      create_list(:salary_range, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of salary ranges by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/salary_ranges/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :name, "Name"

    let(:salary_range) { create(:salary_range) }

    let(:id) { salary_range.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a salary range by Id" do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        SalaryRangeSerializer.new(salary_range)).to_json
    end
  end

  post "/salary_ranges" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :salary_range

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Salary range" }

    let(:raw_post) { params.to_json }

    example_request "Creating a salary range by admin user" do
      expect(status).to eq 201
    end
  end

  put "/salary_ranges/:id" do
    before do
      user.add_role :admin
    end

    parameter :id, "Id"
    parameter :name, "Name", scope: :salary_range

    response_field :id, "Id"
    response_field :name, "Name"

    let(:salary_range) { create(:salary_range) }

    let(:id) { salary_range.id }
    let(:name) { "New salary range" }

    let(:raw_post) { params.to_json }

    example_request "Updating a salary range by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/salary_ranges/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:salary_range) { create(:salary_range) }
    let(:id) { salary_range.id }

    example_request "Deleting a salary range by admin user" do
      expect(status).to eq 204
    end  
  end
end