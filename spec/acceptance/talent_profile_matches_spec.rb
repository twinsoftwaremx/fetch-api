require 'acceptance_helper'

resource "Talent Profile Matches" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }
  let(:talent_profile) { create(:talent_profile, user: user) }
  let(:positions) { create_list(:position, 5) }

  before do
    header "Authorization", encode_token(user.auth_token)
    talent_profile.matches << positions
    @talent_profile_match = talent_profile.talent_profile_matches.first
  end

  get "/talent_profiles/:talent_profile_id/matches" do
    let(:talent_profile_id) { talent_profile.id }
    let(:raw_post) { params.to_json }

    example_request "Getting a list of matches for a talent_profile_id" do
      expect(status).to eq(200)
      json = JSON.parse(response_body)
      expect(json['data'].length).to eq(5)
    end
  end

  post "/talent_profiles/:talent_profile_id/matches/:id/like" do
    let(:talent_profile_id) { talent_profile.id }
    let(:id) { @talent_profile_match.id }
    let(:raw_post) { params.to_json }

    example_request "Like a TalentProfileMatch" do
      expect(status).to eq(201)
      json = JSON.parse(response_body)
      expect(json['data']['attributes']['status']).to eq('liked')
    end
  end

  post "/talent_profiles/:talent_profile_id/matches/:id/reject" do
    let(:talent_profile_id) { talent_profile.id }
    let(:id) { @talent_profile_match.id }
    let(:raw_post) { params.to_json }

    example_request "Reject a TalentProfileMatch" do
      expect(status).to eq(201)
      json = JSON.parse(response_body)
      expect(json['data']['attributes']['status']).to eq('rejected')
    end
  end
end
