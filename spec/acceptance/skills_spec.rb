require 'acceptance_helper'

resource "Skills" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/skills" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      header "Authorization", ""
      create_list(:skill, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of skills by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/skills" do
    parameter :category_id, "Category Id"

    response_field :id, "Id"
    response_field :name, "Name"

    let(:software_development) { create(:skill_category, name: 'Software Development') }
    let(:project_manager) { create(:skill_category, name: 'Project Manager') }

    before do
      header "Authorization", ""

      create_list(:skill, 4, skill_category: software_development)
      create_list(:skill, 2, skill_category: project_manager)
    end

    let(:category_id) { software_development.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of skills of a particular category by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 4
    end
  end

  get "/skills" do
    parameter :categories_list, "List of category Ids"

    response_field :id, "Id"
    response_field :name, "Name"

    let(:software_development) { create(:skill_category, name: 'Software Development') }
    let(:project_manager) { create(:skill_category, name: 'Project Manager') }
    let(:creative_professional) { create(:skill_category, name: 'Creative Professional') }

    before do
      header "Authorization", ""

      create_list(:skill, 4, skill_category: software_development)
      create_list(:skill, 2, skill_category: project_manager)
      create_list(:skill, 3, skill_category: creative_professional)
    end

    let(:categories_list) { software_development.id.to_s + "," + creative_professional.id.to_s }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of skills of a list of categories by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 2
      first_list = json_response[:data][0]
      second_list = json_response[:data][1]
      expect(first_list.count).to eq 4
      expect(second_list.count).to eq 3
    end
  end

  get "/skills/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :name, "Name"

    let(:skill) { create(:skill) }

    let(:id) { skill.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a skill by Id" do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        SkillSerializer.new(skill)).to_json
    end
  end

  post "/skills" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :skill

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Skill name" }

    let(:raw_post) { params.to_json }

    example_request "Creating a skill by admin user" do
      expect(status).to eq 201
    end
  end

  put "/skills/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    parameter :name, "Name", scope: :skill

    response_field :id, "Id"
    response_field :name, "Name"

    let(:skill) { create(:skill) }
    let(:id) { skill.id }

    let(:name) { "New skill name" }

    let(:raw_post) { params.to_json }

    example_request "Updating a skill by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/skills/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:skill) { create(:skill) }
    let(:id) { skill.id }

    example_request "Deleting a skill by admin user" do
      expect(status).to eq 204
    end
  end
end
