require 'acceptance_helper'

resource "LocationOfInterestAssociations" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/talent_profiles/:talent_profile_id/interested_locations" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :location, "Interested location"

    let(:talent_profile) { create(:talent_profile, :with_locations_of_interest, number_of_locations_of_interest: 3, user: user) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a list of locations of interest for the talent profile" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/talent_profiles/:talent_profile_id/interested_locations" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :location, "Interested location"

    let(:talent_profile) { create(:talent_profile, :with_locations_of_interest, number_of_locations_of_interest: 3) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a list of locations of interest from another user" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:talent_profile_id/interested_locations" do
    parameter :talent_profile_id, "Talent Profile Id"

    response_field :id, "Id"
    response_field :location, "Interested location"

    let(:talent_profile) { create(:talent_profile, :with_locations_of_interest, number_of_locations_of_interest: 3) }

    let(:talent_profile_id) { talent_profile.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a list of locations of interest from another user" do
      expect(status).to eq(403)
    end
  end

  get "/talent_profiles/:talent_profile_id/interested_locations/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :location, "Interested location"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:location_of_interest_association) { create(:location_of_interest_association, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { location_of_interest_association.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a location of interest by Id for the talent profile" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        LocationOfInterestAssociationSerializer.new(location_of_interest_association)).to_json
    end
  end

  get "/talent_profiles/:talent_profile_id/interested_locations/:id" do
    parameter :talent_profile_id, "Talent Profile Id"
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :location, "Interested location"

    let(:talent_profile) { create(:talent_profile) }
    let(:location_of_interest_association) { create(:location_of_interest_association, talent_profile: talent_profile) }

    let(:talent_profile_id) { talent_profile.id }
    let(:id) { location_of_interest_association.id }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when getting a location of interest by Id from another user" do
      expect(status).to eq(403)
    end
  end

  post "/talent_profiles/:talent_profile_id/interested_locations" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id"
    parameter :location_id, "Interested Location Id", scope: :location_of_interest_association

    response_field :id, "Id"
    response_field :interested_location, "Interest Location"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:location) { create(:location) }

    let(:location_id) { location.id }


    let(:raw_post) { params.to_json }

    example_request "Adding a location of interest to the talent profile" do
      expect(status).to eq(201)
    end
  end

  post "/talent_profiles/:talent_profile_id/interested_locations" do
    parameter :talent_profile_id, "Talent Profile Id"

    parameter :id, "Id"
    parameter :location_id, "Interested Location Id", scope: :location_of_interest_association

    response_field :id, "Id"
    response_field :interested_location, "Interest Location"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:location) { create(:location) }

    let(:location_id) { location.id }


    let(:raw_post) { params.to_json }

    example_request "User gets an error when adding a location of interest to the talent profile of another user" do
      expect(status).to eq(403)
    end
  end

  delete "/talent_profiles/:talent_profile_id/interested_locations/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile, user: user) }
    let(:talent_profile_id) { talent_profile.id }

    let(:location_of_interest_association) { create(:location_of_interest_association, talent_profile: talent_profile) }
    let(:id) { location_of_interest_association.id }

    example_request "Removing a location of interest by Id from a talent profile" do
      expect(status).to eq(204)
    end
  end

  delete "/talent_profiles/:talent_profile_id/interested_locations/:id" do
    parameter :id, "Id"
    parameter :talent_profile_id, "Id"

    let(:talent_profile) { create(:talent_profile) }
    let(:talent_profile_id) { talent_profile.id }

    let(:location_of_interest_association) { create(:location_of_interest_association, talent_profile: talent_profile) }
    let(:id) { location_of_interest_association.id }

    example_request "User gets an error when removing a location of interest by Id from another user" do
      expect(status).to eq(403)
    end
  end
end
