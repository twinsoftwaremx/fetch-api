require 'acceptance_helper'

resource "Cultures" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/cultures" do
    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"

    before do
      header "Authorization", ""
      create_list(:culture, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of cultures by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/cultures/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"

    let(:culture) { create(:culture) }

    let(:id) { culture.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a culture by Id" do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        CultureSerializer.new(culture)).to_json
    end
  end

  post "/cultures" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :culture
    parameter :photo_id, "Photo ID", scope: :culture

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"

    let(:name) { "Culture name" }
    let(:photo_id) { create(:photo).id }

    let(:raw_post) { params.to_json }

    example_request "Creating a culture by admin user" do
      expect(status).to eq 201
    end
  end

  put "/cultures/:id" do
    before do
      user.add_role :admin
    end

    parameter :id, "Id"
    parameter :name, "Name", scope: :culture
    parameter :photo_id, "Photo ID", scope: :culture

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :photo_url, "Photo URL"

    let(:culture) { create(:culture) }

    let(:id) { culture.id }
    let(:name) { "New culture name" }

    let(:raw_post) { params.to_json }

    example_request "Updating a culture by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/cultures/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:culture) { create(:culture) }
    let(:id) { culture.id }

    example_request "Deleting a culture by admin user" do
      expect(status).to eq 204
    end
  end
end
