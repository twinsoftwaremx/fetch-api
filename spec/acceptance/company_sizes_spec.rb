require 'acceptance_helper'

resource "Company Sizes" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  let(:company_size) { create(:company_size) }

  get "/company_sizes" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      header "Authorization", ""
      create_list(:company_size, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of company sizes as non-authorized user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/company_sizes" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      user.add_role :account_admin
      create_list(:company_size, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of company sizes as account_admin user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/company_sizes" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      user.add_role :employer_contact
      create_list(:company_size, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of company sizes as employer_contact user" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/company_sizes/:id" do
    before do
      header "Authorization", ""
    end
    parameter :id, "Id", scope: :company_size

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { company_size.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a company size by Id when by a non-authorized user" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        CompanySizeSerializer.new(company_size)).to_json
    end
  end

  get "/company_sizes/:id" do
    before do
      user.add_role :account_admin
    end
    parameter :id, "Id", scope: :company_size

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { company_size.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a company size by Id when user has account_admin role" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        CompanySizeSerializer.new(company_size)).to_json
    end
  end

  get "/company_sizes/:id" do
    before do
      user.add_role :employer_contact
    end
    parameter :id, "Id", scope: :company_size

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { company_size.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a company size by Id when user has employer_contact role" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        CompanySizeSerializer.new(company_size)).to_json
    end
  end

  post "/company_sizes" do
    parameter :name, "Name", scope: :company_size

    response_field :id, "Id"
    response_field :name, "Name"

    before do
      user.add_role :admin
    end

    let(:name) { "Company size name" }

    let(:raw_post) { params.to_json }

    example_request "Creating a company size" do
      expect(status).to eq(201)
    end
  end

  post "/company_sizes" do
    parameter :name, "Name", scope: :company_size

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Company size name" }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when creating a company size and does not have the admin role" do
      expect(status).to eq(403)
    end
  end

  put "/company_sizes/:id" do
    parameter :id, "Id"

    parameter :name, "Name", scope: :company_size

    before do
      user.add_role :admin
    end

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { company_size.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "Updating a company size by Id" do
      expect(status).to eq(204)
    end  
  end

  put "/company_sizes/:id" do
    parameter :id, "Id"

    parameter :name, "Name", scope: :company_size

    response_field :id, "Id"
    response_field :name, "Name"

    let(:id) { company_size.id }
    let(:name) { "Updated name" }

    let(:raw_post) { params.to_json }

    example_request "User gets an error when updating a company size by Id and does not have the admin role" do
      expect(status).to eq(403)
    end  
  end

  delete "/company_sizes/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:id) { company_size.id }

    example_request "Deleting a company size by Id" do
      expect(status).to eq(204)
    end
  end

  delete "/company_sizes/:id" do
    parameter :id, "Id"

    let(:id) { company_size.id }

    example_request "User gets an error when deleting a company size by Id and does not have the admin role" do
      expect(status).to eq(403)
    end
  end

end