require 'acceptance_helper'

resource "Assets" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  let(:resume) {FactoryGirl.create(:resume) }
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get '/uploads/:id' do
    response_field :id, "Id"
    response_field :url, "Asset URL"

    let(:id) {resume.id}

    let(:raw_post) { params.to_json }

    describe "access allowed" do
      example_request "Getting a specific uploaded asset using auth token" do
        expect(response_body).to eq ActiveModel::Serializer.adapter.new(
          AssetSerializer.new(resume)).to_json
        expect(status).to eq(200)
      end
    end

    describe "access rejected with invalid token" do
      before do
        header "Authorization", encode_token('fake')
      end

      example_request "Getting a specific uploaded asset using invalid auth token" do
        expect(status).to eq(401)
      end
    end

    describe "access rejected with missing token" do
      before do
        header "Authorization", ""
      end

      example_request "Getting a specific uploaded asset without auth token" do
        expect(status).to eq(401)
      end
    end

  end

  post '/uploads' do
    parameter :file, "File to upload", scope: :asset
    parameter :type, "Type of Asset (Photo or Resume)", scope: :asset

    response_field :id, "Id"
    response_field :url, "Asset URL"

    let(:file) {Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec', 'support', 'test.docx')), 'application/msword')}
    let(:type) { "Resume" }
    example_request "Creating a asset" do
      explanation "First, create a asset, then make a later request to get it back"
      asset_hash = JSON.parse(response_body)
      expect(status).to eq(201)
    end


  end
end
