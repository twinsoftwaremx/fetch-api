require 'acceptance_helper'

resource "Messages" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/messages" do
    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:raw_post) { params.to_json }

    before do
      create_list(:message, 3, from_user: user)
    end

    example_request "Getting all the messages for the current user" do
      expect(status).to eq(200)
    end
  end

  get "/messages/:id" do
    parameter :id, "Message Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:message) { create(:message, from_user: user)}
    let(:id) { message.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a message by Id for the current user" do
      expect(status).to eq(200)
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        MessageSerializer.new(message)).to_json
    end
  end

  post "/messages" do
    parameter :body, "Body", scope: :message
    parameter :to_user_id, "Recipient Id", scope: :message
    parameter :position_id, "position Id associated to this message", scope: :message

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }

    let(:body) { "This is a test message" }
    let(:to_user_id) { another_user.id }
    let(:position_id) { position.id }

    let(:raw_post) { params.to_json }

    example_request "Sending a new message" do
      expect(status).to eq(201)
    end
  end

  post "/messages" do
    parameter :body, "Body", scope: :message
    parameter :position_id, "position Id associated to this message", scope: :message

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }

    let(:body) { "This is a test message" }
    let(:position_id) { position.id }

    let(:raw_post) { params.to_json }

    example_request "Sending a new message passing only the body and position_id" do
      expect(status).to eq(201)
    end
  end

  put "/messages/:id" do
    parameter :id, "Message Id"
    parameter :body, "Body", scope: :message
    parameter :to_user_id, "Recipient Id", scope: :message
    parameter :position_id, "position Id associated to this message", scope: :message

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:existing_message) { create(:message, from_user: user) }
    let(:id) { existing_message.id }

    let(:body) { "This is a test message" }

    let(:raw_post) { params.to_json }

    example_request "Updating an existing message from the current user" do
      expect(status).to eq(204)
    end
  end

  delete "/messages/:id" do
    parameter :id, "Message Id"

    let(:existing_message) { create(:message, from_user: user) }
    let(:id) { existing_message.id }

    let(:raw_post) { params.to_json }

    example_request "Deleting a message from the current user" do
      expect(status).to eq(204)
    end
  end

  get "/messages/for_position/:position_id" do
    parameter :position_id, "Position Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "Position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }

    before do
      create_list(:message, 2, from_user: another_user, to_user: user, position: position)
      create_list(:message, 3, from_user: user, to_user: another_user, position: position)
    end

    let(:position_id) { position.id }
    let(:to_user_id) { another_user.id }

    let(:raw_post) { params.to_json }

    example_request "Get messages from current user for a position" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 5
    end
  end

  get "/messages/for_position/:position_id/sent" do
    parameter :position_id, "Position Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }
    let(:another_position) { create(:position) }

    before do
      create_list(:message, 2, from_user: another_user, position: another_position)
      create_list(:message, 3, from_user: user, to_user: another_user, position: position)
    end

    let(:position_id) { position.id }
    let(:to_user_id) { another_user.id }

    let(:raw_post) { params.to_json }

    example_request "Get messages sent from current user for a position" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 3
    end
  end

  get "/messages/for_position/:position_id/to_user/:to_user_id/sent" do
    parameter :position_id, "Position Id"
    parameter :to_user_id, "Recipient User Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }
    let(:another_position) { create(:position) }

    before do
      create_list(:message, 2, from_user: another_user, position: another_position)
      create_list(:message, 3, from_user: user, to_user: another_user, position: position)
    end

    let(:position_id) { position.id }
    let(:to_user_id) { another_user.id }

    let(:raw_post) { params.to_json }

    example_request "Get messages sent from current user to another user for a position" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 3
    end
  end

  get "/messages/for_position/:position_id/received" do
    parameter :position_id, "Position Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:position) { create(:position) }

    before do
      user1 = create(:user)
      user2 = create(:user)

      create_list(:message, 2, to_user: user, from_user: user1, position: position)
      create_list(:message, 3, to_user: user, from_user: user2, position: position)
    end

    let(:position_id) { position.id }
    let(:from_user_id) { another_user.id }

    let(:raw_post) { params.to_json }

    example_request "Get messages received by the current user for a position" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 5
    end
  end

  get "/messages/for_position/:position_id/from_user/:from_user_id/received" do
    parameter :position_id, "Position Id"
    parameter :from_user_id, "Sender User Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }
    let(:another_position) { create(:position) }

    before do
      create_list(:message, 3, to_user: another_user, position: another_position)
      create_list(:message, 2, to_user: user, from_user: another_user, position: position)
    end

    let(:position_id) { position.id }
    let(:from_user_id) { another_user.id }

    let(:raw_post) { params.to_json }

    example_request "Get messages received by the current user from another user for a position" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 2
    end
  end

  get "/messages/for_position/:position_id/with_user/:with_user_id" do
    parameter :position_id, "Position Id"
    parameter :with_user_id, "Other User Id"

    response_field :id, "Message Id"
    response_field :body, "Body"
    response_field :to_user, "Recipient"
    response_field :from_user, "Sender"
    response_field :position, "position associated to this message"

    let(:another_user) { create(:user) }
    let(:position) { create(:position) }
    let(:another_position) { create(:position) }
    let(:with_user_id) { another_user.id }

    before do
      create_list(:message, 3, to_user: another_user, from_user: user, position: position)
      create_list(:message, 2, to_user: user, from_user: another_user, position: position)
    end

    let(:position_id) { position.id }
    let(:from_user_id) { another_user.id }

    let(:raw_post) { params.to_json }

    example_request "Get messages by the current user with another user for a position" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq 5
    end
  end
end