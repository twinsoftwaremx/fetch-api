require 'acceptance_helper'

resource "Cities" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/cities" do
    response_field :id, "Id"
    response_field :name, "Name"
    response_field :state, "State"

    before do
      header "Authorization", ""
      create_list(:city, 3)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of cities by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 3
    end
  end

  get "/cities/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :state, "State"

    let(:city) { create(:city) }

    let(:id) { city.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a city by Id" do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        CitySerializer.new(city)).to_json
    end
  end

  post "/cities" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :city
    parameter :state_id, "State Id", scope: :city

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :state, "State"

    let(:state) { create(:state) }

    let(:name) { "City name" }
    let(:state_id) { state.id }

    let(:raw_post) { params.to_json }

    example_request "Creating a city by admin user" do
      expect(status).to eq 201
    end
  end

  put "/cities/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"
    parameter :name, "Name", scope: :city
    parameter :state_id, "State Id", scope: :city

    response_field :id, "Id"
    response_field :name, "Name"
    response_field :state, "State"

    let(:city) { create(:city) }

    let(:id) { city.id }
    let(:name) { "New city name" }

    let(:raw_post) { params.to_json }

    example_request "Updating a city by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/cities/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:city) { create(:city) }
    let(:id) { city.id }

    example_request "Deleting a city by admin user" do
      expect(status).to eq 204
    end
  end
end