require 'acceptance_helper'

resource "Skill Categories" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user) }

  before do
    header "Authorization", encode_token(user.auth_token)
  end

  get "/skill_categories" do
    response_field :id, "Id"
    response_field :name, "Name"

    before do
      header "Authorization", ""
      create_list(:skill_category, 4)
    end

    let(:raw_post) { params.to_json }

    example_request "Getting a list of skill categories by a non-authorized user" do
      expect(status).to eq 200
      json_response = JSON.parse(response_body, symbolize_names: true)
      expect(json_response[:data].count).to eq 4
    end
  end

  get "/skill_categories/:id" do
    parameter :id, "Id"

    response_field :id, "Id"
    response_field :name, "Name"

    let(:skill_category) { create(:skill_category) }

    let(:id) { skill_category.id }

    let(:raw_post) { params.to_json }

    example_request "Getting a skill category by Id" do
      expect(status).to eq 200
      expect(response_body).to eq ActiveModel::Serializer.adapter.new(
        SkillCategorySerializer.new(skill_category)).to_json
    end
  end

  post "/skill_categories" do
    before do
      user.add_role :admin
    end
    parameter :name, "Name", scope: :skill_category

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Skill Category Name" }

    let(:raw_post) { params.to_json }

    example_request "Creating a skill category by admin user" do
      expect(status).to eq 201
    end
  end

  post "/skill_categories" do
    parameter :name, "Name", scope: :skill_category

    response_field :id, "Id"
    response_field :name, "Name"

    let(:name) { "Skill Category Name" }

    let(:raw_post) { params.to_json }

    example_request "Non-admin user gets an error when creating a skill category" do
      expect(status).to eq 403
    end
  end

  put "/skill_categories/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    parameter :name, "Name", scope: :skill_category

    response_field :id, "Id"
    response_field :name, "Name"

    let(:skill_category) { create(:skill_category) }
    let(:id) { skill_category.id }

    let(:name) { "Skill Category Name" }

    let(:raw_post) { params.to_json }

    example_request "Updating a skill category by admin user" do
      expect(status).to eq 204
    end
  end

  put "/skill_categories/:id" do
    parameter :id, "Id"

    parameter :name, "Name", scope: :skill_category

    response_field :id, "Id"
    response_field :name, "Name"

    let(:skill_category) { create(:skill_category) }
    let(:id) { skill_category.id }

    let(:name) { "Skill Category Name" }

    let(:raw_post) { params.to_json }

    example_request "Non-admin user gets an error when updating a skill category" do
      expect(status).to eq 403
    end
  end

  delete "/skill_categories/:id" do
    before do
      user.add_role :admin
    end
    parameter :id, "Id"

    let(:skill_category) { create(:skill_category) }
    let(:id) { skill_category.id }

    example_request "Deleting a skill category by admin user" do
      expect(status).to eq 204
    end
  end

  delete "/skill_categories/:id" do
    parameter :id, "Id"

    let(:skill_category) { create(:skill_category) }
    let(:id) { skill_category.id }

    example_request "Non-admin user gets an error when deleting a skill category" do
      expect(status).to eq 403
    end
  end
end