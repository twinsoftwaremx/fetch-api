require 'acceptance_helper'

resource "Offers" do
  header "Accept", "application/vnd.fetchapi.v1+json"
  header "Content-Type", "application/json"
  let(:user) { create(:user, role: 'talent') }
  let(:user2) { create(:user, role: 'employer_contact') }
  let(:employer_contact) { create(:employer_contact)}
  let(:talent_profile) { create(:talent_profile) }
  let(:positions) { create_list(:position, 5) }

  before do
    header "Authorization", encode_token(user.auth_token)
    user.talent_profile = talent_profile
    user2.employer_contact = employer_contact
    talent_profile.matches << positions
    @talent_profile_matches = talent_profile.talent_profile_matches
    @offer = create(:offer, talent_profile_match: @talent_profile_matches.first, employer_contact: employer_contact)
  end

  get "/offers" do
    let(:raw_post) { params.to_json }

    example_request "Getting a list of offers scoped by Talent" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response['data'].count).to eq(1)
    end
  end

  get "/offers/:id" do
    let(:id) { @offer.id }
    let(:raw_post) { params.to_json }

    example_request "Get offer by Id" do
      expect(status).to eq(200)
      json_response = JSON.parse(response_body)
      expect(json_response["data"]["id"]).to eq(@offer.id.to_s)
    end
  end

  post "/offers" do
    parameter :company_name, "Company Name", scope: :offer
    parameter :position_title, "Position Title", scope: :offer
    parameter :talent_profile_match_id, "Talent Profile Id", scope: :offer
    parameter :employer_contact_id, "Employer Contact Id", scope: :offer
    parameter :benefits, "Benefits", scope: :offer

    response_field :id, "Id"
    response_field :company_name, "Company Name"
    response_field :position_title, "Company Size Id"
    response_field :employer_contact_id, "Employer Contact Id"
    response_field :talent_profile_match_id, "Talent Profile Match Id"
    response_field :benefits, "Benefits"

    before do
      header "Authorization", encode_token(user2.auth_token)
      @offer2 = build(:offer, employer_contact_id: employer_contact.id)
    end

    let(:company_name) { @offer2.company_name }
    let(:position_title) { @offer2.position_title }
    let(:employer_contact_id) { @offer2.employer_contact_id }
    let(:talent_profile_match_id) { @talent_profile_matches.last.id }
    let(:benefits) { @offer2.benefits }

    let(:raw_post) { params.to_json }

    example_request "Creating an account" do
      expect(status).to eq(201)
    end
  end

  post "/offers/:id/accept" do
    let(:id) { @offer.id }
    let(:raw_post) { params.to_json }

    example_request "Toggle acception of an Offer" do
      expect(status).to eq(201)
      json_response = JSON.parse(response_body)
      expect(json_response["data"]["attributes"]["status"]).to eq("accepted")
    end
  end

  post "/offers/:id/reject" do
    let(:id) { @offer.id }
    let(:raw_post) { params.to_json }

    example_request "Toggle rejection of an Offer" do
      expect(status).to eq(201)
      json_response = JSON.parse(response_body)
      expect(json_response["data"]["attributes"]["status"]).to eq("rejected")
    end
  end

  delete "/offers/:id" do
    before do
      header "Authorization", encode_token(user2.auth_token)
    end

    let(:id) { @offer.id }
    let(:raw_post) { params.to_json }

    example_request "Delete an Offer" do
      expect(status).to eq(204)
      expect{ Offer.find(@offer.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
