OmniAuth.config.test_mode = true

RSpec.configure do |config|
  config.after(:each) do
    OmniAuth.config.mock_auth[:linkedin] = nil
    OmniAuth.config.mock_auth[:github] = nil
  end
end
