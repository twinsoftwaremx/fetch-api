require 'rails_helper'

RSpec.describe OmniauthCallbacksController, type: :request do
  describe 'Github' do
    context "new user" do
      before do
        @user = build(:user)
        Rails.application.env_config["devise.mapping"] = Devise.mappings[:user]
        OmniAuth.config.mock_auth[:github] = OmniAuth::AuthHash.new({
          provider: 'github',
          uid: '123545',
          info: {
            email: @user.email, name: Faker::Name.name, image: "https://avatar.example.com",
            urls: {"Github" => "https://example.github.com"}
          },
          credentials: {"token"=>"blah", "expires"=>false}
        })
        Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:github]
      end

      it "redirects to the callback" do
        get '/users/auth/github'
        expect(response).to redirect_to '/users/auth/github/callback'
      end

      it "callback behaves correctly" do
        expect {
          get '/users/auth/github/callback'
        }.to change{User.count}.by(1)
        expect(response.status).to eq(302)
      end

      it "callback also creates a TalentProfile" do
        OmniauthCallbacksController.any_instance.stub(:auth_params).and_return({"type" => "talent"})
        expect{
          get '/users/auth/github/callback'
        }.to change{TalentProfile.count}.by(1)
      end
    end
    context 'existing user' do
      before do
        @user = create(:user)
        sign_in_user(@user)
        Rails.application.env_config["devise.mapping"] = Devise.mappings[:user]
        OmniAuth.config.mock_auth[:github] = OmniAuth::AuthHash.new({
          provider: 'github',
          uid: '123545',
          info: {
            email: @user.email, name: Faker::Name.name, image: "https://avatar.example.com",
            urls: {"Github" => "https://example.github.com"}},
          credentials: {"token"=>"blah", "expires"=>false}
          })
        Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:github]
      end

      it "callback creates a new identitiy for the user" do
        expect(@user.identities.size).to eq(0)
        get '/users/auth/github/callback'
        expect(response.status).to eq(302)
        expect(@user.reload.identities.size).to eq(1)
      end
    end
  end

  describe 'Linkedin' do
    context "new user" do
      before do
        @user = build(:user)
        Rails.application.env_config["devise.mapping"] = Devise.mappings[:user]
        OmniAuth.config.mock_auth[:linkedin] = OmniAuth::AuthHash.new({
          provider: 'linkedin',
          uid: '123545',
          info: {
            email: @user.email,
            name: Faker::Name.name,
            image: "https://avatar.example.com",
            urls: {
              public_profile: "https://example.linkedin.com"
            }
          },
          extra: {
            access_token: {token: 'blah', secret: 'blah' }
          }
        })
        Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:linkedin]
      end

      it "redirects to the callback" do
        get '/users/auth/linkedin'
        expect(response).to redirect_to '/users/auth/linkedin/callback'
      end

      it "callback creates a new user" do
        expect {
          get '/users/auth/linkedin/callback'
        }.to change{User.count}.by(1)
        expect(response.status).to eq(302)
      end

      it "callback also creates a new TalentProfile" do
        OmniauthCallbacksController.any_instance.stub(:auth_params).and_return({"type" => "talent"})
        expect{
          get '/users/auth/linkedin/callback'
        }.to change{TalentProfile.count}.by(1)
      end
    end

    context "existing user" do
      before do
        @user = create(:user)
        sign_in_user(@user)
        Rails.application.env_config["devise.mapping"] = Devise.mappings[:user]
        OmniAuth.config.mock_auth[:linkedin] = OmniAuth::AuthHash.new({
          provider: 'linkedin',
          uid: '123545',
          info: {
            email: @user.email,
            name: Faker::Name.name,
            image: "https://avatar.example.com",
            urls: {
              public_profile: "https://example.linkedin.com"
            }
          },
          extra: {
            access_token: {token: 'blah', secret: 'blah' }
          }
        })
        Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:linkedin]
      end

      it "callback creates a new identity for the user" do
        expect(@user.identities.size).to eq(0)
        get '/users/auth/linkedin/callback'
        expect(response.status).to eq(302)
        expect(@user.reload.identities.size).to eq(1)
      end
    end
  end
end

def sign_in_user(user)
  post_via_redirect user_session_path, 'user[email]' => user.email, 'user[password]' => 'password123?'
end
