require 'rails_helper'

RSpec.describe "Sending messages between users", type: :request do
  
  before do
    @talent_user = create(:user, role:'talent')
    @employer_user = create(:user, role:'employer_contact')
    @account = create(:account)
    @employer_contact = create(:employer_contact, user:@employer_user, account:@account)
    @position = create(:position, employer_contact:@employer_contact)
  end

  describe "Send message" do
    it "should let the talent send a message to an employer contact" do

      # ***************************************** 
      # Section for the Talent User
      # ***************************************** 
      user_data = {
        email:@talent_user.email,
        password:@talent_user.password
      }
      headers_without_auth = {
        "Accept":"application/vnd.fetchapi.v1+json", 
        "Content-Type":"application/json"        
      }
      # Login talent user
      post '/sessions', {session:user_data}.to_json, headers_without_auth
      expect(response.status).to eq(200)
      json_response = JSON.parse(response.body)
      auth_token = json_response['data']['attributes']['auth_token']

      # Add authorization token to headers
      headers_with_token = headers_without_auth
      headers_with_token['Authorization'] = ActionController::HttpAuthentication::Token.encode_credentials(auth_token)

      message_data = {
        'message': {
          'body': 'This is a test message',
          'to_user_id': @employer_user.id,
          'position_id': @position.id
        }
      }.to_json
      post "/messages", message_data, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'messages'
      expect(json_response['data']['id']).to_not be_nil

      # Get messages sent for the current user to the employer about the position
      get "/messages/for_position/#{@position.id}/to_user/#{@employer_user.id}/sent",
          {}, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data'].count).to eq 1
      
      # ***************************************** 
      # Section for the Employer User
      # ***************************************** 
      user_data = {
        email:@employer_user.email,
        password:@employer_user.password
      }
      headers_without_auth = {
        "Accept":"application/vnd.fetchapi.v1+json", 
        "Content-Type":"application/json"        
      }
      # Login employer user
      post '/sessions', {session:user_data}.to_json, headers_without_auth
      expect(response.status).to eq(200)
      json_response = JSON.parse(response.body)
      auth_token = json_response['data']['attributes']['auth_token']

      # Add authorization token to headers
      headers_with_token = headers_without_auth
      headers_with_token['Authorization'] = ActionController::HttpAuthentication::Token.encode_credentials(auth_token)
   
      # Get all the messages received by the current user
      get "/messages", {}, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data'].count).to eq 1

      # Get messages received by the current user for a particular position
      get "/messages/for_position/#{@position.id}/received", {}, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data'].count).to eq 1
    end
  end

end