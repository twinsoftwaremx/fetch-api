require 'rails_helper.rb'

RSpec.describe "Create a position", type: :request do

  before do
    @employment_type = create(:employment_type)
  end

  describe "create a position process" do

    it "should let the employer_contact user create a position" do

      user_data = {
        email:"employer@email.com",
        password:"password123?",
        password_confirmation:"password123?",
        role:"account_admin"
      }
      headers_without_auth = {
        "Accept":"application/vnd.fetchapi.v1+json", 
        "Content-Type":"application/json"        
      }
      # Signup new account_admin user
      post '/users', {user:user_data}.to_json, headers_without_auth
      expect(response.status).to eq(201)
      json_response = JSON.parse(response.body)
      auth_token = json_response['data']['attributes']['auth_token']

      # Add authorization token to headers
      headers_with_token = headers_without_auth
      headers_with_token['Authorization'] = ActionController::HttpAuthentication::Token.encode_credentials(auth_token)

      # Create the account
      account_params = {
        'account': {
          'company_name': 'Company name',
          'size': 'company size',
          'phone': Faker::PhoneNumber.phone_number,
          'summary': 'Company summary',
          'benefits': 'Company benefits',
          'perks': 'Company perks',
          'culture': 'remote',
          'github_url': 'https://www.github.com/company_x',
          'company_site_url': 'http://www.company.com'
        },
        'location': {
          'line1': '3800 Colonnade Parkway Suite 140',
          'line2': '',
          'city': 'Birmingham',
          'state': 'AL',
          'zip': '35243'
        }
      }.to_json
      post "/accounts", account_params, headers_with_token
      json_response = JSON.parse(response.body)
      account_id = json_response['data']['id']
      expect(json_response['data']['type']).to eq 'accounts'
      expect(account_id).to_not be_nil

      # Create employer contact
      employer_contact_params = {
        'employer_contact': {
          'name': Faker::Name.name,
          'title': Faker::Name.title,
          'linked_in_url': 'https://linkedin.com/profile/123',
          'summary': 'summary',
          'phone': Faker::PhoneNumber.phone_number,
          'time_zone': 'CST',
          'account_id': account_id
        }
      }.to_json
      post "/employer_contacts", employer_contact_params, headers_with_token
      json_response = JSON.parse(response.body)
      employer_contact_id = json_response['data']['id']
      expect(json_response['data']['type']).to eq 'employer_contacts'
      expect(employer_contact_id).to_not be_nil

      # Create a position
      position_params = {
        'position': {
          'job_title': Faker::Name.title,
          'description': 'Description of the position',
          'hours': '40 hours',
          'salary_range': '80k - 100k',
          'desired_years_experience': '5 years',
          'team_size': '3 developers',
          'employment_type_id': @employment_type.id
        }
      }.to_json
      post "/employer_contacts/#{employer_contact_id}/positions", position_params, headers_with_token
      json_response = JSON.parse(response.body)
      expect(json_response['data']['type']).to eq 'positions'
      expect(json_response['data']['id']).to_not be_nil
    end

  end

end