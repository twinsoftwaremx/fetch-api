require 'rails_helper'

RSpec.describe ActiveAdmin, type: :request do
  describe 'Admin' do
    describe 'unathenticated' do
      it 'should redirect to /admin/login' do
        get '/admin'
        expect(response).to redirect_to '/admin/login'
      end
    end
    # Lets just test that the index routes for each resource is accessible
    describe 'authenticated' do
      ROUTES = ['/admin', '/admin/users', '/admin/talent_profiles',
        '/admin/company_sizes', '/admin/admin_users', '/admin/assets',
        '/admin/accounts', '/admin/employer_contacts', '/admin/skills',
        '/admin/interests', '/admin/addresses', '/admin/positions', '/admin/dashboard',
        '/admin/agents', '/admin/comments', '/admin/employment_types', '/admin/locations',
        '/admin/skill_categories', '/admin/offers']

      before(:all) do
        login_admin_user(FactoryGirl.create(:admin_user))
      end

      ROUTES.each do |route|
        it "#{route} should be accessible" do
          get route
          verify_correct_admin_response
        end
      end
    end
  end

  def login_admin_user(user)
    post admin_user_session_path, admin_user: {email: user.email, password: 'password'}
  end

  def verify_correct_admin_response
    expect(response).to render_template(:index)
    expect(response.status).to eq(200)
  end
end
