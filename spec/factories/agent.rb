FactoryGirl.define do
  factory :agent do
    name {Faker::Name.name }
    association :admin_user
  end
end
