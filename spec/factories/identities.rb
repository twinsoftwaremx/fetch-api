# == Schema Information
#
# Table name: identities
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  provider   :string
#  uid        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_identities_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_5373344100  (user_id => users.id)
#

FactoryGirl.define do
  factory :identity do
    association :user
    provider "github"
    uid { Random.rand(9999) }
  end
end
