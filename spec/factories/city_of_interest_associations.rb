# == Schema Information
#
# Table name: city_of_interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  city_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_city_of_interest_associations_on_city_id            (city_id)
#  index_city_of_interest_associations_on_talent_profile_id  (talent_profile_id)
#

FactoryGirl.define do
  factory :city_of_interest_association do
    talent_profile
    city
  end

end
