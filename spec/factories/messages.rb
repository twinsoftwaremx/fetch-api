# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  body         :text             not null
#  to_user_id   :integer          not null
#  from_user_id :integer          not null
#  position_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_messages_on_from_user_id  (from_user_id)
#  index_messages_on_position_id   (position_id)
#  index_messages_on_to_user_id    (to_user_id)
#

FactoryGirl.define do
  factory :message do
    body "MyText"
    association :to_user, factory: :user
    association :from_user, factory: :user
    position
  end

end
