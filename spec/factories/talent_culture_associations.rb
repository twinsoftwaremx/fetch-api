# == Schema Information
#
# Table name: talent_culture_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  culture_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_talent_culture_associations_on_culture_id         (culture_id)
#  index_talent_culture_associations_on_talent_profile_id  (talent_profile_id)
#

FactoryGirl.define do
  factory :talent_culture_association do
    association :talent_profile
    association :culture
  end
end
