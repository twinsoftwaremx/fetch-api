# == Schema Information
#
# Table name: skills
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  skill_category_id :integer
#

FactoryGirl.define do
  factory :skill do
    name { Faker::Lorem.word }
  end
end
