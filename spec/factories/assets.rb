# == Schema Information
#
# Table name: assets
#
#  id             :integer          not null, primary key
#  file_id        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  assetable_id   :integer
#  assetable_type :string
#  type           :string
#

FactoryGirl.define do
  factory :asset, class: 'Asset' do
    file { Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec', 'support', 'test.docx')), 'application/msword')}
  end

  factory :photo, parent: :asset, class: 'Photo' do
    # type "Photo"
    file { Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec', 'support', 'test.jpeg')), 'application/jpeg')}
  end

  factory :resume, parent: :asset, class: 'Resume' do
    # type "Resume"
    file { Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec', 'support', 'test.docx')), 'application/msword')}
  end
end
