# == Schema Information
#
# Table name: offers
#
#  id                      :integer          not null, primary key
#  talent_profile_match_id :integer          not null
#  employer_contact_id     :integer          not null
#  status                  :integer          default(0)
#  company_name            :string
#  position_title          :string
#  manager_name            :string
#  salary_pay_rate         :string
#  start_date              :string
#  benefits                :text
#  legal_verbiage          :text
#  hiring_managers_name    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  details                 :text
#  job_type                :string
#
# Indexes
#
#  index_offers_on_employer_contact_id      (employer_contact_id)
#  index_offers_on_talent_profile_match_id  (talent_profile_match_id)
#

FactoryGirl.define do
  factory :offer do
    talent_profile_match
    employer_contact
    status 0
    company_name { Faker::Name.title }
    position_title { Faker::Name.title }
    manager_name  { Faker::Name.name }
    salary_pay_rate "$50/h"
    start_date     { Faker::Date.forward(30).to_s }
    benefits        "401k, Health Insurance, Commission"
    legal_verbiage    "Legal mumbo jumbo"
    hiring_managers_name  { Faker::Name.name }
  end

end
