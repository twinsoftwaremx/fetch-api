# == Schema Information
#
# Table name: github_assets
#
#  id                :integer          not null, primary key
#  follower_count    :integer
#  public_repo_count :integer
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_github_assets_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_beaad0bb71  (user_id => users.id)
#

FactoryGirl.define do
  factory :github_asset do
    follower_count 1
    public_repo_count 1
    user nil
  end
end
