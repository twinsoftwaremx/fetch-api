# == Schema Information
#
# Table name: previous_employments
#
#  id                :integer          not null, primary key
#  company           :string
#  position          :string
#  start_date        :date
#  end_date          :date
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

FactoryGirl.define do
  factory :previous_employment do
    association :talent_profile
    company { Faker::Company.name }
    position "MyString"
    start_date { Date.today }
    end_date { Date.today + 3.years }
    description { Faker::Lorem.paragraph }
  end
end
