# == Schema Information
#
# Table name: states
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  abbreviation :string           not null
#

FactoryGirl.define do
  factory :state do
    name "MyString"
    abbreviation "MS"
  end

end
