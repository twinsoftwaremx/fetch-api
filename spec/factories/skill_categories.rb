# == Schema Information
#
# Table name: skill_categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :skill_category do
    name "MyString"
  end

end
