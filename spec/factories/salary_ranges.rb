# == Schema Information
#
# Table name: salary_ranges
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :salary_range do
    name { "$0 - $#{Random.rand(500)}k"}
  end
end
