# == Schema Information
#
# Table name: positions
#
#  id                       :integer          not null, primary key
#  job_title                :string           not null
#  description              :text
#  hours                    :integer          default(0)
#  desired_years_experience :integer          default(0)
#  team_size                :string
#  employment_type_id       :integer          not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  employer_contact_id      :integer
#  remote                   :boolean          default(FALSE)
#  location_id              :integer
#  salary_range_id          :integer
#  city_id                  :integer
#  hiring_manager           :string
#  is_deleted               :boolean          default(FALSE)
#
# Indexes
#
#  index_positions_on_city_id              (city_id)
#  index_positions_on_employer_contact_id  (employer_contact_id)
#  index_positions_on_employment_type_id   (employment_type_id)
#

FactoryGirl.define do
  factory :position do
    job_title Faker::Name.title
    description Faker::Lorem.paragraph
    hours Faker::Number.between(20, 60)
    desired_years_experience Faker::Number.between(0,20)
    team_size "MyString"
    employment_type
    employer_contact
    location
    salary_range
    city

    trait :without_location do
      after(:create) do |position, evaluator|
        position.remote = true
        position.location = nil
      end
    end

    trait :with_bonus_skill_list do
      transient do
        number_of_bonus_skills 1
      end

      after(:create) do |position, evaluator|
        create_list(:bonus_skill_association, evaluator.number_of_bonus_skills, position: position)
      end
    end

    trait :with_required_skill_list do
      transient do
        number_of_skills 1
      end

      after(:create) do |position, evaluator|
        create_list(:required_skill_association, evaluator.number_of_skills, position: position)
      end
    end

  end

end
