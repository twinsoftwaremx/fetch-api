# == Schema Information
#
# Table name: accounts
#
#  id               :integer          not null, primary key
#  company_name     :string
#  phone            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  summary          :text
#  benefits         :text
#  perks            :text
#  github_url       :string
#  company_site_url :string
#  address_id       :integer
#  culture          :integer          default(0)
#  agent_id         :integer
#  company_size_id  :integer
#  year_founded     :integer
#  revenue          :string
#  who_we_are       :text
#  what_we_do       :text
#  video_url        :string
#
# Indexes
#
#  index_accounts_on_company_size_id  (company_size_id)
#

FactoryGirl.define do
  factory :account do
    company_name Faker::Company.name
    phone Faker::PhoneNumber.phone_number
    summary Faker::Lorem.paragraph
    benefits Faker::Lorem.paragraph
    perks Faker::Lorem.paragraph
    culture 1
    github_url Faker::Internet.url('github.com')
    company_site_url Faker::Internet.url
    address {create(:full_address) }
    company_size
    year_founded 2000
    revenue "MyString"
    who_we_are "MyString"
    what_we_do "MyString"
    video_url Faker::Internet.url('youtube.com')
  end

end
