# == Schema Information
#
# Table name: location_of_interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  location_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  idx_loc_int_on_intlocid                                       (location_id)
#  index_location_of_interest_associations_on_talent_profile_id  (talent_profile_id)
#

FactoryGirl.define do
  factory :location_of_interest_association do
    association :talent_profile
    association :location
  end
end
