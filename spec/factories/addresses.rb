# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  line1      :string
#  line2      :string
#  city       :string
#  state      :string
#  zip        :string
#  type       :string
#  latitude   :float
#  longitude  :float
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_addresses_on_type  (type)
#

FactoryGirl.define do
  factory :address do
    zip Faker::Address.zip
  end

  factory :full_address, class: "FullAddress" do
    line1 Faker::Address.street_address
    line2 Faker::Address.secondary_address
    city Faker::Address.city
    state Faker::Address.state
    zip Faker::Address.zip
    latitude Faker::Address.latitude
    longitude Faker::Address.longitude
  end

  factory :location, class: "Location" do
    city Faker::Address.city
    state Faker::Address.state
    zip Faker::Address.zip
    latitude Faker::Address.latitude
    longitude Faker::Address.longitude
  end
end
