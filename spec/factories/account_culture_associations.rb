# == Schema Information
#
# Table name: account_culture_associations
#
#  id         :integer          not null, primary key
#  culture_id :integer
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_account_culture_associations_on_account_id  (account_id)
#  index_account_culture_associations_on_culture_id  (culture_id)
#

FactoryGirl.define do
  factory :account_culture_association do
    association :culture
    association :account
  end
end
