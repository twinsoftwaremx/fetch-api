# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cities_on_state_id  (state_id)
#

FactoryGirl.define do
  factory :city do
    name "MyString"
    state
  end

end
