# == Schema Information
#
# Table name: cultures
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :culture do
    name { Faker::Lorem.word }
  end

end
