# == Schema Information
#
# Table name: talent_profiles
#
#  id                                           :integer          not null, primary key
#  name                                         :string           not null
#  time_zone                                    :string           not null
#  phone                                        :string
#  summary                                      :text
#  area_of_interest                             :string
#  years_of_experience                          :integer          default(0)
#  usdod_clearance                              :boolean          default(FALSE)
#  reason_for_interest_in_new_job_opportunities :text
#  job_search_status                            :integer          default(0)
#  usa_work_auth_type                           :integer          default(0)
#  linked_in_url                                :string
#  personal_website                             :string
#  dribbble_url                                 :string
#  stack_overflow_url                           :string
#  blog_url                                     :string
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  location_id                                  :integer
#  agent_id                                     :integer
#  salary_range_id                              :integer
#  github_url                                   :string
#  avatar_url                                   :string
#  title                                        :string
#  company                                      :string
#

FactoryGirl.define do
  factory :talent_profile do
    name { Faker::Name.name }
    time_zone "MyString"
    phone "MyString"
    summary "MyText"
    area_of_interest "MyString"
    years_of_experience 1
    usdod_clearance false
    reason_for_interest_in_new_job_opportunities "MyText"
    job_search_status 1
    salary_range
    usa_work_auth_type 1
    linked_in_url "MyString"
    personal_website "MyString"
    dribbble_url "MyString"
    stack_overflow_url "MyString"
    blog_url "MyString"
    association :location, factory: :full_address
    user

    trait :with_skill_list do
      transient do
        number_of_skills 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:skill_association, evaluator.number_of_skills, talent_profile: talent_profile)
      end
    end

    trait :with_interests do
      transient do
        number_of_interests 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:interest_association, evaluator.number_of_interests, talent_profile: talent_profile)
      end
    end

    trait :with_locations_of_interest do
      transient do
        number_of_locations_of_interest 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:location_of_interest_association, evaluator.number_of_locations_of_interest, talent_profile: talent_profile)
      end
    end

    trait :with_pref_company_sizes do
      transient do
        number_of_pref_company_sizes 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:preferred_company_size_association, evaluator.number_of_pref_company_sizes, talent_profile: talent_profile)
      end
    end

    trait :with_previous_employments do
      transient do
        number_of_previous_employments 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:previous_employment, evaluator.number_of_previous_employments, talent_profile: talent_profile)
      end
    end

    trait :with_education_items do
      transient do
        number_of_education_items 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:education_item, evaluator.number_of_education_items, talent_profile: talent_profile)
      end
    end

    trait :with_city_list do
      transient do
        number_of_cities 1
      end

      after(:create) do |talent_profile, evaluator|
        create_list(:city_of_interest_association, evaluator.number_of_cities, talent_profile: talent_profile)
      end
    end
  end
end
