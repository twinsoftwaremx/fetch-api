# == Schema Information
#
# Table name: employer_contacts
#
#  id            :integer          not null, primary key
#  name          :string           not null
#  title         :string
#  linked_in_url :string
#  summary       :text
#  phone         :string
#  time_zone     :string
#  account_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#
# Indexes
#
#  index_employer_contacts_on_account_id  (account_id)
#  index_employer_contacts_on_user_id     (user_id)
#

FactoryGirl.define do
  factory :employer_contact do
    name Faker::Name.name
    title Faker::Name.title
    linked_in_url Faker::Internet.url('linkedin.com')
    summary "MyText"
    phone Faker::PhoneNumber.cell_phone
    time_zone "MyString"
    account
    user

    trait :with_positions do
      transient do
        number_of_positions 1
      end

      after(:create) do |employer_contact, evaluator|
        create_list(:position, evaluator.number_of_positions, employer_contact: employer_contact)
      end
    end
  end

end
