# == Schema Information
#
# Table name: skill_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  skill_id          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  rank              :integer
#
# Indexes
#
#  index_skill_associations_on_skill_id           (skill_id)
#  index_skill_associations_on_talent_profile_id  (talent_profile_id)
#

FactoryGirl.define do
  factory :skill_association do
    association :talent_profile
    association :skill
    rank 10
  end
end
