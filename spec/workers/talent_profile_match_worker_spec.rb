require 'rails_helper'

RSpec.describe TalentProfileMatchWorker do
  describe 'scheduling' do
    it 'will be scheduled daily' do
      valid, invalid = times

      expect( next_occurrence ).to eq(valid)
      expect( next_occurrence ).not_to eq(invalid)

      Timecop.freeze(Time.now.midnight + 1.day)

      # Testing the past time is no longer valid
      expect( next_occurrence ).not_to eq(valid)

      valid, invalid = times(1.days)

      expect( next_occurrence ).to eq(valid)
      expect( next_occurrence ).not_to eq(invalid)
      Timecop.return
    end
  end

  def next_occurrence
    TalentProfileMatchWorker.schedule.next_occurrence
  end

  def times(offset = 1.day)
    valid = Time.now.midnight + offset
    invalid = valid + 1.minute

    [valid, invalid]
  end
end
