require 'rails_helper'

RSpec.describe GithubWorker do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_id) { user.id }
  let(:github_worker) { GithubWorker.new }

  it 'should create a GithubAsset with User relation' do
    user_info = double(:public_repos => 10, :followers => 10)
    users = double(:get => user_info)
    github = double(:users => users)
    allow(Github).to receive(:new).with(oauth_token: "blah").and_return(github)
    github_worker.perform('blah', user_id)
    g_asset = GithubAsset.last
    expect(g_asset.user).to eq(user)
    expect(g_asset.public_repo_count).to eq(10)
    expect(g_asset.follower_count).to eq(10)
  end
end
