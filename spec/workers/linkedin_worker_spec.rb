require 'rails_helper'

RSpec.describe LinkedinWorker do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_id) { user.id }
  let(:linkedin_worker) { LinkedinWorker.new }

  it 'should create a LinkedinAsset with User relation' do
    linkedin = double("Linkedin")
    linkedin_session = double(:profile => "{'some':'info'}")
    linkedin.stub(:authorize_from_access).with('blah', 'blah')
    linkedin.stub(:profile)
    allow(::LinkedIn::Client).to receive(:new).and_return(linkedin)
    linkedin_worker.perform('blah', 'blah', user_id)
    l_asset = LinkedinAsset.last
    expect(l_asset.user).to eq(user)
  end
end
