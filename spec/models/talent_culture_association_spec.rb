# == Schema Information
#
# Table name: talent_culture_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  culture_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_talent_culture_associations_on_culture_id         (culture_id)
#  index_talent_culture_associations_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe TalentCultureAssociation, type: :model do
  describe "factory" do
    let(:talent_culture_association) { build(:talent_culture_association) }

    it "should be valid" do
      expect(talent_culture_association).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile) }
    it { is_expected.to validate_presence_of(:culture) }
  end
end
