# == Schema Information
#
# Table name: talent_profiles
#
#  id                                           :integer          not null, primary key
#  name                                         :string           not null
#  time_zone                                    :string           not null
#  phone                                        :string
#  summary                                      :text
#  area_of_interest                             :string
#  years_of_experience                          :integer          default(0)
#  usdod_clearance                              :boolean          default(FALSE)
#  reason_for_interest_in_new_job_opportunities :text
#  job_search_status                            :integer          default(0)
#  usa_work_auth_type                           :integer          default(0)
#  linked_in_url                                :string
#  personal_website                             :string
#  dribbble_url                                 :string
#  stack_overflow_url                           :string
#  blog_url                                     :string
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  location_id                                  :integer
#  agent_id                                     :integer
#  salary_range_id                              :integer
#  github_url                                   :string
#  avatar_url                                   :string
#  title                                        :string
#  company                                      :string
#

require 'rails_helper'

RSpec.describe TalentProfile, type: :model do
  describe "factory" do
    let(:talent_profile) {build(:talent_profile) }

    it "factory should be valid" do
      expect(talent_profile).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:time_zone) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:location)}
    it { is_expected.to belong_to(:agent) }
    it { is_expected.to have_many(:interests).through(:interest_associations) }
    it { is_expected.to have_many(:skills).through(:skill_associations) }
    it { is_expected.to have_many(:preferred_company_sizes).through(:preferred_company_size_associations) }
    it { is_expected.to have_many(:previous_employments) }
    it { is_expected.to have_many(:education_items) }
    it { is_expected.to have_many(:locations_of_interest).through(:location_of_interest_associations)}
    it { is_expected.to have_many(:cities_of_interest).through(:city_of_interest_associations) }
    it { is_expected.to have_one(:user).through(:talent) }
    it { is_expected.to have_one(:photo).class_name('Photo') }
    it { is_expected.to have_one(:resume).class_name('Resume') }
    it { is_expected.to have_many(:matches).through(:talent_profile_matches) }
    it { is_expected.to belong_to(:salary_range)}
  end

  describe "#skill_list" do
    let(:talent_profile) { create(:talent_profile, :with_skill_list, number_of_skills: 3) }

    it "returns a string of Skills" do
      expect(talent_profile.skills.count).to eq 3
      expect(talent_profile.skill_list).to include(Skill.first.name)
    end

    it "should be able to set the skill list" do
      talent_profile.skill_list="Adams,Rules"
      talent_profile.save
      expect(talent_profile.skills.count).to eq 2
      expect(talent_profile.skill_list).to include("Adam")
    end
  end

  describe "#city_list" do
    let(:talent_profile) { create(:talent_profile, :with_city_list, number_of_cities: 2) }

    it "returns a string of Cities" do
      expect(talent_profile.cities_of_interest.count).to eq 2
      expect(talent_profile.city_list).to include(City.first.name)
    end
  end

  describe "get_recent_conversations" do
    before do
      @talent_profile = create(:talent_profile)
      employer_contact1 = create(:employer_contact, :with_positions, number_of_positions: 3)
      employer_contact2 = create(:employer_contact, :with_positions, number_of_positions: 3)
      employer_contact3 = create(:employer_contact, :with_positions, number_of_positions: 3)
      
      create(:message, to_user: employer_contact1.user, 
              from_user: @talent_profile.user, 
              position: employer_contact1.positions.first)
      create(:message, to_user: employer_contact2.user, 
              from_user: @talent_profile.user, 
              position: employer_contact2.positions.first)
      create(:message, to_user: employer_contact3.user, 
              from_user: @talent_profile.user, 
              position: employer_contact3.positions.first)
      create(:message, to_user: employer_contact3.user, 
              from_user: @talent_profile.user, 
              position: employer_contact3.positions.last)
    end

    it "should return a valid array with the results" do
      result = TalentProfile.get_recent_conversations(@talent_profile.id, 3)
      expect(result.count).to eq 3
    end

    it "should return the records ordered by most recent" do
      result = TalentProfile.get_recent_conversations(@talent_profile.id, 3)
      expect(result[0][:last_message_date]).to be > result[1][:last_message_date]
      expect(result[1][:last_message_date]).to be > result[2][:last_message_date]
    end

    it "should return an array of hashes with valid keys" do
      result = TalentProfile.get_recent_conversations(@talent_profile.id, 3)
      expect(result[0].key?(:job_id)).to be true
      expect(result[0].key?(:employer_contact_name)).to be true
      expect(result[0].key?(:user_id)).to be true
      expect(result[0].key?(:photo_url)).to be true
      expect(result[0].key?(:last_message_date)).to be true
    end

    it "should return default count for recent conversations if passed 0 as count parameter" do
      result = TalentProfile.get_recent_conversations(@talent_profile.id, 0)
      expect(result.count).to eq TalentProfile::DEFAULT_RECENT_CONVERSATIONS
    end
  end

  describe "building up a talent profile" do
    it "can build in steps" do
      attrs = attributes_for(:talent_profile)
      talent_profile = TalentProfile.new(attrs)
      expect(talent_profile.save).to eq(true)

      skill_list = "ruby,c++"
      talent_profile.skill_list = skill_list
      expect(talent_profile.skills).to include(Skill.find_by(name: "ruby"))

      interest_list = create_list(:interest, 2)
      talent_profile.interests = interest_list
      expect(talent_profile.interests.count).to eq(interest_list.count)

      location = create(:full_address)
      talent_profile.location = location
      expect(location.id).to eq(talent_profile.location.id)

      interested_locations = create_list(:location, 4)
      talent_profile.locations_of_interest = interested_locations
      expect(talent_profile.locations_of_interest.count).to eq(interested_locations.count)

      previous_employments = create_list(:previous_employment, 5)
      talent_profile.previous_employments = previous_employments
      expect(talent_profile.previous_employments.count).to eq(previous_employments.count)
      expect(talent_profile.save).to eq(true)

      education_items = create_list(:education_item, 2)
      talent_profile.education_items = education_items
      expect(talent_profile.education_items.count).to eq(education_items.count)

      preferred_company_sizes = create_list(:company_size, 2)
      talent_profile.preferred_company_sizes = preferred_company_sizes
      expect(talent_profile.preferred_company_sizes.count).to eq(preferred_company_sizes.count)
    end
  end
end
