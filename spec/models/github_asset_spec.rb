# == Schema Information
#
# Table name: github_assets
#
#  id                :integer          not null, primary key
#  follower_count    :integer
#  public_repo_count :integer
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_github_assets_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_beaad0bb71  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe GithubAsset, type: :model do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_id) { user.id }

  it 'should relate to a user' do
    g_asset = GithubAsset.new(user_id: user_id)
    expect(g_asset.user).to eq(user)
  end
end
