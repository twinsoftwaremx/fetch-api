# == Schema Information
#
# Table name: preferred_company_size_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  company_size_id   :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_preferred_company_size_associations_on_company_size_id    (company_size_id)
#  index_preferred_company_size_associations_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe PreferredCompanySizeAssociation, type: :model do
  describe "factory" do
    let(:preferred_company_size_association) { build(:preferred_company_size_association) }

    it "shoud be valid" do
      expect(preferred_company_size_association).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile) }
    it { is_expected.to validate_presence_of(:company_size) }
  end
end
