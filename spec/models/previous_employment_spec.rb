# == Schema Information
#
# Table name: previous_employments
#
#  id                :integer          not null, primary key
#  company           :string
#  position          :string
#  start_date        :date
#  end_date          :date
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

require 'rails_helper'

RSpec.describe PreviousEmployment, type: :model do
  describe "factory" do
    let(:previous_employment) {build(:previous_employment)}

    it "should be valid" do
      expect(previous_employment).to be_valid
    end
  end

  describe "associations" do
    it { is_expected.to belong_to(:talent_profile)}
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile)}
    it { is_expected.to validate_presence_of(:company)}
    it { is_expected.to validate_presence_of(:position)}
  end
end
