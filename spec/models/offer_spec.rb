# == Schema Information
#
# Table name: offers
#
#  id                      :integer          not null, primary key
#  talent_profile_match_id :integer          not null
#  employer_contact_id     :integer          not null
#  status                  :integer          default(0)
#  company_name            :string
#  position_title          :string
#  manager_name            :string
#  salary_pay_rate         :string
#  start_date              :string
#  benefits                :text
#  legal_verbiage          :text
#  hiring_managers_name    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  details                 :text
#  job_type                :string
#
# Indexes
#
#  index_offers_on_employer_contact_id      (employer_contact_id)
#  index_offers_on_talent_profile_match_id  (talent_profile_match_id)
#

require 'rails_helper'

RSpec.describe Offer, type: :model do
  describe "factory" do
    let(:offer) { build(:offer) }

    it "should be valid" do
      expect(offer).to be_valid
      expect(offer.status).to eq("unanswered")
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile_match) }
    it { is_expected.to validate_presence_of(:employer_contact) }
    it { is_expected.to validate_presence_of(:company_name) }
    it { is_expected.to validate_presence_of(:position_title) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:talent_profile_match) }
    it { is_expected.to belong_to(:employer_contact) }
  end
end
