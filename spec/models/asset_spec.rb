# == Schema Information
#
# Table name: assets
#
#  id             :integer          not null, primary key
#  file_id        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  assetable_id   :integer
#  assetable_type :string
#  type           :string
#

require 'rails_helper'

RSpec.describe Asset, type: :model do
  describe "factory" do
    let(:photo) {build(:photo)}

    it "should be valid" do
      expect(photo).to be_valid
    end
  end

  describe "associations" do
    it { is_expected.to belong_to(:assetable) }
  end
end
