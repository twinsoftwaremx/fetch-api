# == Schema Information
#
# Table name: states
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  abbreviation :string           not null
#

require 'rails_helper'

RSpec.describe State, type: :model do
  describe "factory" do
    let(:state) { build(:state) }

    it "should be valid" do
      expect(state).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:abbreviation) }
  end

  describe "associations" do
    it { is_expected.to have_many(:cities) }
  end
end
