# == Schema Information
#
# Table name: agents
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Agent, type: :model do
  describe "factory" do
    let(:agent) {build(:agent)}

    it "should be valid" do
      expect(agent).to be_valid
    end

    it ".admin?" do
      expect(agent.admin?).to be_truthy
    end

    it ".photo_id=" do
      photo = create(:photo)
      agent.photo_id=photo.id
      expect(agent.photo.present?).to be_truthy
    end

    describe "validations" do
      it { is_expected.to validate_presence_of(:name) }
    end

    describe "associations" do
      it { is_expected.to have_one(:admin_user) }
      it { is_expected.to have_one(:photo).class_name('Photo') }
      it { is_expected.to have_many(:talent_profiles) }
      it { is_expected.to have_many(:accounts) }
    end
  end
end
