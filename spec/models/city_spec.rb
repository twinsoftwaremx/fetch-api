# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cities_on_state_id  (state_id)
#

require 'rails_helper'

RSpec.describe City, type: :model do
  describe "factory" do
    let(:city) { build(:city) }

    it "should be valid" do
      expect(city).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:state) }
  end
end
