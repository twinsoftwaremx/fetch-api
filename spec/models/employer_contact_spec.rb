# == Schema Information
#
# Table name: employer_contacts
#
#  id            :integer          not null, primary key
#  name          :string           not null
#  title         :string
#  linked_in_url :string
#  summary       :text
#  phone         :string
#  time_zone     :string
#  account_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#
# Indexes
#
#  index_employer_contacts_on_account_id  (account_id)
#  index_employer_contacts_on_user_id     (user_id)
#

require 'rails_helper'

RSpec.describe EmployerContact, type: :model do
  describe "factory" do
    let(:employer_contact) {build(:employer_contact)}

    it "should be valid" do
      expect(employer_contact).to be_valid
    end
  end

  describe "get_recent_conversations" do
    before do
      @employer_contact = create(:employer_contact, :with_positions, number_of_positions: 3)
      talent_profile1 = create(:talent_profile)
      talent_profile2 = create(:talent_profile)
      talent_profile3 = create(:talent_profile)
      talent_profile4 = create(:talent_profile)
      talent_profile5 = create(:talent_profile)
      
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile1.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile2.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile3.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile4.user, 
              position: @employer_contact.positions.first)
      create(:message, to_user: @employer_contact.user, 
              from_user: talent_profile5.user, 
              position: @employer_contact.positions.first)
    end

    it "should return a valid array with the results" do
      result = EmployerContact.get_recent_conversations(@employer_contact.id, 3)
      expect(result.count).to eq 3
    end

    it "should return the records ordered by most recent" do
      result = EmployerContact.get_recent_conversations(@employer_contact.id, 3)
      expect(result[0][:last_message_date]).to be > result[1][:last_message_date]
      expect(result[1][:last_message_date]).to be > result[2][:last_message_date]
    end

    it "should return an array of hashes with valid keys" do
      result = EmployerContact.get_recent_conversations(@employer_contact.id, 3)
      expect(result[0].key?(:job_id)).to be true
      expect(result[0].key?(:talent_profile_name)).to be true
      expect(result[0].key?(:user_id)).to be true
      expect(result[0].key?(:photo_url)).to be true
      expect(result[0].key?(:last_message_date)).to be true
    end

    it "should return default count for recent conversations if passed 0 as count parameter" do
      result = EmployerContact.get_recent_conversations(@employer_contact.id, 0)
      expect(result.count).to eq EmployerContact::DEFAULT_RECENT_CONVERSATIONS
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:account) }
    it { is_expected.to validate_presence_of(:user) }
  end

  describe "associations" do
    it { is_expected.to have_many(:positions) }
    it { is_expected.to have_one(:photo).class_name('Photo') }
    it { is_expected.to belong_to(:account) }
    it { is_expected.to belong_to(:user) }
  end
end
