# == Schema Information
#
# Table name: talent_profile_matches
#
#  id                :integer          not null, primary key
#  position_id       :integer
#  talent_profile_id :integer
#  match_score       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  status            :integer          default(0)
#
# Indexes
#
#  index_talent_profile_matches_on_position_id        (position_id)
#  index_talent_profile_matches_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe TalentProfileMatch, type: :model do
  describe "factory" do
    let(:talent_profile_match) {build(:talent_profile_match) }

    it "factory should be valid" do
      expect(talent_profile_match).to be_valid
      expect(talent_profile_match.status).to eq("new_match")
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:position) }
    it { is_expected.to validate_presence_of(:talent_profile) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:position) }
    it { is_expected.to belong_to(:talent_profile)}
  end
end
