# == Schema Information
#
# Table name: salary_ranges
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SalaryRange, type: :model do
  describe "factory" do
    let(:salary_range) {build(:salary_range)}

    it "should be valid" do
      expect(salary_range).to be_valid
    end
  end

  describe "associations" do
    it { is_expected.to have_many(:talent_profiles) }
    it { is_expected.to have_many(:positions) }
  end
end
