require 'rails_helper'

RSpec.describe FeeStructure, type: :model do
  describe '#calculate_fees' do
    before do
      @salary1 = 55000
      @salary2 = 80000
      @low_salary = 1000
    end

    it "should return correct fees" do
      fee_structure = FeeStructure.new
      fee_structure.calculate_fees(@salary1)
      expect(fee_structure.standard_fee).to eq 8850
      expect(fee_structure.monthly_subscription).to eq 250
      expect(fee_structure.upfront_fee).to eq 1000

      fee_structure.calculate_fees(@salary2)
      expect(fee_structure.standard_fee).to eq 12600
      expect(fee_structure.monthly_subscription).to eq 350
      expect(fee_structure.upfront_fee).to eq 2000
    end

    it "should return 0 if the result is negative number" do
      fee_structure = FeeStructure.new
      fee_structure.calculate_fees(@low_salary)
      expect(fee_structure.standard_fee).to eq 0
      expect(fee_structure.monthly_subscription).to eq 0
      expect(fee_structure.upfront_fee).to eq 1000
    end

    it "should return correct fees even if the salary parameter is string" do
      fee_structure = FeeStructure.new
      fee_structure.calculate_fees(@salary1.to_s)
      expect(fee_structure.standard_fee).to eq 8850
      expect(fee_structure.monthly_subscription).to eq 250
      expect(fee_structure.upfront_fee).to eq 1000
    end
  end
end