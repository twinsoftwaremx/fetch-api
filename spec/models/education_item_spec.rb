# == Schema Information
#
# Table name: education_items
#
#  id                :integer          not null, primary key
#  college           :string
#  year              :integer
#  degree            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  talent_profile_id :integer
#

require 'rails_helper'

RSpec.describe EducationItem, type: :model do
  describe "factory" do
    let(:education_item) {build(:education_item)}

    it "factory should be valid" do
      expect(education_item).to be_valid
    end
  end

  describe "associations" do
    it { is_expected.to belong_to(:talent_profile)}
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile) }
    it { is_expected.to validate_presence_of(:college)}
    it { is_expected.to validate_presence_of(:year)}
    it { is_expected.to validate_presence_of(:degree)}
  end
end
