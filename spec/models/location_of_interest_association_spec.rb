# == Schema Information
#
# Table name: location_of_interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  location_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  idx_loc_int_on_intlocid                                       (location_id)
#  index_location_of_interest_associations_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe LocationOfInterestAssociation, type: :model do
  describe "factory" do
    let(:location_of_interest_association) { build(:location_of_interest_association) }

    it "should be valid" do
      expect(location_of_interest_association).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:talent_profile) }
    it { is_expected.to validate_presence_of(:location) }
  end
end
