# == Schema Information
#
# Table name: account_culture_associations
#
#  id         :integer          not null, primary key
#  culture_id :integer
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_account_culture_associations_on_account_id  (account_id)
#  index_account_culture_associations_on_culture_id  (culture_id)
#

require 'rails_helper'

RSpec.describe AccountCultureAssociation, type: :model do
  describe "factory" do
    let(:account_culture_association) { build(:account_culture_association) }

    it "should be valid" do
      expect(account_culture_association).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:account) }
    it { is_expected.to validate_presence_of(:culture) }
  end
end
