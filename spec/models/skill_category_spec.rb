# == Schema Information
#
# Table name: skill_categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SkillCategory, type: :model do
  let(:skill_category) {build(:skill_category)}

  it "factory should be valid" do
    expect(skill_category).to be_valid
  end

  describe "associations" do
    it { is_expected.to have_many(:skills) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
  end
end
