# == Schema Information
#
# Table name: skill_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  skill_id          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  rank              :integer
#
# Indexes
#
#  index_skill_associations_on_skill_id           (skill_id)
#  index_skill_associations_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe SkillAssociation, type: :model do
  describe "factory" do
    let(:skill_association) { build(:skill_association) }

    it "should be valid" do
      expect(skill_association).to be_valid
    end
  end

  describe "validations" do
    subject { build(:skill_association) }
    it { is_expected.to validate_presence_of(:talent_profile) }
    it { is_expected.to validate_presence_of(:skill) }
    # FIXME: Uncomment this when front end logic reflects skill ranks
    # it { is_expected.to ensure_inclusion_of(:rank).in_array((1..10).to_a) }
  end
end
