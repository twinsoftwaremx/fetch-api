# == Schema Information
#
# Table name: required_skill_associations
#
#  id          :integer          not null, primary key
#  position_id :integer
#  skill_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_required_skill_associations_on_position_id  (position_id)
#  index_required_skill_associations_on_skill_id     (skill_id)
#

require 'rails_helper'

RSpec.describe RequiredSkillAssociation, type: :model do
end
