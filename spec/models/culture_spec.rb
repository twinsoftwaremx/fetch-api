# == Schema Information
#
# Table name: cultures
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Culture, type: :model do
  let(:culture) {build(:culture)}

  it "factory should be valid" do
    expect(culture).to be_valid
  end

  describe "validates" do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe "associations" do
    it { is_expected.to have_one(:photo).class_name('Photo') }
    it { is_expected.to have_many(:accounts).through(:account_culture_associations) }
    it { is_expected.to have_many(:talent_profiles).through(:talent_culture_associations) }
  end
end
