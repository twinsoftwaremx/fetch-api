# == Schema Information
#
# Table name: company_sizes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe CompanySize, type: :model do
  describe "associations" do
    it { is_expected.to have_many(:talent_profiles).through(:preferred_company_size_associations)}
  end
end
