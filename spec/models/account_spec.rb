# == Schema Information
#
# Table name: accounts
#
#  id               :integer          not null, primary key
#  company_name     :string
#  phone            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  summary          :text
#  benefits         :text
#  perks            :text
#  github_url       :string
#  company_site_url :string
#  address_id       :integer
#  culture          :integer          default(0)
#  agent_id         :integer
#  company_size_id  :integer
#  year_founded     :integer
#  revenue          :string
#  who_we_are       :text
#  what_we_do       :text
#  video_url        :string
#
# Indexes
#
#  index_accounts_on_company_size_id  (company_size_id)
#

require 'rails_helper'

RSpec.describe Account, type: :model do
  describe "factory" do
    let(:account) {build(:account)}

    it "should be valid" do
      expect(account).to be_valid
    end
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:company_name) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:address) }
    it { is_expected.to have_many(:employer_contacts) }
    it { is_expected.to have_many(:positions).through(:employer_contacts) }
    it { is_expected.to have_one(:logo).class_name('Photo') }
    it { is_expected.to belong_to(:agent) }
    it { is_expected.to belong_to(:company_size) }
  end

  describe "building up an account" do
    it "can build in steps" do
      attrs = attributes_for(:account)
      account = Account.new(attrs)
      expect(account.save).to eq(true)

      employer_contacts = create_list(:employer_contact, 3)
      account.employer_contacts = employer_contacts
      expect(account.employer_contacts.count).to eq 3

      positions_for_contact1 = create_list(:position, 5)
      employer_contacts.first.positions = positions_for_contact1
      expect(employer_contacts.first.positions.count).to eq 5

      positions_for_contact2 = create_list(:position, 3)
      employer_contacts.last.positions = positions_for_contact2
      expect(employer_contacts.last.positions.count).to eq 3

      expect(account.positions.count).to eq 8

      first_position = positions_for_contact1.first
      first_position.required_skill_list = 'Ruby,Rails'
      first_position.save
      expect(first_position.required_skills.count).to eq 2

      first_position.bonus_skill_list = 'Javascript,MongoDB,TDD'
      first_position.save
      expect(first_position.bonus_skills.count).to eq 3
    end
  end
end
