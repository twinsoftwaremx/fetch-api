# == Schema Information
#
# Table name: skills
#
#  id                :integer          not null, primary key
#  name              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  skill_category_id :integer
#

require 'rails_helper'

RSpec.describe Skill, type: :model do
  let(:skill) {build(:skill)}

  it "factory should be valid" do
    expect(skill).to be_valid
  end
end
