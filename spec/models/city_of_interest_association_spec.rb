# == Schema Information
#
# Table name: city_of_interest_associations
#
#  id                :integer          not null, primary key
#  talent_profile_id :integer
#  city_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_city_of_interest_associations_on_city_id            (city_id)
#  index_city_of_interest_associations_on_talent_profile_id  (talent_profile_id)
#

require 'rails_helper'

RSpec.describe CityOfInterestAssociation, type: :model do
  describe "factory" do
    let(:city_of_interest_association) { build(:city_of_interest_association) }

    it "should be valid" do
      expect(city_of_interest_association).to be_valid
    end
  end

  describe "associations" do
    it { is_expected.to belong_to(:talent_profile) }
    it { is_expected.to belong_to(:city) }
  end
end
