require "rails_helper"

RSpec.describe OfferMailer, type: :mailer do
  describe "send make offer mail to talent" do
    let(:offer) { create(:offer) }
    let(:mail) { OfferMailer.email_make_offer(offer) }

    it "renders the subject" do
      expect(mail.subject).to eql('You have received a job offer!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the talent profile email" do
      expect(mail.to).to include(offer.talent_profile_match.talent_profile.user.email)
    end
  end

  describe "send offer accept mail" do
    let(:offer) { create(:offer) }
    let(:mail) { OfferMailer.email_accept_offer(offer) }

    it "renders the subject" do
      expect(mail.subject).to eql('Talent accepted the offer!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the employer email" do
      expect(mail.to).to include(offer.employer_contact.user.email)
    end
  end

  describe "send offer reject mail" do
    let(:offer) { create(:offer) }
    let(:mail) { OfferMailer.email_reject_offer(offer) }

    it "renders the subject" do
      expect(mail.subject).to eql('Talent rejected the offer!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the employer email" do
      expect(mail.to).to include(offer.employer_contact.user.email)
    end
  end
end
