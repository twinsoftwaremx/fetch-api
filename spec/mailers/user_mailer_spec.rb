require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe "send test" do
    let(:email) { Faker::Internet.email }
    let(:mail) { UserMailer.send_test(email) }

    it "renders the subject" do
      expect(mail.subject).to eql('Welcome to My Awesome Site')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "assigns @email" do
      expect(mail.body.encoded).to match(email)
    end
  end
end
