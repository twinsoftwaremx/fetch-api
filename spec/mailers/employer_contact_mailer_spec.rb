require "rails_helper"

RSpec.describe EmployerContactMailer, type: :mailer do
  describe "send invitation to coworkers" do
    let(:employer_contact) { create(:employer_contact) }
    let(:coworker_email) { 'coworker1@test.com' }
    let(:mail) { EmployerContactMailer.invite_coworker(coworker_email, 'http://www.test.com/invite') }

    it "renders the subject" do
      expect(mail.subject).to eql('Invitation to register on Fetch!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the coworker email" do
      expect(mail.to).to include(coworker_email)
    end
  end
end
