require "rails_helper"

RSpec.describe TalentMatchMailer, type: :mailer do
  describe "send test" do
    let(:email) { Faker::Internet.email }
    let(:mail) { TalentMatchMailer.send_test(email) }

    it "renders the subject" do
      expect(mail.subject).to eql('Welcome to My Awesome Site')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "assigns @email" do
      expect(mail.body.encoded).to match(email)
    end
  end

  describe "sent talent match summary" do
    let(:talent) { FactoryGirl.create(:talent) }
    let(:mail) { TalentMatchMailer.email_talent_match(talent, []) }

    it "renders the subject" do
      expect(mail.subject).to eql('You have been matched on Fetch!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the talent email" do
      expect(mail.to).to include(talent.user.email)
    end
  end

  describe "sent talent interest" do
    let(:talent) { FactoryGirl.create(:talent_profile) }
    let(:match) { create(:talent_profile_match, talent_profile: talent)}
    let(:mail) { TalentMatchMailer.email_talent_interest(talent, match) }

    it "renders the subject" do
      expect(mail.subject).to eql('Talent interested on position!')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "sends to the employer email" do
      expect(mail.to).to include(match.position.employer_contact.user.email)
    end
  end
end
