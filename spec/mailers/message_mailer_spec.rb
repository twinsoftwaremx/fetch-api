require "rails_helper"

RSpec.describe MessageMailer, type: :mailer do
  describe "send test" do
    let(:email) { Faker::Internet.email }
    let(:mail) { MessageMailer.send_test(email) }

    it "renders the subject" do
      expect(mail.subject).to eql('Welcome to My Awesome Site')
    end

    it "renders the sender email" do
      expect(mail.from).to include("noreply@api.fetch.com")
    end

    it "assigns @email" do
      expect(mail.body.encoded).to match(email)
    end
  end

  describe "notifying users" do
    let(:message) { FactoryGirl.create(:message) }
    let(:mail) { MessageMailer.notify_users(message.from_user, message.to_user, message.body) }

    it "renders the subject" do
      expect(mail.subject).to eql("You've recieved a message on Fetch!")
    end

    it "renders the sender email" do
      expect(mail.from).to include(message.from_user.email)
    end

    it "renders the reciever email" do
      expect(mail.to).to include(message.to_user.email)
    end
  end
end
