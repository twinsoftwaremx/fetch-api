class AddRankToSkillAssociations < ActiveRecord::Migration
  def change
    add_column :skill_associations, :rank, :integer
  end
end
