class CreateSalaryRanges < ActiveRecord::Migration
  def up
    remove_column :positions, :salary_range
    remove_column :talent_profiles, :preferred_base_salary
    add_column :positions, :salary_range_id, :integer, index: true
    add_column :talent_profiles, :salary_range_id, :integer, index: true
    create_table :salary_ranges do |t|
      t.string :name

      t.timestamps null: false
    end
  end

  def down
    drop_table :salary_ranges
    add_column :positions, :salary_range, :string
    remove_column :positions, :salary_range_id
    add_column :talent_profiles, :preferred_base_salary, :string
    remove_column :talent_profiles, :salary_range_id
  end
end
