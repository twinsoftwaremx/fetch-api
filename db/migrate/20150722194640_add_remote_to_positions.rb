class AddRemoteToPositions < ActiveRecord::Migration
  def change
    add_column :positions, :remote, :boolean, default: false
  end
end
