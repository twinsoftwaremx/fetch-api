class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :job_title, null: false
      t.text :description
      t.integer :hours, default: 0
      t.string :salary_range
      t.integer :desired_years_experience, default: 0
      t.string :team_size
      t.integer :employment_type_id, null: false
      t.string :github_url
      t.string :company_site_url

      t.timestamps null: false
    end

    add_index :positions, :employment_type_id
  end
end
