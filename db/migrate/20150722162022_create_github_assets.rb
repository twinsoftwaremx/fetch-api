class CreateGithubAssets < ActiveRecord::Migration
  def change
    create_table :github_assets do |t|
      t.integer :follower_count
      t.integer :public_repo_count
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
