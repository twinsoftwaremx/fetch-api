class CreateSkillAssociations < ActiveRecord::Migration
  def change
    create_table :skill_associations do |t|
      t.integer :talent_profile_id
      t.integer :skill_id

      t.timestamps null: false
    end
    add_index :skill_associations, :talent_profile_id
    add_index :skill_associations, :skill_id
  end
end
