class AddHiringManagerToPositions < ActiveRecord::Migration
  def change
    add_column :positions, :hiring_manager, :string
  end
end
