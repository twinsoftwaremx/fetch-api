class AddCultureToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :culture, :text
  end
end
