class CreateTalents < ActiveRecord::Migration
  def change
    create_table :talents do |t|
      t.integer :user_id
      t.integer :talent_profile_id

      t.timestamps null: false
    end
    add_index :talents, :user_id
    add_index :talents, :talent_profile_id
  end
end
