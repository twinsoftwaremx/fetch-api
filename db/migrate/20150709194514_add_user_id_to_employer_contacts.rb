class AddUserIdToEmployerContacts < ActiveRecord::Migration
  def change
    add_column :employer_contacts, :user_id, :integer
    add_index :employer_contacts, :user_id
  end
end
