class AddTalentProfileIdToPreviousEmploymentAndEductionItem < ActiveRecord::Migration
  def change
    add_column :education_items, :talent_profile_id, :integer, index: true
    add_column :previous_employments, :talent_profile_id, :integer, index: true
  end
end
