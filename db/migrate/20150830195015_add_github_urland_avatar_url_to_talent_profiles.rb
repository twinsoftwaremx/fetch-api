class AddGithubUrlandAvatarUrlToTalentProfiles < ActiveRecord::Migration
  def change
    add_column :talent_profiles, :github_url, :string
    add_column :talent_profiles, :avatar_url, :string
  end
end
