class AddPhotoIdToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :photo_id, :integer
  end
end
