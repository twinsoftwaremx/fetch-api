class AddEmployerContactIdToPositions < ActiveRecord::Migration
  def change
    add_column :positions, :employer_contact_id, :integer
    add_index :positions, :employer_contact_id
  end
end
