class AddBenefitsToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :benefits, :text
  end
end
