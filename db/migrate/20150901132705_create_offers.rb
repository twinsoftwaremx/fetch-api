class CreateOffers < ActiveRecord::Migration
  def change
    add_column :talent_profile_matches, :status, :integer, default: 0
    create_table :offers do |t|
      t.integer :talent_profile_match_id, null: false
      t.integer :employer_contact_id, null: false
      t.integer :status, default: 0
      t.string :company_name
      t.string :position_title
      t.string :manager_name
      t.string :salary_pay_rate
      t.string :start_date
      t.text   :benefits
      t.text   :legal_verbiage
      t.string :hiring_managers_name

      t.timestamps null: false
    end
    add_index :offers, :talent_profile_match_id
    add_index :offers, :employer_contact_id
  end
end
