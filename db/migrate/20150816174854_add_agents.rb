class AddAgents < ActiveRecord::Migration
  def change
    add_column :admin_users, :agent_id, :integer, index: true
    add_column :accounts, :agent_id, :integer, index: true
    add_column :talent_profiles, :agent_id, :integer, index: true

    create_table(:agents) do |t|
      t.string :name,               null: false, default: ""

      t.timestamps null: false
    end
  end
end
