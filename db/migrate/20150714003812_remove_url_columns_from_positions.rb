class RemoveUrlColumnsFromPositions < ActiveRecord::Migration
  def up
    remove_column :positions, :github_url
    remove_column :positions, :company_site_url
  end
end
