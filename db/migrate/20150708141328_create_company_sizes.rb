class CreateCompanySizes < ActiveRecord::Migration
  def change
    create_table :company_sizes do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
