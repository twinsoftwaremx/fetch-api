class AddCompanySizeIdToAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :size
    add_column :accounts, :company_size_id, :integer
    add_index :accounts, :company_size_id
  end
end
