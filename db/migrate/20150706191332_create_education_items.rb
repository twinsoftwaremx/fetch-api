class CreateEducationItems < ActiveRecord::Migration
  def change
    create_table :education_items do |t|
      t.string :college
      t.integer :year
      t.string :degree

      t.timestamps null: false
    end
  end
end
