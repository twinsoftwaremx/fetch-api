class MakeAssetsPolymorphic < ActiveRecord::Migration
  def up
    add_column :assets, :assetable_id, :integer
    add_column :assets, :assetable_type, :string
    remove_column :cultures, :photo_id
    remove_column :talent_profiles, :photo_id
    remove_column :talent_profiles, :resume_id
    remove_column :employer_contacts, :photo_id
    remove_column :agents, :photo_id
  end

  def down
    remove_column :assets, :assetable_id
    remove_column :assets, :assetable_type
    add_column :accounts, :photo_id, :integer
    add_column :culture, :photo_id, :integer
    add_column :talent_profiles, :photo_id, :integer
    add_column :talent_profiles, :resume_id, :integer
    add_column :employer_contacts, :photo_id, :integer
    add_column :agents, :photo_id, :integer
  end
end
