class AddDetailsAndJobTypeToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :details, :text
    add_column :offers, :job_type, :string
  end
end
