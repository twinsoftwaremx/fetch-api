class AddCityIdToPositions < ActiveRecord::Migration
  def change
    add_column :positions, :city_id, :integer
    add_index :positions, :city_id
  end
end
