class CreateLinkedinAssets < ActiveRecord::Migration
  def change
    create_table :linkedin_assets do |t|
      t.json :recommendations
      t.json :skills
      t.json :positions
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
