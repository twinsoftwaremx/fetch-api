class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :body, null: false
      t.integer :to_user_id, null: false
      t.integer :from_user_id, null: false
      t.integer :position_id

      t.timestamps null: false
    end

    add_index :messages, :to_user_id
    add_index :messages, :from_user_id
    add_index :messages, :position_id
  end
end
