class CreateInterestedLocations < ActiveRecord::Migration
  def change
    create_table :interested_locations do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
