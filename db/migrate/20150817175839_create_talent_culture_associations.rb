class CreateTalentCultureAssociations < ActiveRecord::Migration
  def change
    create_table :talent_culture_associations do |t|
      t.integer :talent_profile_id
      t.integer :culture_id

      t.timestamps null: false
    end
    add_index :talent_culture_associations, :talent_profile_id
    add_index :talent_culture_associations, :culture_id
  end
end
