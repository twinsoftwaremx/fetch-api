class AddUrlColumnsToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :github_url, :string
    add_column :accounts, :company_site_url, :string
  end
end
