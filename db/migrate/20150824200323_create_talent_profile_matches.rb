class CreateTalentProfileMatches < ActiveRecord::Migration
  def change
    create_table :talent_profile_matches do |t|
      t.integer :position_id
      t.integer :talent_profile_id
      t.integer :match_score

      t.timestamps null: false
    end
    add_index :talent_profile_matches, :position_id
    add_index :talent_profile_matches, :talent_profile_id
  end
end
