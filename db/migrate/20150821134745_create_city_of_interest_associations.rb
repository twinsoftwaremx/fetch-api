class CreateCityOfInterestAssociations < ActiveRecord::Migration
  def change
    create_table :city_of_interest_associations do |t|
      t.integer :talent_profile_id
      t.integer :city_id

      t.timestamps null: false
    end

    add_index :city_of_interest_associations, :talent_profile_id
    add_index :city_of_interest_associations, :city_id
  end
end
