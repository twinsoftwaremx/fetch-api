class CreateRequiredSkillAssociations < ActiveRecord::Migration
  def change
    create_table :required_skill_associations do |t|
      t.integer :position_id
      t.integer :skill_id

      t.timestamps null: false
    end

    add_index :required_skill_associations, :position_id
    add_index :required_skill_associations, :skill_id
  end
end
