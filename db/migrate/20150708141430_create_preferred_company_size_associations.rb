class CreatePreferredCompanySizeAssociations < ActiveRecord::Migration
  def change
    create_table :preferred_company_size_associations do |t|
      t.integer :talent_profile_id
      t.integer :company_size_id

      t.timestamps null: false
    end
    add_index :preferred_company_size_associations, :talent_profile_id
    add_index :preferred_company_size_associations, :company_size_id
  end
end
