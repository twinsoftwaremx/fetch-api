require_relative '../lib/db_seeder'

DBSeeder.reset_db!

DBSeeder.seed!
