# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150923164221) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_culture_associations", force: :cascade do |t|
    t.integer  "culture_id"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "account_culture_associations", ["account_id"], name: "index_account_culture_associations_on_account_id", using: :btree
  add_index "account_culture_associations", ["culture_id"], name: "index_account_culture_associations_on_culture_id", using: :btree

  create_table "accounts", force: :cascade do |t|
    t.string   "company_name"
    t.string   "phone"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "summary"
    t.text     "benefits"
    t.text     "perks"
    t.string   "github_url"
    t.string   "company_site_url"
    t.integer  "address_id"
    t.integer  "culture",          default: 0
    t.integer  "agent_id"
    t.integer  "company_size_id"
    t.integer  "year_founded"
    t.string   "revenue"
    t.text     "who_we_are"
    t.text     "what_we_do"
    t.string   "video_url"
  end

  add_index "accounts", ["company_size_id"], name: "index_accounts_on_company_size_id", using: :btree

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.string   "line1"
    t.string   "line2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "type"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["type"], name: "index_addresses_on_type", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "agent_id"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "agents", force: :cascade do |t|
    t.string   "name",       default: "", null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "assets", force: :cascade do |t|
    t.string   "file_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "assetable_id"
    t.string   "assetable_type"
    t.string   "type"
  end

  create_table "bonus_skill_associations", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "skill_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "bonus_skill_associations", ["position_id"], name: "index_bonus_skill_associations_on_position_id", using: :btree
  add_index "bonus_skill_associations", ["skill_id"], name: "index_bonus_skill_associations_on_skill_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "city_of_interest_associations", force: :cascade do |t|
    t.integer  "talent_profile_id"
    t.integer  "city_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "city_of_interest_associations", ["city_id"], name: "index_city_of_interest_associations_on_city_id", using: :btree
  add_index "city_of_interest_associations", ["talent_profile_id"], name: "index_city_of_interest_associations_on_talent_profile_id", using: :btree

  create_table "company_sizes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cultures", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "education_items", force: :cascade do |t|
    t.string   "college"
    t.integer  "year"
    t.string   "degree"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "talent_profile_id"
  end

  create_table "employer_contacts", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "title"
    t.string   "linked_in_url"
    t.text     "summary"
    t.string   "phone"
    t.string   "time_zone"
    t.integer  "account_id",    null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
  end

  add_index "employer_contacts", ["account_id"], name: "index_employer_contacts_on_account_id", using: :btree
  add_index "employer_contacts", ["user_id"], name: "index_employer_contacts_on_user_id", using: :btree

  create_table "employment_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "github_assets", force: :cascade do |t|
    t.integer  "follower_count"
    t.integer  "public_repo_count"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "github_assets", ["user_id"], name: "index_github_assets_on_user_id", using: :btree

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "interest_associations", force: :cascade do |t|
    t.integer  "talent_profile_id"
    t.integer  "interest_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "interest_associations", ["interest_id"], name: "index_interest_associations_on_interest_id", using: :btree
  add_index "interest_associations", ["talent_profile_id"], name: "index_interest_associations_on_talent_profile_id", using: :btree

  create_table "interested_locations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "interests", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "linkedin_assets", force: :cascade do |t|
    t.json     "recommendations"
    t.json     "skills"
    t.json     "positions"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.json     "profile"
  end

  add_index "linkedin_assets", ["user_id"], name: "index_linkedin_assets_on_user_id", using: :btree

  create_table "location_of_interest_associations", force: :cascade do |t|
    t.integer  "talent_profile_id"
    t.integer  "location_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "location_of_interest_associations", ["location_id"], name: "idx_loc_int_on_intlocid", using: :btree
  add_index "location_of_interest_associations", ["talent_profile_id"], name: "index_location_of_interest_associations_on_talent_profile_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.text     "body",         null: false
    t.integer  "to_user_id",   null: false
    t.integer  "from_user_id", null: false
    t.integer  "position_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "messages", ["from_user_id"], name: "index_messages_on_from_user_id", using: :btree
  add_index "messages", ["position_id"], name: "index_messages_on_position_id", using: :btree
  add_index "messages", ["to_user_id"], name: "index_messages_on_to_user_id", using: :btree

  create_table "offers", force: :cascade do |t|
    t.integer  "talent_profile_match_id",             null: false
    t.integer  "employer_contact_id",                 null: false
    t.integer  "status",                  default: 0
    t.string   "company_name"
    t.string   "position_title"
    t.string   "manager_name"
    t.string   "salary_pay_rate"
    t.string   "start_date"
    t.text     "benefits"
    t.text     "legal_verbiage"
    t.string   "hiring_managers_name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.text     "details"
    t.string   "job_type"
  end

  add_index "offers", ["employer_contact_id"], name: "index_offers_on_employer_contact_id", using: :btree
  add_index "offers", ["talent_profile_match_id"], name: "index_offers_on_talent_profile_match_id", using: :btree

  create_table "positions", force: :cascade do |t|
    t.string   "job_title",                                null: false
    t.text     "description"
    t.integer  "hours",                    default: 0
    t.integer  "desired_years_experience", default: 0
    t.string   "team_size"
    t.integer  "employment_type_id",                       null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "employer_contact_id"
    t.boolean  "remote",                   default: false
    t.integer  "location_id"
    t.integer  "salary_range_id"
    t.integer  "city_id"
    t.string   "hiring_manager"
    t.boolean  "is_deleted",               default: false
  end

  add_index "positions", ["city_id"], name: "index_positions_on_city_id", using: :btree
  add_index "positions", ["employer_contact_id"], name: "index_positions_on_employer_contact_id", using: :btree
  add_index "positions", ["employment_type_id"], name: "index_positions_on_employment_type_id", using: :btree

  create_table "preferred_company_size_associations", force: :cascade do |t|
    t.integer  "talent_profile_id"
    t.integer  "company_size_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "preferred_company_size_associations", ["company_size_id"], name: "index_preferred_company_size_associations_on_company_size_id", using: :btree
  add_index "preferred_company_size_associations", ["talent_profile_id"], name: "index_preferred_company_size_associations_on_talent_profile_id", using: :btree

  create_table "previous_employments", force: :cascade do |t|
    t.string   "company"
    t.string   "position"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "talent_profile_id"
  end

  create_table "required_skill_associations", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "skill_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "required_skill_associations", ["position_id"], name: "index_required_skill_associations_on_position_id", using: :btree
  add_index "required_skill_associations", ["skill_id"], name: "index_required_skill_associations_on_skill_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "salary_ranges", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skill_associations", force: :cascade do |t|
    t.integer  "talent_profile_id"
    t.integer  "skill_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "rank"
  end

  add_index "skill_associations", ["skill_id"], name: "index_skill_associations_on_skill_id", using: :btree
  add_index "skill_associations", ["talent_profile_id"], name: "index_skill_associations_on_talent_profile_id", using: :btree

  create_table "skill_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skills", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "skill_category_id"
  end

  create_table "states", force: :cascade do |t|
    t.string   "name",         null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "abbreviation", null: false
  end

  create_table "talent_culture_associations", force: :cascade do |t|
    t.integer  "talent_profile_id"
    t.integer  "culture_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "talent_culture_associations", ["culture_id"], name: "index_talent_culture_associations_on_culture_id", using: :btree
  add_index "talent_culture_associations", ["talent_profile_id"], name: "index_talent_culture_associations_on_talent_profile_id", using: :btree

  create_table "talent_profile_matches", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "talent_profile_id"
    t.integer  "match_score"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "status",            default: 0
  end

  add_index "talent_profile_matches", ["position_id"], name: "index_talent_profile_matches_on_position_id", using: :btree
  add_index "talent_profile_matches", ["talent_profile_id"], name: "index_talent_profile_matches_on_talent_profile_id", using: :btree

  create_table "talent_profiles", force: :cascade do |t|
    t.string   "name",                                                         null: false
    t.string   "time_zone",                                                    null: false
    t.string   "phone"
    t.text     "summary"
    t.string   "area_of_interest"
    t.integer  "years_of_experience",                          default: 0
    t.boolean  "usdod_clearance",                              default: false
    t.text     "reason_for_interest_in_new_job_opportunities"
    t.integer  "job_search_status",                            default: 0
    t.integer  "usa_work_auth_type",                           default: 0
    t.string   "linked_in_url"
    t.string   "personal_website"
    t.string   "dribbble_url"
    t.string   "stack_overflow_url"
    t.string   "blog_url"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "location_id"
    t.integer  "agent_id"
    t.integer  "salary_range_id"
    t.string   "github_url"
    t.string   "avatar_url"
    t.string   "title"
    t.string   "company"
  end

  create_table "talents", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "talent_profile_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "talents", ["talent_profile_id"], name: "index_talents_on_talent_profile_id", using: :btree
  add_index "talents", ["user_id"], name: "index_talents_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "auth_token",             default: ""
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "github_assets", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "linkedin_assets", "users"
end
