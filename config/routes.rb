require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  constraint = lambda { |request| request.env['warden'].authenticate? and request.env['warden'].user.admin? }
  constraints constraint do
    mount Sidekiq::Web, at: '/admin/sidekiq', as: :sidekiq
  end

  scope module: :v1, constraints: ApiConstraint.new(version: 1) do
    resources :users, only: [:show, :create] do
      collection do
        get :from_token
      end
    end
    resources :sessions, only: [:create, :destroy]
    resources :agents, only: [:show]

    resources :accounts, only: [:index, :show, :create, :update, :destroy]
    resources :company_sizes, only: [:index, :show, :create, :update, :destroy]
    resources :employer_contacts, only: [:index, :show, :create, :update, :destroy] do
      resources :positions, only: [:index, :show, :create, :update, :destroy]

      post '/invite_coworkers', to: 'employer_contacts#invite_coworkers'
      get '/get_recent_conversations', to: 'employer_contacts#get_recent_conversations'
    end
    resources :positions, only: [:index, :show] do
      resources :matches, only: [:index], controller: 'talent_profile_matches'
    end
    resources :cities, only: [:index, :show, :create, :update, :destroy]
    resources :cultures, only: [:index, :show, :create, :update, :destroy]
    resources :employment_types, only: [:index, :show, :create, :update, :destroy]
    resources :interested_locations, only: [:index, :show, :create, :update, :destroy]
    resources :interests, only: [:index, :show, :create, :update, :destroy]
    resources :locations, only: [:index, :show, :create, :update, :destroy]
    resources :salary_ranges, only: [:index, :show, :create, :update, :destroy]
    resources :skills, only: [:index, :show, :create, :update, :destroy]
    resources :skill_categories, only: [:index, :show, :create, :update, :destroy]
    resources :messages do
      collection do
        get '/for_position/:position_id', to: 'messages#index'
        get '/for_position/:position_id/to_user/:to_user_id/sent', to: 'messages#sent'
        get '/for_position/:position_id/from_user/:from_user_id/received', to: 'messages#received'
        get '/for_position/:position_id/with_user/:with_user_id', to: 'messages#with_user'
        get '/for_position/:position_id/received', to: 'messages#received'
        get '/for_position/:position_id/sent', to: 'messages#sent'
      end
    end
    resources :talent_profiles, only: [:show, :create, :update, :destroy] do
      get '/get_recent_conversations', to: 'talent_profiles#get_recent_conversations'

      resources :previous_employments, only: [:index, :show, :create, :update, :destroy]
      resources :education_items, only: [:index, :show, :create, :update, :destroy]
      resources :interests, only: [:index, :show, :create, :destroy], path: '/interests', controller: 'interest_associations'
      resources :company_sizes, only: [:index, :show, :create, :destroy], path: '/pref_company_sizes', controller: 'preferred_company_size_associations'
      resources :interested_locations, only: [:index, :show, :create, :destroy], path: '/interested_locations', controller: 'location_of_interest_associations'
      resources :matches, only: [:index], controller: 'talent_profile_matches' do
        member do
          post :reject
          post :like
        end
      end
    end
    resources :uploads, only: [:show, :create], controller: 'assets'
    resources :offers do
      member do
        post :reject
        post :accept
      end
    end

    get 'get_fees/:salary', to: 'fee_structure#get_fees'

    scope :search do
      get 'talent_profile', to: 'search#talent_profile'
      get 'positions/:id/talent_profiles', to: 'search#talent_by_position'
    end
  end

  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  root to: 'users#index'
end
