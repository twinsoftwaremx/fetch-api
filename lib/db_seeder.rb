class DBSeeder
  def self.seed!
    # Admin Users and Agents

    agent = Agent.create! name: "Handsom Hutson"
    agent.photo_id = self.add_asset("agent1.jpeg", "Photo").id
    agent.save
    admin = AdminUser.create! email: "admin@test.com", password: "password", password_confirmation: "password"
    admin2 = AdminUser.create! email: "agent@test.com", password: "password", password_confirmation: "password", agent: agent
    # Company Size
    ["Early Stage / Seed companies, < 15 employees",
    "Series A companies, 16-50 employees",
    "Series B+ companies, 51-500 employees",
    "Large companies, 501+ employees"].each do |text|
      CompanySize.create!(name: text)
    end
    # Salary Range
    ["$0 - $20k", "$20 - $40k", "$40 - $75k",
      "$75 - $100k", "$100 - $500k"].each do |salary|
      SalaryRange.create!(name: salary)
    end
    # Employment Type
    ["Contract", "Full Time"].each do |et|
      EmploymentType.create! name: et
    end
    # Culture
    ["START UP", "PROFESSIONAL", "REMOTE"].each do |name|
      culture = Culture.create name: name
      asset_name = name.downcase.gsub(/\s+/, "")
      culture.photo_id = self.add_asset("culture-#{asset_name}.jpg", "Photo").id
      culture.save
    end

    %w(Birmingham Remote).each do |loc|
      InterestedLocation.first_or_create!(name: loc)
    end
    # Skill Category
    ["Software Development", "Infrastructure", "IT Support",
    "Project Manager", "Networking", "Business Analyst",
    "IT Management", "Creative Professional"].each do |text|
      cat = SkillCategory.create(name: text)
    end
    # Skills
    ["Java", "C++", "SAS", "Ruby", "Perl", "Javascript", "Swift", "Python",
      "HTML", "Scala", "Elixir", ".NET", "CSS", "PHP"].each do |text|
      Skill.create! name: text, skill_category: SkillCategory.find_by(name: "Software Development")
    end
    # States
    alabama = State.create(name: "Alabama", abbreviation: "AL")
    City.create(name: "Birmingham", state: alabama)
    City.create(name: "Huntsville", state: alabama)
    City.create(name: "Mobile", state: alabama)
    City.create(name: "Montgomery", state: alabama)
    tennessee = State.create(name: "Tennessee", abbreviation: "TN")
    City.create(name: "Nashville", state: tennessee)
    City.create(name: "Chattanooga", state: tennessee)
    luisiana = State.create(name: "Luisiana", abbreviation: "LA")
    City.create(name: "New Orleans", state: luisiana)
    georgia = State.create(name: "Georgia", abbreviation: "GA")
    City.create(name: "Atlanta", state: georgia)
    north_carolina = State.create(name: "North Carolina", abbreviation: "NC")
    City.create(name: "Charlotte", state: north_carolina)
    texas = State.create(name: "Texas", abbreviation: "TX")
    City.create(name: "Austin", state: texas)

    # Talents
    user1 = User.create! email: "talent1@test.com", password: "password", password_confirmation: "password"
    profile = TalentProfile.create! name: "Chase Allen Hutson", summary: "Chief Development Officer of Janitorial Services",
                                    time_zone: "EST", agent: agent, salary_range: SalaryRange.last
    user1.talent_profile = profile
    profile.skill_list = "C++,Java,SAS"
    profile.culture_list = "START UP,PROFESSIONAL"
    l1 = Location.create! city: "Birmingham", state: "AL", zip: "35201"
    l2 = Location.create! city: "Huntsville", state: "AL", zip: "35801"
    profile.locations_of_interest << [l1, l2]
    profile.cities_of_interest << [City.find_by(name: "Birmingham"), City.find_by(name: "Huntsville")]
    profile.photo_id = self.add_asset("avatar1.png", "Photo").id
    profile.resume_id = self.add_asset("resume1.jpg", "Resume").id
    profile.save

    user2 = User.create! email: "talent2@test.com", password: "password", password_confirmation: "password"
    profile2 = TalentProfile.create! name: "John Doe", summary: "Senior Web Developer",
                                     time_zone: "EST", agent: agent, salary_range: SalaryRange.last
    user2.talent_profile = profile2
    profile2.skill_list = "C++,Ruby,Python"
    profile2.cities_of_interest << City.find_by(name: "Birmingham")
    profile.salary_range = SalaryRange.first
    profile2.save

    user3 = User.create! email: "talent3@test.com", password: "password", password_confirmation: "password"
    profile3 = TalentProfile.create! name: "Mike Smith", summary: "Front end developer",
                                     time_zone: "EST", agent: agent, salary_range: SalaryRange.first
    user3.talent_profile = profile3
    profile3.skill_list = "Javascript,CSS,HTML"
    profile3.cities_of_interest << City.find_by(name: "Birmingham")
    profile3.save

    # Accounts

    user = User.create! email: "employer_shell@test.com", password: "password", password_confirmation: "password", role: "account_admin"
    account = Account.create! company_name: "Shell", agent: agent, logo_id: self.add_asset("shell.jpeg", "Photo").id
    account.who_we_are = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In dictum, eros sit amet blandit pellentesque, sapien sapien tempus diam, at condimentum leo eros vitae lacus. Sed dolor lacus, dapibus a lectus at, molestie varius justo. Proin ut purus a ipsum eleifend pretium vel sit amet nibh. Sed varius sem id lacus lobortis volutpat. Morbi maximus mi lobortis ante accumsan viverra. Nam ut est condimentum, mattis lorem non, tristique quam. Nam gravida arcu arcu, in volutpat mauris aliquet ut. Praesent fringilla, mauris a finibus accumsan, ipsum neque dapibus libero, vitae iaculis felis nisi a ligula. Maecenas eu eleifend nibh. Aenean sodales blandit libero non ultrices. Cras facilisis et nunc a eleifend. Vestibulum et est metus. Integer et risus nec sapien sagittis vulputate mattis nec magna."
    account.what_we_do = "Donec ac bibendum lorem. Integer nec fringilla velit. Nam sem nisl, viverra vitae sodales non, blandit at quam. Praesent ac dui sagittis, blandit massa vitae, ultrices lorem. Ut pulvinar condimentum arcu ac dapibus. Suspendisse id maximus diam, at fermentum mi. Donec consectetur enim eu interdum ullamcorper. Mauris ac mattis leo, ut hendrerit urna."
    account.perks = "Praesent imperdiet suscipit magna eu tristique. Vivamus bibendum lorem nec risus sollicitudin, vel malesuada purus fringilla. Quisque consectetur lorem quis urna convallis, sagittis tempus diam eleifend. Suspendisse potenti. Morbi eu aliquam erat. Ut sollicitudin ante non pellentesque facilisis. Praesent auctor, mauris eget posuere blandit, neque orci efficitur lectus, non convallis nibh felis et est. Nunc viverra faucibus massa in dignissim."
    account.company_site_url = "www.shellcorp.com"
    account.year_founded = 1996
    account.company_size = CompanySize.where(name:"Series A companies, 16-50 employees").first
    account.revenue = "$1.5 MILLION"
    account.address = FullAddress.create(city: 'Birmingham', state: 'AL', zip: '35201')
    account.save
    account.culture_list = "START UP,PROFESSIONAL"
    ec = EmployerContact.create! user: user, account: account, name: "Joe Smith", title: "CTO", summary: "Information Technology executive, proven strategist and business partner with over 25 years combined online and B2C experience."
    position = Position.create! employment_type: EmploymentType.first, employer_contact: ec, job_title: "Ice Cream Taster", salary_range: SalaryRange.last, location: l1,
        city: City.find_by(name: "Birmingham"), description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in metus eget ante egestas commodo. Proin vitae purus nec magna lobortis consectetur nec cursus leo. Sed laoreet dignissim lectus, tristique rhoncus dui semper nec. Cras vestibulum, diam sit amet bibendum tristique, sapien dui pulvinar eros, id facilisis justo tortor et odio. Donec finibus lorem ipsum, malesuada blandit quam placerat at. Aenean sodales nulla in velit viverra, ac dictum leo vestibulum. Nunc sit amet cursus neque, varius sollicitudin orci."
    position.required_skill_list = "C++,Java,SAS"
    ec.photo_id = self.add_asset("avatar1.png", "Photo").id

    position = Position.create! employment_type: EmploymentType.first, employer_contact: ec, job_title: "Beer drinker", salary_range: SalaryRange.last, location: l1,
        city: City.find_by(name: "Birmingham")
    position.required_skill_list = "Javascript"

    user = User.create! email: "employer_rs@test.com", password: "password", password_confirmation: "password", role: "account_admin"
    account = Account.create! company_name: "Rolling Stone", agent: agent, logo_id: self.add_asset("rollingstone.jpeg", "Photo").id
    account.culture_list = "START UP,PROFESSIONAL"
    ec = EmployerContact.create! user: user, account: account, name: "Joe Smith"
    position = Position.create! employment_type: EmploymentType.first,  employer_contact: ec, job_title: "Coffee Retriever", salary_range: SalaryRange.last, location: l2,
      city: City.find_by(name: "Birmingham")
    position.required_skill_list = "C++,Java,SAS"

    user = User.create! email: "employer_vw@test.com", password: "password", password_confirmation: "password", role: "account_admin"
    account = Account.create! company_name: "Volkswagon", agent: agent, logo_id: self.add_asset("volkswagon.jpeg", "Photo").id
    account.culture_list = "START UP,PROFESSIONAL"
    ec = EmployerContact.create! user: user, account: account, name: "Joe Smith"
    position = Position.create! employment_type: EmploymentType.first,  employer_contact: ec, job_title: "Laidenhosen seamstress", salary_range: SalaryRange.last, location: l2,
        city: City.find_by(name: "Birmingham")
    position.required_skill_list = "C++,Java,SAS"

    user = User.create! email: "employer_nbc@test.com", password: "password", password_confirmation: "password", role: "account_admin"
    account = Account.create! company_name: "NBC", agent: agent, logo_id: self.add_asset("nbc.png", "Photo").id
    account.culture_list = "START UP,PROFESSIONAL"
    ec = EmployerContact.create! user: user, account: account, name: "Joe Smith"
    position = Position.create! employment_type: EmploymentType.first,  employer_contact: ec, job_title: "Intern", salary_range: SalaryRange.last, location: l1,
        city: City.find_by(name: "Birmingham")
    position.required_skill_list = "C++,Java,SAS"


    message1 = Message.create(body: "Hey I think this would be a great fit for you!", position: position, to_user: user1, from_user: user)
    message2 = Message.create(body: "Are you sure?  It says they want a goat...", position: position, to_user: user, from_user: user1)
    message3 = Message.create(body: "I'm sure I can tweak your resume so you fit...", position: position, to_user: user1, from_user: user)

    Position.all.each do |p|
      TalentProfileMatch.create! position: p, talent_profile: profile, match_score: 1.1366339
    end
  end

  def self.reset_db!
    [User, City, State, Skill, SkillCategory, CompanySize, InterestedLocation, Account,
      Address, AdminUser, Agent, Asset, BonusSkillAssociation, EducationItem, EmployerContact,
      EmploymentType, GithubAsset, Identity, Interest, InterestAssociation, LinkedinAsset, LocationOfInterestAssociation,
      Message, Position, PreferredCompanySizeAssociation, PreviousEmployment, RequiredSkillAssociation, Role, SkillAssociation,
      TalentProfile, Talent, Culture, EmployerContact, SalaryRange, TalentProfileMatch].each do |klass|
      klass.destroy_all
      ActiveRecord::Base.connection.reset_pk_sequence!("#{klass.table_name}")
    end
  end

  def self.add_asset(asset_file, type)
    file = File.open(File.join(Rails.root, "lib", "seeds", asset_file))
    asset = Asset.create! file: file, type: type
    asset
  end
end
